package com.softdesign.skillbrunchmainapp.ui.screens.root;

import android.support.v4.view.ViewPager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author Sergey Vorobyev
 */
public class ActionBarBuilderTest {

    @Mock
    private RootActivity mockRootActivity;
    private ActionBarBuilder actionBarBuilder;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        actionBarBuilder = new ActionBarBuilder(mockRootActivity)
                .setVisibility(true)
                .setTitle("title")
                .setBackArrow(true)
                .addAction(mock(MenuItemHolder.class));
    }

    @Test
    public void build_WithViewPager() throws Exception {
        actionBarBuilder.setTabs(mock(ViewPager.class)).build();

        verify(mockRootActivity, times(1)).setActionBarVisibility(anyBoolean());
        verify(mockRootActivity, times(1)).setActionBarTitle(anyString());
        verify(mockRootActivity, times(1)).setBackArrow(anyBoolean());
        verify(mockRootActivity, times(1)).setMenuItems(anyList());
        verify(mockRootActivity, times(1)).setTabLayout(any(ViewPager.class));
        verify(mockRootActivity, never()).removeTabLayout();
    }

    @Test
    public void build_WithoutViewPager() throws Exception {
        actionBarBuilder.build();

        verify(mockRootActivity, times(1)).setActionBarVisibility(anyBoolean());
        verify(mockRootActivity, times(1)).setActionBarTitle(anyString());
        verify(mockRootActivity, times(1)).setBackArrow(anyBoolean());
        verify(mockRootActivity, times(1)).setMenuItems(anyList());
        verify(mockRootActivity, times(1)).removeTabLayout();
        verify(mockRootActivity, never()).setTabLayout(any(ViewPager.class));
    }
}