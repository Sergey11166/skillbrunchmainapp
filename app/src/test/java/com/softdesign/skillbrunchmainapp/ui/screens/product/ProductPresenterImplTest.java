package com.softdesign.skillbrunchmainapp.ui.screens.product;

import android.content.Context;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.di.components.AppComponent;
import com.softdesign.skillbrunchmainapp.di.components.DaggerAppComponent;
import com.softdesign.skillbrunchmainapp.di.modules.AppModule;
import com.softdesign.skillbrunchmainapp.di.modules.MockRootModule;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogModel;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.DaggerCatalogScreen_Component;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.MockCatalogModule;
import com.softdesign.skillbrunchmainapp.ui.screens.product_details.ProductDetailsScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.DaggerRootActivity_RootComponent;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductViewModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

import static mortar.bundler.BundleServiceRunner.SERVICE_NAME;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Sergey Vorobyev
 */
public class ProductPresenterImplTest {

    @Mock
    private Flow mockFlow;

    @Mock
    private Context mockContext;

    @Mock
    private RootView mockRootView;

    @Mock
    private CatalogModel mockModel;

    @Mock
    private ProductViewImpl mockView;

    @Mock
    private ProductViewModel mockViewModel;

    @Mock
    private RootPresenter mockRootPresenter;

    private ProductScreen.Component productDaggerComponent;
    private ProductPresenterImpl testPresenter;
    private ProductRealm testProduct;

    @Before
    @SuppressWarnings("WrongConstant")
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        prepareDagger();

        BundleServiceRunner bundleServiceRunner = new BundleServiceRunner();
        MortarScope mockScope = MortarScope.buildRootScope()
                .withService(SERVICE_NAME, bundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, productDaggerComponent)
                .build("MockScope");

        String flowService = "flow.InternalContextWrapper.FLOW_SERVICE";
        when(mockContext.getSystemService(flowService)).thenReturn(mockFlow);
        when(mockContext.getSystemService(SERVICE_NAME)).thenReturn(bundleServiceRunner);
        when(mockView.getContext()).thenReturn(mockContext);
        when(mockRootPresenter.getView()).thenReturn(mockRootView);
        when(mockContext.getSystemService(MortarScope.class.getName())).thenReturn(mockScope);

        testProduct = new ProductRealm("id", "productName", "imageUrl",
                "description", 1, 2, 1, true);

        testPresenter = new ProductPresenterImpl(testProduct);
        testPresenter.takeView(mockView);
    }

    private void prepareDagger() {

        AppComponent appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(mockContext))
                .build();

        RootActivity.RootComponent rootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(appComponent)
                .rootModule(new MockRootModule(mockRootPresenter))
                .build();

        CatalogScreen.Component catalogDaggerComponent = DaggerCatalogScreen_Component.builder()
                .rootComponent(rootComponent)
                .module(new MockCatalogModule(mockModel))
                .build();

        productDaggerComponent = DaggerProductScreen_Component.builder()
                .component(catalogDaggerComponent)
                .module(new MockProductModule(mockViewModel))
                .build();
    }

    @Test
    public void onPlusButtonClick() throws Exception {
        testPresenter.onPlusButtonClick();

        verify(mockModel, times(1)).incrementProductCount(testProduct);
    }

    @Test
    public void onMinusButtonClick() throws Exception {
        testPresenter.onMinusButtonClick();

        verify(mockModel, times(1)).decrementProductCount(testProduct);
    }

    @Test
    public void onFavoriteClick() throws Exception {
        testPresenter.onFavoriteClick();

        verify(mockModel, times(1)).changeFavoriteStatus(testProduct);
    }

    @Test
    public void onDetailsClick() throws Exception {
        testPresenter.onDetailsClick();

        verify(mockFlow, times(1)).set(any(ProductDetailsScreen.class));
    }

    @Test
    public void onEnterScope() throws Exception {
        verify(mockViewModel, times(1))
                .setPrice(String.valueOf(testProduct.getPrice() * testProduct.getCount()));
        verify(mockViewModel, times(1)).setCount(String.valueOf(testProduct.getCount()));
        verify(mockViewModel, times(1)).setDescription(testProduct.getDescription());
        verify(mockViewModel, times(1)).setName(testProduct.getProductName());
        verify(mockViewModel, times(1)).setImage(testProduct.getImageUrl());
        verify(mockViewModel, times(1)).setFavorite(testProduct.isFavorite());
    }
}