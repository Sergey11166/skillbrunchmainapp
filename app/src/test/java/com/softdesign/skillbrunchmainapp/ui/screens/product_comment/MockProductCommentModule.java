package com.softdesign.skillbrunchmainapp.ui.screens.product_comment;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductCommentViewModel;

import static org.mockito.Mockito.mock;

/**
 * @author Sergey Vorobyev
 */
class MockProductCommentModule extends ProductCommentScreen.Module {

    private ProductCommentViewModel viewModel;

    MockProductCommentModule(ProductCommentViewModel viewModel) {
        super(mock(ProductRealm.class));
        this.viewModel = viewModel;
    }

    @Override
    ProductCommentViewModel provideViewModel() {
        return viewModel;
    }
}
