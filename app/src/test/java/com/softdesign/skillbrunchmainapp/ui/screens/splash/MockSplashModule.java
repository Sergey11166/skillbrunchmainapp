package com.softdesign.skillbrunchmainapp.ui.screens.splash;

/**
 * @author Sergey Vorobyev
 */
class MockSplashModule extends SplashScreen.Module {

    private SplashModel model;

    MockSplashModule(SplashModel model) {
        this.model = model;
    }

    @Override
    SplashModel provideModel() {
        return model;
    }
}
