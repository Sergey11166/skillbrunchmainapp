package com.softdesign.skillbrunchmainapp.ui.screens.root;

import android.content.Intent;

import com.softdesign.skillbrunchmainapp.data.storage.dto.ActivityResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.dto.PermissionsResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.ui.view_models.RootViewModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;

import rx.Observable;
import rx.subjects.PublishSubject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Sergey Vorobyev
 */
public class RootPresenterImplTest {

    @Mock
    private RootView mockView;

    @Mock
    private RootModel mockModel;

    @Mock
    private RootViewModel mockViewModel;

    @Mock
    private PublishSubject<ActivityResultDto> mockActivityResultSubject;

    @Mock
    private PublishSubject<PermissionsResultDto> mockPermissionsSubject;

    private RootPresenterImpl testPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        testPresenter = new RootPresenterImpl(mockModel, mockViewModel,
                mockActivityResultSubject, mockPermissionsSubject);

        UserInfoRealm userInfo = new UserInfoRealm("userId", "username", "+79022582585",
                "http://avatar_url", true, true);
        when(mockModel.getUserInfo()).thenReturn(Observable.just(userInfo));

        testPresenter.takeView(mockView);
    }

    @Test
    public void takeView() throws Exception {
        verify(mockView, times(1)).bindViewModel(mockViewModel);
        verify(mockModel, times(1)).getUserInfo();
    }

    @Test
    public void setCartCount() throws Exception {
        testPresenter.setCartCount(1);
        verify(mockViewModel, times(1)).setCartCount("1");
    }

    @Test
    public void getView() throws Exception {
        assertEquals(mockView, testPresenter.getView());
    }

    @Test
    public void startCameraForResult() throws Exception {
        File mockFile = mock(File.class);
        testPresenter.startCameraForResult(mockFile, 1);

        verify(mockView, times(1)).startCameraForResult(mockFile, 1);
    }

    @Test
    public void startGalleryForResult() throws Exception {
        testPresenter.startGalleryForResult(1);

        verify(mockView, times(1)).startGalleryForResult(1);
    }

    @Test
    public void startSettingForResult() throws Exception {
        testPresenter.startSettingForResult(1);

        verify(mockView, times(1)).startSettingForResult(1);
    }

    @Test
    public void checkAndRequestPermission_AllGranted() throws Exception {
        String[] permissions = {"permission1", "permission2"};
        when(mockView.isAllPermissionsGranted(permissions)).thenReturn(true);

        testPresenter.checkAndRequestPermission(permissions, 1);

        verify(mockView, never()).requestPermissions(any(String[].class), anyInt());
    }

    @Test
    public void checkAndRequestPermission_NonAllGranted() throws Exception {
        String[] permissions = {"permission1", "permission2"};
        when(mockView.isAllPermissionsGranted(permissions)).thenReturn(false);

        testPresenter.checkAndRequestPermission(permissions, 1);

        verify(mockView, times(1)).requestPermissions(permissions, 1);
    }

    @Test
    public void onActivityResult() throws Exception {
        testPresenter.onActivityResult(1, 1, new Intent());

        verify(mockActivityResultSubject, only()).onNext(any(ActivityResultDto.class));
    }

    @Test
    public void onRequestPermissionsResult() throws Exception {
        String[] permissions = {"permission1", "permission2"};
        testPresenter.onRequestPermissionsResult(1, permissions, new int[1]);

        verify(mockPermissionsSubject, only()).onNext(any(PermissionsResultDto.class));
    }

    @Test
    public void newActionBarBuilder() throws Exception {

        ActionBarBuilder builder1 = testPresenter.newActionBarBuilder();
        ActionBarBuilder builder2 = testPresenter.newActionBarBuilder();

        assertNotNull(builder1);
        assertNotNull(builder2);
        assertNotEquals(builder1, builder2);
    }
}