package com.softdesign.skillbrunchmainapp.ui.screens.account;

import com.softdesign.skillbrunchmainapp.ui.view_models.AccountViewModel;

import static org.mockito.Mockito.mock;

/**
 * @author Sergey Vorobyev
 */
public class MockAccountModule extends AccountScreen.Module {

    private AccountViewModel viewModel;
    private AccountModel model;

    MockAccountModule(AccountViewModel viewModel, AccountModel model) {
        this.viewModel = viewModel;
        this.model = model;
    }

    public MockAccountModule(AccountModel model) {
        this.viewModel = mock(AccountViewModel.class);
        this.model = model;
    }

    @Override
    AccountModel provideModel() {
        return model;
    }

    @Override
    AccountViewModel provideViewModel() {
        return viewModel;
    }
}
