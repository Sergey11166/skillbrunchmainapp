package com.softdesign.skillbrunchmainapp.ui.screens.product_details;

import android.content.Context;
import android.support.v4.view.ViewPager;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.di.components.AppComponent;
import com.softdesign.skillbrunchmainapp.di.components.DaggerAppComponent;
import com.softdesign.skillbrunchmainapp.di.modules.AppModule;
import com.softdesign.skillbrunchmainapp.di.modules.MockRootModule;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogModel;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.DaggerCatalogScreen_Component;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.MockCatalogModule;
import com.softdesign.skillbrunchmainapp.ui.screens.product_comment.ProductCommentScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.ActionBarBuilder;
import com.softdesign.skillbrunchmainapp.ui.screens.root.DaggerRootActivity_RootComponent;
import com.softdesign.skillbrunchmainapp.ui.screens.root.MenuItemHolder;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductViewModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

import static mortar.bundler.BundleServiceRunner.SERVICE_NAME;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Sergey Vorobyev
 */
public class ProductDetailsPresenterImplTest {

    @Mock
    private Flow mockFlow;

    @Mock
    private Context mockContext;

    @Mock
    private RootView mockRootView;

    @Mock
    private CatalogModel mockModel;

    @Mock
    private ProductViewModel mockViewModel;

    @Mock
    private ProductDetailsViewImpl mockView;

    @Mock
    private RootPresenter mockRootPresenter;

    @Mock
    private ActionBarBuilder mockActionBarBuilder;

    private ProductDetailsScreen.Component productDetailsDaggerComponent;
    private ProductDetailsPresenterImpl testPresenter;
    private ProductRealm testProduct;

    @Before
    @SuppressWarnings("WrongConstant")
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        prepareDagger();

        BundleServiceRunner bundleServiceRunner = new BundleServiceRunner();
        MortarScope mockScope = MortarScope.buildRootScope()
                .withService(SERVICE_NAME, bundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, productDetailsDaggerComponent)
                .build("MockScope");

        String flowService = "flow.InternalContextWrapper.FLOW_SERVICE";
        when(mockContext.getSystemService(flowService)).thenReturn(mockFlow);
        when(mockContext.getSystemService(SERVICE_NAME)).thenReturn(bundleServiceRunner);
        when(mockView.getContext()).thenReturn(mockContext);
        when(mockView.getViewPager()).thenReturn(mock(ViewPager.class));
        when(mockRootPresenter.getView()).thenReturn(mockRootView);
        when(mockContext.getSystemService(MortarScope.class.getName())).thenReturn(mockScope);

        when(mockActionBarBuilder.setTitle(anyString())).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.setBackArrow(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.setVisibility(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.setTabs(any(ViewPager.class))).thenReturn(mockActionBarBuilder);
        when(mockRootPresenter.newActionBarBuilder()).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.addAction(any(MenuItemHolder.class))).thenReturn(mockActionBarBuilder);

        testProduct = new ProductRealm("id", "productName", "imageUrl",
                "description", 1, 2, 1, true);

        testPresenter = new ProductDetailsPresenterImpl(testProduct);
        testPresenter.takeView(mockView);
    }

    private void prepareDagger() {

        AppComponent appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(mockContext))
                .build();

        RootActivity.RootComponent rootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(appComponent)
                .rootModule(new MockRootModule(mockRootPresenter))
                .build();

        CatalogScreen.Component catalogDaggerComponent = DaggerCatalogScreen_Component.builder()
                .rootComponent(rootComponent)
                .module(new MockCatalogModule(mockModel))
                .build();

        productDetailsDaggerComponent = DaggerProductDetailsScreen_Component.builder()
                .component(catalogDaggerComponent)
                .module(new MockProductDetailsModule(mockViewModel))
                .build();
    }

    @Test
    public void onPlusButtonClick() throws Exception {
        testPresenter.onPlusButtonClick();

        verify(mockModel, times(1)).incrementProductCount(testProduct);
    }

    @Test
    public void onMinusButtonClick() throws Exception {
        testPresenter.onMinusButtonClick();

        verify(mockModel, times(1)).decrementProductCount(testProduct);
    }

    @Test
    public void onFavoriteClick() throws Exception {
        testPresenter.onFavoriteClick();

        verify(mockModel, times(1)).changeFavoriteStatus(testProduct);
    }

    @Test
    public void onAddCommentClick() throws Exception {
        testPresenter.onAddCommentClick();

        verify(mockFlow, times(1)).set(any(ProductCommentScreen.class));
    }

    @Test
    public void onEnterScope() throws Exception {
        verify(mockViewModel, times(1)).setName(testProduct.getProductName());
        verify(mockViewModel, times(1)).setDescription(testProduct.getDescription());
        verify(mockViewModel, times(1)).setImage(testProduct.getImageUrl());
        verify(mockViewModel, times(1))
                .setPrice(String.valueOf(testProduct.getPrice() * testProduct.getCount()));
        verify(mockViewModel, times(1)).setCount(String.valueOf(testProduct.getCount()));
        verify(mockViewModel, times(1)).setFavorite(testProduct.isFavorite());
        verify(mockViewModel, times(1)).setRating(testProduct.getRating());
    }

    @Test
    public void onLoad() throws Exception {
        verify(mockView, times(1)).showComments(anyList());
    }

    @Test
    public void initActionBar() throws Exception {
        verify(mockActionBarBuilder, times(1)).setTitle(testProduct.getProductName());
        verify(mockActionBarBuilder, times(1)).setBackArrow(true);
        verify(mockActionBarBuilder, times(1)).addAction(any(MenuItemHolder.class));
        verify(mockActionBarBuilder, times(1)).setTabs(any(ViewPager.class));
        verify(mockActionBarBuilder, times(1)).build();
    }
}