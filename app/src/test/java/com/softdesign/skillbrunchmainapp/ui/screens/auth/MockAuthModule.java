package com.softdesign.skillbrunchmainapp.ui.screens.auth;

import com.softdesign.skillbrunchmainapp.ui.view_models.AuthViewModel;

/**
 * @author Sergey Vorobyev
 */
class MockAuthModule extends AuthScreen.Module {

    private AuthModel model;
    private AuthViewModel viewModel;

    MockAuthModule(AuthModel model, AuthViewModel viewModel) {
        this.model = model;
        this.viewModel = viewModel;
    }

    @Override
    AuthModel provideModel() {
        return model;
    }

    @Override
    AuthViewModel provideViewModel() {
        return viewModel;
    }
}
