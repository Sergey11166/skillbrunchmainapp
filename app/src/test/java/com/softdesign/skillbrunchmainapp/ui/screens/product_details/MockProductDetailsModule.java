package com.softdesign.skillbrunchmainapp.ui.screens.product_details;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductViewModel;

import static org.mockito.Mockito.mock;

/**
 * @author Sergey Vorobyev
 */
public class MockProductDetailsModule extends ProductDetailsScreen.Module {

    private ProductViewModel viewModel;

    public MockProductDetailsModule() {
        super(mock(ProductRealm.class));
    }

    MockProductDetailsModule(ProductViewModel viewModel) {
        super(mock(ProductRealm.class));
        this.viewModel = viewModel;
    }

    @Override
    ProductViewModel provideViewModel() {
        return viewModel;
    }
}
