package com.softdesign.skillbrunchmainapp.ui.screens.product_comment;

import android.content.Context;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductCommentRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.di.components.AppComponent;
import com.softdesign.skillbrunchmainapp.di.components.DaggerAppComponent;
import com.softdesign.skillbrunchmainapp.di.modules.AppModule;
import com.softdesign.skillbrunchmainapp.di.modules.MockRootModule;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogModel;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.DaggerCatalogScreen_Component;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.MockCatalogModule;
import com.softdesign.skillbrunchmainapp.ui.screens.product_details.DaggerProductDetailsScreen_Component;
import com.softdesign.skillbrunchmainapp.ui.screens.product_details.MockProductDetailsModule;
import com.softdesign.skillbrunchmainapp.ui.screens.product_details.ProductDetailsScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.ActionBarBuilder;
import com.softdesign.skillbrunchmainapp.ui.screens.root.DaggerRootActivity_RootComponent;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductCommentViewModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

import static mortar.bundler.BundleServiceRunner.SERVICE_NAME;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Sergey Vorobyev
 */
public class ProductCommentPresenterImplTest {

    @Mock
    private Flow mockFlow;

    @Mock
    private Context mockContext;

    @Mock
    private RootView mockRootView;

    @Mock
    private CatalogModel mockModel;

    @Mock
    private ProductCommentViewImpl mockView;

    @Mock
    private RootPresenter mockRootPresenter;

    @Mock
    private ActionBarBuilder mockActionBarBuilder;

    @Mock
    private ProductCommentViewModel mockViewModel;

    private ProductCommentScreen.Component productCommentDaggerComponent;
    private ProductCommentPresenterImpl testPresenter;
    private ProductRealm testProduct;

    @Before
    @SuppressWarnings("WrongConstant")
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        prepareDagger();

        BundleServiceRunner bundleServiceRunner = new BundleServiceRunner();
        MortarScope mockScope = MortarScope.buildRootScope()
                .withService(SERVICE_NAME, bundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, productCommentDaggerComponent)
                .build("MockScope");

        String flowService = "flow.InternalContextWrapper.FLOW_SERVICE";
        when(mockContext.getSystemService(flowService)).thenReturn(mockFlow);
        when(mockContext.getSystemService(SERVICE_NAME)).thenReturn(bundleServiceRunner);
        when(mockView.getContext()).thenReturn(mockContext);
        when(mockRootPresenter.getView()).thenReturn(mockRootView);
        when(mockContext.getSystemService(MortarScope.class.getName())).thenReturn(mockScope);

        when(mockActionBarBuilder.setTitle(anyString())).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.setBackArrow(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockRootPresenter.newActionBarBuilder()).thenReturn(mockActionBarBuilder);

        testProduct = new ProductRealm("id", "productName", "imageUrl",
                "description", 1, 2, 1, true);

        testPresenter = new ProductCommentPresenterImpl(testProduct);
        testPresenter.takeView(mockView);
    }

    private void prepareDagger() {

        AppComponent appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(mockContext))
                .build();

        RootActivity.RootComponent rootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(appComponent)
                .rootModule(new MockRootModule(mockRootPresenter))
                .build();

        CatalogScreen.Component catalogDaggerComponent = DaggerCatalogScreen_Component.builder()
                .rootComponent(rootComponent)
                .module(new MockCatalogModule(mockModel))
                .build();

        ProductDetailsScreen.Component detailsComponent = DaggerProductDetailsScreen_Component.builder()
                .component(catalogDaggerComponent)
                .module(new MockProductDetailsModule())
                .build();

        productCommentDaggerComponent = DaggerProductCommentScreen_Component.builder()
                .component(detailsComponent)
                .module(new MockProductCommentModule(mockViewModel))
                .build();
    }

    @Test
    public void onSaveButtonClick() throws Exception {
        when(mockViewModel.getComment()).thenReturn("comment");
        when(mockViewModel.getRating()).thenReturn(5f);

        testPresenter.onSaveButtonClick();

        verify(mockModel, times(1)).addProductComment(eq(testProduct), any(ProductCommentRealm.class));
        verify(mockFlow, times(1)).goBack();
    }

    @Test
    public void initActionBar() throws Exception {
        verify(mockActionBarBuilder, times(1)).setTitle(testProduct.getProductName());
        verify(mockActionBarBuilder, times(1)).setBackArrow(true);
        verify(mockActionBarBuilder, times(1)).build();
    }
}