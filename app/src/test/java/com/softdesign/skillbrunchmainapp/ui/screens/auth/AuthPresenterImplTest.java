package com.softdesign.skillbrunchmainapp.ui.screens.auth;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;

import com.facebook.CallbackManager;
import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.network.error.ForbiddenApiError;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ActivityResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.di.components.AppComponent;
import com.softdesign.skillbrunchmainapp.di.components.DaggerAppComponent;
import com.softdesign.skillbrunchmainapp.di.modules.AppModule;
import com.softdesign.skillbrunchmainapp.di.modules.MockRootModule;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.DaggerCatalogScreen_Component;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.MockCatalogModule;
import com.softdesign.skillbrunchmainapp.ui.screens.root.ActionBarBuilder;
import com.softdesign.skillbrunchmainapp.ui.screens.root.DaggerRootActivity_RootComponent;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;
import com.softdesign.skillbrunchmainapp.ui.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.ui.view_models.AuthViewModel;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import rx.Observable;
import rx.subjects.PublishSubject;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.softdesign.skillbrunchmainapp.ui.screens.auth.SocialSdkType.FB;
import static com.softdesign.skillbrunchmainapp.ui.screens.auth.SocialSdkType.TWITTER;
import static com.softdesign.skillbrunchmainapp.ui.screens.auth.SocialSdkType.VK;
import static mortar.bundler.BundleServiceRunner.SERVICE_NAME;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static rx.subjects.PublishSubject.create;

/**
 * @author Sergey Vorobyev
 */
public class AuthPresenterImplTest {

    @Mock
    private Flow mockFlow;

    @Mock
    private AuthModel mockModel;

    @Mock
    private Context mockContext;

    @Mock
    private RootView mockRootView;

    @Mock
    private AuthViewImpl mockView;

    @Mock
    private AuthViewModel mockViewModel;

    @Mock
    private RootPresenter mockRootPresenter;

    @Mock
    private ActionBarBuilder mockActionBarBuilder;

    @Mock
    private CallbackManager mockFbCallbackManager;

    @Mock
    private TwitterAuthClient mockTwitterAuthClient;

    private PublishSubject<ActivityResultDto> testActivityResultSubject = create();
    private AuthScreen.Component authDaggerComponent;
    private AuthPresenterImpl testPresenter;
    private UserInfoRealm testUserInfo;

    @Before
    @SuppressLint("WrongConstant")
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        prepareDagger();

        BundleServiceRunner bundleServiceRunner = new BundleServiceRunner();
        MortarScope mockScope = MortarScope.buildRootScope()
                .withService(SERVICE_NAME, bundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, authDaggerComponent)
                .build("MockScope");

        String flowService = "flow.InternalContextWrapper.FLOW_SERVICE";
        when(mockContext.getSystemService(flowService)).thenReturn(mockFlow);
        when(mockContext.getSystemService(SERVICE_NAME)).thenReturn(bundleServiceRunner);
        when(mockView.getContext()).thenReturn(mockContext);
        when(mockRootPresenter.getView()).thenReturn(mockRootView);
        when(mockContext.getSystemService(MortarScope.class.getName())).thenReturn(mockScope);

        when(mockActionBarBuilder.setBackArrow(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.setVisibility(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockRootPresenter.newActionBarBuilder()).thenReturn(mockActionBarBuilder);

        testUserInfo = new UserInfoRealm("userId", "username", "+79022582585", "http://avatar_url",
                true, true);
        testPresenter = new AuthPresenterImpl(testActivityResultSubject,
                mockFbCallbackManager, mockTwitterAuthClient);
    }

    private void prepareDagger() {
        AppComponent appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(mockContext))
                .build();

        RootActivity.RootComponent rootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(appComponent)
                .rootModule(new MockRootModule(mockRootPresenter))
                .build();

        CatalogScreen.Component catalogComponent = DaggerCatalogScreen_Component.builder()
                .rootComponent(rootComponent)
                .module(new MockCatalogModule())
                .build();

        authDaggerComponent = DaggerAuthScreen_Component.builder()
                .component(catalogComponent)
                .module(new MockAuthModule(mockModel, mockViewModel))
                .build();
    }

    @Test
    public void onLoad_NEWER_SHOW_ERROR() throws Exception {
        testPresenter.takeView(mockView);
        verify(mockRootView, never()).showMessage(anyString());
        verify(mockView, times(1)).bindViewModel(any(AbsViewModel.class));
        verify(mockRootPresenter, times(1)).newActionBarBuilder();
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void onEmailChanged_EmailAndPasswordValid() throws Exception {
        when(mockViewModel.getEmail()).thenReturn("mail@mail.com");
        when(mockViewModel.getPassword()).thenReturn("12345678");
        when(mockViewModel.getVisibilityProgress()).thenReturn(GONE);
        when(mockViewModel.getVisibilityInputs()).thenReturn(VISIBLE);

        testPresenter.takeView(mockView);
        testPresenter.onEmailChanged();

        verify(mockViewModel, times(1)).getEmail();
        verify(mockViewModel, times(1)).getPassword();
        verify(mockViewModel, times(1)).setEmailValid(true);
        verify(mockViewModel, times(2)).setLoginButtonEnabled(true);
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void onEmailChanged_EmailValidPasswordNotValid() throws Exception {
        when(mockViewModel.getEmail()).thenReturn("mail@mail.com");
        when(mockViewModel.getPassword()).thenReturn("123");
        when(mockViewModel.getVisibilityProgress()).thenReturn(GONE);
        when(mockViewModel.getVisibilityInputs()).thenReturn(VISIBLE);

        testPresenter.takeView(mockView);
        testPresenter.onEmailChanged();

        verify(mockViewModel, times(1)).getEmail();
        verify(mockViewModel, times(1)).getPassword();
        verify(mockViewModel, times(1)).setEmailValid(true);
        verify(mockViewModel, times(1)).setLoginButtonEnabled(true);
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void onEmailChanged_EmailNotValidPasswordValid() throws Exception {
        when(mockViewModel.getEmail()).thenReturn("mail");
        when(mockViewModel.getPassword()).thenReturn("12345678");
        when(mockViewModel.getVisibilityProgress()).thenReturn(GONE);
        when(mockViewModel.getVisibilityInputs()).thenReturn(VISIBLE);

        testPresenter.takeView(mockView);
        testPresenter.onEmailChanged();

        verify(mockViewModel, times(1)).getEmail();
        verify(mockViewModel, times(1)).getPassword();
        verify(mockViewModel, times(1)).setEmailValid(false);
        verify(mockViewModel, times(1)).setLoginButtonEnabled(false);
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void onPasswordChanged_PasswordAndEmailValid() throws Exception {
        when(mockViewModel.getEmail()).thenReturn("mail@mail.com");
        when(mockViewModel.getPassword()).thenReturn("12345678");
        when(mockViewModel.getVisibilityProgress()).thenReturn(GONE);
        when(mockViewModel.getVisibilityInputs()).thenReturn(VISIBLE);

        testPresenter.takeView(mockView);
        testPresenter.onPasswordChanged();

        verify(mockViewModel, times(1)).getEmail();
        verify(mockViewModel, times(1)).getPassword();
        verify(mockViewModel, times(1)).setPasswordValid(true);
        verify(mockViewModel, times(2)).setLoginButtonEnabled(true);
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void onPasswordChanged_PasswordValidEmailNotValid() throws Exception {
        when(mockViewModel.getEmail()).thenReturn("mail");
        when(mockViewModel.getPassword()).thenReturn("12345678");
        when(mockViewModel.getVisibilityProgress()).thenReturn(GONE);
        when(mockViewModel.getVisibilityInputs()).thenReturn(VISIBLE);

        testPresenter.takeView(mockView);
        testPresenter.onPasswordChanged();

        verify(mockViewModel, times(1)).getEmail();
        verify(mockViewModel, times(1)).getPassword();
        verify(mockViewModel, times(1)).setPasswordValid(true);
        verify(mockViewModel, times(1)).setLoginButtonEnabled(true);
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void onPasswordChanged_PasswordNotValidEmailValid() throws Exception {
        when(mockViewModel.getEmail()).thenReturn("mail@mail.com");
        when(mockViewModel.getPassword()).thenReturn("123");
        when(mockViewModel.getVisibilityProgress()).thenReturn(GONE);
        when(mockViewModel.getVisibilityInputs()).thenReturn(VISIBLE);

        testPresenter.takeView(mockView);
        testPresenter.onPasswordChanged();

        verify(mockViewModel, times(1)).getEmail();
        verify(mockViewModel, times(1)).getPassword();
        verify(mockViewModel, times(1)).setPasswordValid(false);
        verify(mockViewModel, times(1)).setLoginButtonEnabled(false);
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void onLoginButtonClick_CheckAuthorisationTrue() throws Exception {
        when(mockModel.isAuthenticated()).thenReturn(true);

        testPresenter.takeView(mockView);
        testPresenter.onLoginButtonClick();

        verify(mockModel, times(1)).logout();
        verify(mockViewModel, times(1)).setLoginButtonText(R.string.auth_button_log_in);
    }

    @Test
    public void onLoginButtonClick_CheckAuthorisationFalseInputsInvisible() throws Exception {
        when(mockModel.isAuthenticated()).thenReturn(false);
        when(mockViewModel.getVisibilityInputs()).thenReturn(GONE);
        when(mockViewModel.getEmail()).thenReturn("any mail");
        when(mockViewModel.getPassword()).thenReturn("pass");

        testPresenter.takeView(mockView);
        testPresenter.onLoginButtonClick();

        verify(mockViewModel, times(1)).setVisibilityInputs(VISIBLE);
        verify(mockViewModel, times(1)).setVisibilityCatalogButton(GONE);
        verify(mockViewModel, times(1)).setVisibilitySocialButtons(VISIBLE);
        verify(mockView, times(1)).slideAnimateHideShowInputs(true);
        verify(mockView, times(1)).animateHideShowSocialButtons(true);
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void onLoginButtonClick_CheckAuthorisationFalseInputsVisibleLoginTrue() throws Exception {
        when(mockModel.isAuthenticated()).thenReturn(false);
        when(mockModel.login(anyString(), anyString())).thenReturn(Observable.just(new UserInfoRealm()));
        when(mockViewModel.getEmail()).thenReturn("any mail");
        when(mockViewModel.getPassword()).thenReturn("pass");
        when(mockViewModel.getVisibilityInputs()).thenReturn(VISIBLE);

        testPresenter.takeView(mockView);
        testPresenter.onLoginButtonClick();

        verify(mockViewModel, atLeast(2)).setVisibilityProgress(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilityInputs(GONE);
        verify(mockViewModel, atLeast(1)).setVisibilitySocialButtons(anyInt());
        verify(mockViewModel, atLeast(2)).setLoginButtonEnabled(anyBoolean());
        verify(mockViewModel, atLeast(1)).setVisibilityCatalogButton(anyInt());
        verify(mockViewModel, times(1)).setLoginButtonText(R.string.auth_button_log_out);
        verify(mockView, times(1)).alphaAnimateHideShowInputs(false);
        verify(mockView, times(2)).animateHideShowProgress(anyBoolean());
        verify(mockView, times(1)).animateHideShowSocialButtons(anyBoolean());
        verify(mockView, times(1)).showMessageLoggedIn();
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void onLoginButtonClick_CheckAuthorisationFalseInputsVisibleLoginFalse() throws Exception {
        when(mockModel.isAuthenticated()).thenReturn(false);
        when(mockViewModel.getEmail()).thenReturn("any mail");
        when(mockViewModel.getPassword()).thenReturn("pass");
        when(mockViewModel.getVisibilityInputs()).thenReturn(VISIBLE);
        when(mockModel.login(anyString(), anyString())).thenReturn(Observable.error(new ForbiddenApiError()));

        testPresenter.takeView(mockView);
        testPresenter.onLoginButtonClick();

        verifyFinishSocialLoginError();
    }

    @Test
    public void onFbButtonClick_FbLoggedInSuccess() throws Exception {
        when(mockModel.checkFbAuth()).thenReturn(Observable.just(true));
        when(mockModel.getFbUser()).thenReturn(Observable.just(testUserInfo));
        when(mockModel.loginSocial(testUserInfo, FB)).thenReturn(Observable.just(testUserInfo));

        testPresenter.takeView(mockView);
        testPresenter.onFbButtonClick();

        verifyFinishSocialLoginSuccess();
    }

    @Test
    public void onFbButtonClick_FbLoggedInError() throws Exception {
        when(mockModel.checkFbAuth()).thenReturn(Observable.just(true));
        when(mockModel.getFbUser()).thenReturn(Observable.just(testUserInfo));
        when(mockModel.loginSocial(testUserInfo, FB)).thenReturn(Observable.error(new Exception()));

        testPresenter.takeView(mockView);
        testPresenter.onFbButtonClick();

        verifyFinishSocialLoginError();
    }

    @Test
    public void onFbButtonClick_FbNotLoggedIn() throws Exception {
        when(mockModel.checkFbAuth()).thenReturn(Observable.just(false));

        testPresenter.takeView(mockView);
        testPresenter.onFbButtonClick();

        verify(mockRootPresenter, times(1)).startLoginFb();
    }

    @Test
    public void onVkButtonClick_VkLoggedInSuccess() throws Exception {
        when(mockModel.checkVkAuth()).thenReturn(Observable.just(true));
        when(mockModel.getVkUser()).thenReturn(Observable.just(testUserInfo));
        when(mockModel.loginSocial(testUserInfo, VK)).thenReturn(Observable.just(testUserInfo));

        testPresenter.takeView(mockView);
        testPresenter.onVkButtonClick();

        verifyFinishSocialLoginSuccess();
    }

    @Test
    public void onVkButtonClick_VkLoggedInError() throws Exception {
        when(mockModel.checkVkAuth()).thenReturn(Observable.just(true));
        when(mockModel.getVkUser()).thenReturn(Observable.just(testUserInfo));
        when(mockModel.loginSocial(testUserInfo, VK)).thenReturn(Observable.error(new Exception()));

        testPresenter.takeView(mockView);
        testPresenter.onVkButtonClick();

        verifyFinishSocialLoginError();
    }

    @Test
    public void onVkButtonClick_VkNotLoggedIn() throws Exception {
        when(mockModel.checkVkAuth()).thenReturn(Observable.just(false));

        testPresenter.takeView(mockView);
        testPresenter.onVkButtonClick();

        verify(mockRootPresenter, times(1)).startLoginVk();
    }

    @Test
    public void onTwitterButtonClick_TwitterLoggedInSuccess() throws Exception {
        when(mockModel.checkTwitterAuth()).thenReturn(Observable.just(true));
        when(mockModel.getTwitterUser()).thenReturn(Observable.just(testUserInfo));
        when(mockModel.loginSocial(testUserInfo, TWITTER)).thenReturn(Observable.just(testUserInfo));

        testPresenter.takeView(mockView);
        testPresenter.onTwitterButtonClick();

        verifyFinishSocialLoginSuccess();
    }

    @Test
    public void onTwitterButtonClick_TwitterLoggedInError() throws Exception {
        when(mockModel.checkTwitterAuth()).thenReturn(Observable.just(true));
        when(mockModel.getTwitterUser()).thenReturn(Observable.just(testUserInfo));
        when(mockModel.loginSocial(testUserInfo, TWITTER)).thenReturn(Observable.error(new Exception()));

        testPresenter.takeView(mockView);
        testPresenter.onTwitterButtonClick();

        verifyFinishSocialLoginError();
    }

    @Test
    public void onTwitterButtonClick_TwitterNotLoggedIn() throws Exception {
        when(mockModel.checkTwitterAuth()).thenReturn(Observable.just(false));

        testPresenter.takeView(mockView);
        testPresenter.onTwitterButtonClick();

        verify(mockRootPresenter, times(1)).startLoginTwitter(mockTwitterAuthClient);
    }

    @Test
    public void handleActivityResult_FbSuccess() throws Exception {
        final int fbCode = 64206;
        ActivityResultDto fbActivityResult = new ActivityResultDto(fbCode, RESULT_OK, new Intent());

        when(mockModel.getFbUser()).thenReturn(Observable.just(testUserInfo));
        when(mockModel.loginSocial(testUserInfo, FB)).thenReturn(Observable.just(testUserInfo));

        testPresenter.takeView(mockView);
        testActivityResultSubject.onNext(fbActivityResult);

        verify(mockFbCallbackManager, times(1)).onActivityResult(eq(fbCode), eq(RESULT_OK), any(Intent.class));
        verify(mockViewModel, atLeast(1)).setVisibilityProgress(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilityInputs(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilitySocialButtons(anyInt());
        verify(mockViewModel, atLeast(1)).setLoginButtonEnabled(anyBoolean());
        verify(mockViewModel, times(1)).setLoginButtonText(R.string.auth_button_log_out);
        verify(mockView, times(1)).animateHideShowProgress(anyBoolean());
        verify(mockView, times(1)).showMessageLoggedIn();
        verify(mockView, never()).showMessageLoginError();
    }

    @Test
    public void handleActivityResult_FbCancel() throws Exception {
        final int fbCode = 64206;
        ActivityResultDto fbActivityResult = new ActivityResultDto(fbCode, RESULT_CANCELED, new Intent());

        testPresenter.takeView(mockView);
        testActivityResultSubject.onNext(fbActivityResult);

        verify(mockViewModel, atLeast(1)).setVisibilityProgress(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilityInputs(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilitySocialButtons(anyInt());
        verify(mockViewModel, atLeast(1)).setLoginButtonEnabled(anyBoolean());
        verify(mockViewModel, times(1)).setLoginButtonText(R.string.auth_button_log_in);
        verify(mockView, times(1)).alphaAnimateHideShowInputs(anyBoolean());
        verify(mockView, times(1)).animateHideShowProgress(anyBoolean());
        verify(mockView, times(1)).animateHideShowSocialButtons(anyBoolean());
        verify(mockView, times(1)).showMessageLoginError();
        verify(mockView, never()).showMessageLoggedIn();
    }

    @Test
    public void handleActivityResult_VkSuccess() throws Exception {
        int vkCode = 10485;
        ActivityResultDto vkActivityResult = new ActivityResultDto(vkCode, RESULT_OK, new Intent());

        when(mockModel.getVkUser()).thenReturn(Observable.just(testUserInfo));
        when(mockModel.loginSocial(testUserInfo, VK)).thenReturn(Observable.just(testUserInfo));

        testPresenter.takeView(mockView);
        testActivityResultSubject.onNext(vkActivityResult);

        verify(mockViewModel, atLeast(1)).setVisibilityProgress(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilityInputs(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilitySocialButtons(anyInt());
        verify(mockViewModel, atLeast(1)).setLoginButtonEnabled(anyBoolean());
        verify(mockViewModel, times(1)).setLoginButtonText(R.string.auth_button_log_out);
        verify(mockView, times(1)).animateHideShowProgress(anyBoolean());
        verify(mockView, times(1)).showMessageLoggedIn();
        verify(mockView, never()).showMessageLoginError();
    }

    @Test
    public void handleActivityResult_VkCancel() throws Exception {
        int vkCode = 10485;
        ActivityResultDto vkActivityResult = new ActivityResultDto(vkCode, RESULT_CANCELED, new Intent());

        testPresenter.takeView(mockView);
        testActivityResultSubject.onNext(vkActivityResult);

        verify(mockViewModel, atLeast(1)).setVisibilityProgress(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilityInputs(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilitySocialButtons(anyInt());
        verify(mockViewModel, atLeast(1)).setLoginButtonEnabled(anyBoolean());
        verify(mockViewModel, times(1)).setLoginButtonText(R.string.auth_button_log_in);
        verify(mockView, times(1)).alphaAnimateHideShowInputs(anyBoolean());
        verify(mockView, times(1)).animateHideShowProgress(anyBoolean());
        verify(mockView, times(1)).animateHideShowSocialButtons(anyBoolean());
        verify(mockView, times(1)).showMessageLoginError();
        verify(mockView, never()).showMessageLoggedIn();
    }

    @Test
    public void handleActivityResult_TwitterSuccess() throws Exception {
        int twitterCode = 140;
        ActivityResultDto vkActivityResult = new ActivityResultDto(twitterCode, RESULT_OK, new Intent());

        when(mockModel.getTwitterUser()).thenReturn(Observable.just(testUserInfo));
        when(mockModel.loginSocial(testUserInfo, TWITTER)).thenReturn(Observable.just(testUserInfo));

        testPresenter.takeView(mockView);
        testActivityResultSubject.onNext(vkActivityResult);

        verify(mockTwitterAuthClient, times(1)).onActivityResult(eq(twitterCode), eq(RESULT_OK),
                any(Intent.class));
        verify(mockViewModel, atLeast(1)).setVisibilityProgress(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilityInputs(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilitySocialButtons(anyInt());
        verify(mockViewModel, atLeast(1)).setLoginButtonEnabled(anyBoolean());
        verify(mockViewModel, times(1)).setLoginButtonText(R.string.auth_button_log_out);
        verify(mockView, times(1)).animateHideShowProgress(anyBoolean());
        verify(mockView, times(1)).showMessageLoggedIn();
        verify(mockView, never()).showMessageLoginError();
    }

    @Test
    public void handleActivityResult_TwitterCancel() throws Exception {
        int twitterCode = 140;
        ActivityResultDto twitterActivityResult = new ActivityResultDto(twitterCode,
                RESULT_CANCELED, new Intent());

        testPresenter.takeView(mockView);
        testActivityResultSubject.onNext(twitterActivityResult);

        verify(mockViewModel, atLeast(1)).setVisibilityProgress(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilityInputs(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilitySocialButtons(anyInt());
        verify(mockViewModel, atLeast(1)).setLoginButtonEnabled(anyBoolean());
        verify(mockViewModel, times(1)).setLoginButtonText(R.string.auth_button_log_in);
        verify(mockView, times(1)).alphaAnimateHideShowInputs(anyBoolean());
        verify(mockView, times(1)).animateHideShowProgress(anyBoolean());
        verify(mockView, times(1)).animateHideShowSocialButtons(anyBoolean());
        verify(mockView, times(1)).showMessageLoginError();
        verify(mockView, never()).showMessageLoggedIn();
    }

    @Test
    public void onBackPressed_WhenInputsVisible() throws Exception {
        when(mockViewModel.getVisibilityInputs()).thenReturn(VISIBLE);

        testPresenter.takeView(mockView);
        testPresenter.onBackPressed();

        verify(mockViewModel, atLeast(1)).setVisibilityCatalogButton(VISIBLE);
        verify(mockViewModel, atLeast(1)).setVisibilityInputs(GONE);
        verify(mockViewModel, atLeast(1)).setLoginButtonEnabled(true);
        verify(mockViewModel, atLeast(1)).setVisibilitySocialButtons(INVISIBLE);
        verify(mockView, times(1)).slideAnimateHideShowInputs(false);
        verify(mockView, times(1)).animateHideShowSocialButtons(false);
    }

    @Test
    public void onShowCatalogButtonClick() throws Exception {
        testPresenter.takeView(mockView);

        testPresenter.onShowCatalogButtonClick();

        verify(mockFlow, times(1)).goBack();
    }

    @Test
    public void onEnterScope_WhenCheckAuthorisationTrue() throws Exception {
        when(mockModel.isAuthenticated()).thenReturn(true);

        testPresenter.takeView(mockView);

        verify(mockViewModel, times(1)).setLoginButtonText(R.string.auth_button_log_out);
    }

    @Test
    public void onEnterScope_WhenCheckAuthorisationFalse() throws Exception {
        when(mockModel.isAuthenticated()).thenReturn(false);

        testPresenter.takeView(mockView);

        verify(mockViewModel, times(1)).setVisibilityInputs(GONE);
        verify(mockViewModel, times(1)).setVisibilityCatalogButton(VISIBLE);
        verify(mockViewModel, times(1)).setVisibilitySocialButtons(INVISIBLE);
        verify(mockViewModel, times(1)).setVisibilityProgress(GONE);
        verify(mockViewModel, times(1)).setLoginButtonEnabled(true);
        verify(mockViewModel, times(1)).setLoginButtonText(R.string.auth_button_log_in);
    }

    @Test
    public void initActionBar() throws Exception {
        testPresenter.takeView(mockView);

        verify(mockRootPresenter, times(1)).newActionBarBuilder();
        verify(mockActionBarBuilder, times(1)).setBackArrow(true);
        verify(mockActionBarBuilder, times(1)).setVisibility(false);
        verify(mockActionBarBuilder, times(1)).build();
    }

    private void verifyFinishSocialLoginSuccess() {
        verify(mockViewModel, atLeast(1)).setVisibilityProgress(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilityInputs(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilitySocialButtons(anyInt());
        verify(mockViewModel, atLeast(1)).setLoginButtonEnabled(anyBoolean());
        verify(mockViewModel, times(1)).setLoginButtonText(R.string.auth_button_log_out);
        verify(mockView, times(1)).alphaAnimateHideShowInputs(anyBoolean());
        verify(mockView, times(2)).animateHideShowProgress(anyBoolean());
        verify(mockView, times(1)).animateHideShowSocialButtons(anyBoolean());
        verify(mockView, times(1)).showMessageLoggedIn();
        verify(mockView, never()).showMessageLoginError();
    }

    private void verifyFinishSocialLoginError() {
        verify(mockViewModel, atLeast(1)).setVisibilityProgress(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilityInputs(anyInt());
        verify(mockViewModel, atLeast(1)).setVisibilitySocialButtons(anyInt());
        verify(mockViewModel, atLeast(1)).setLoginButtonEnabled(anyBoolean());
        verify(mockViewModel, times(1)).setLoginButtonText(R.string.auth_button_log_in);
        verify(mockView, times(2)).alphaAnimateHideShowInputs(anyBoolean());
        verify(mockView, times(2)).animateHideShowProgress(anyBoolean());
        verify(mockView, times(2)).animateHideShowSocialButtons(anyBoolean());
        verify(mockView, times(1)).showMessageLoginError();
        verify(mockView, never()).showMessageLoggedIn();
    }
}