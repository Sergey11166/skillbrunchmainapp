package com.softdesign.skillbrunchmainapp.ui.screens.address;

import android.content.Context;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.AddressRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.di.components.AppComponent;
import com.softdesign.skillbrunchmainapp.di.components.DaggerAppComponent;
import com.softdesign.skillbrunchmainapp.di.modules.AppModule;
import com.softdesign.skillbrunchmainapp.di.modules.MockRootModule;
import com.softdesign.skillbrunchmainapp.ui.screens.account.AccountModel;
import com.softdesign.skillbrunchmainapp.ui.screens.account.AccountScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.account.DaggerAccountScreen_Component;
import com.softdesign.skillbrunchmainapp.ui.screens.account.MockAccountModule;
import com.softdesign.skillbrunchmainapp.ui.screens.root.ActionBarBuilder;
import com.softdesign.skillbrunchmainapp.ui.screens.root.DaggerRootActivity_RootComponent;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;
import com.softdesign.skillbrunchmainapp.ui.view_models.AddressViewModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

import static mortar.bundler.BundleServiceRunner.SERVICE_NAME;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Sergey Vorobyev
 */
public class AddressPresenterImplTest {

    @Mock
    private Flow mockFlow;

    @Mock
    private Context mockContext;

    @Mock
    private RootView mockRootView;

    @Mock
    private AccountModel mockModel;

    @Mock
    private AddressViewImpl mockView;

    @Mock
    private AddressViewModel mockViewModel;

    @Mock
    private RootPresenter mockRootPresenter;

    @Mock
    private ActionBarBuilder mockActionBarBuilder;

    private AddressScreen.Component addressDaggerComponent;
    private AddressPresenterImpl testPresenter;
    private UserInfoRealm testUserInfo;

    @Before
    @SuppressWarnings("WrongConstant")
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        prepareDagger();

        BundleServiceRunner bundleServiceRunner = new BundleServiceRunner();
        MortarScope mockScope = MortarScope.buildRootScope()
                .withService(SERVICE_NAME, bundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, addressDaggerComponent)
                .build("MockScope");

        String flowService = "flow.InternalContextWrapper.FLOW_SERVICE";
        when(mockContext.getSystemService(flowService)).thenReturn(mockFlow);
        when(mockContext.getSystemService(SERVICE_NAME)).thenReturn(bundleServiceRunner);
        when(mockView.getContext()).thenReturn(mockContext);
        when(mockRootPresenter.getView()).thenReturn(mockRootView);
        when(mockContext.getSystemService(MortarScope.class.getName())).thenReturn(mockScope);

        when(mockActionBarBuilder.setTitle(anyInt())).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.setBackArrow(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.setVisibility(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockRootPresenter.newActionBarBuilder()).thenReturn(mockActionBarBuilder);

        testUserInfo = new UserInfoRealm("userId", "username", "+79022582585",
                "http://avatar_url", true, true);
        testUserInfo.addAddress(new AddressRealm());
    }

    private void prepareDagger() {

        AppComponent appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(mockContext))
                .build();

        RootActivity.RootComponent rootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(appComponent)
                .rootModule(new MockRootModule(mockRootPresenter))
                .build();

        AccountScreen.Component accountDaggerComponent = DaggerAccountScreen_Component.builder()
                .rootComponent(rootComponent)
                .module(new MockAccountModule(mockModel))
                .build();

        addressDaggerComponent = DaggerAddressScreen_Component.builder()
                .component(accountDaggerComponent)
                .module(new MockAddressModule(mockViewModel))
                .build();
    }

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void onSaveButtonClick_NewAddress() throws Exception {
        testPresenter = new AddressPresenterImpl(testUserInfo, -1);
        testPresenter.takeView(mockView);

        testPresenter.onSaveButtonClick();

        verify(mockViewModel, times(1)).getPlace();
        verify(mockViewModel, times(1)).getFullAddress();
        verify(mockViewModel, times(1)).getComment();
        verify(mockModel, times(1)).insertAddress(eq(testUserInfo), any());
        verify(mockFlow, times(1)).goBack();
    }

    @Test
    public void onSaveButtonClick_EditAddress() throws Exception {
        testPresenter = new AddressPresenterImpl(testUserInfo, 0);
        testPresenter.takeView(mockView);

        testPresenter.onSaveButtonClick();

        verify(mockModel, times(1))
                .updateAddress(eq(testUserInfo), eq(0), any(AddressRealm.class));
        verify(mockFlow, times(1)).goBack();
    }

    @Test
    public void onEnterScope_EditAddress() throws Exception {
        testPresenter = new AddressPresenterImpl(testUserInfo, 0);
        testPresenter.takeView(mockView);

        verify(mockViewModel, times(1))
                .setPlace(eq(testUserInfo.getAddresses().get(0).getPlace()));
        verify(mockViewModel, times(1))
                .setFullAddress(eq(testUserInfo.getAddresses().get(0).getFullAddress()));
        verify(mockViewModel, times(1))
                .setComment(eq(testUserInfo.getAddresses().get(0).getComment()));
    }

    @Test
    public void initActionBar_NewAddress() throws Exception {
        testPresenter = new AddressPresenterImpl(testUserInfo, -1);
        testPresenter.takeView(mockView);

        verify(mockRootPresenter, times(1)).newActionBarBuilder();
        verify(mockActionBarBuilder, times(1)).setBackArrow(true);
        verify(mockActionBarBuilder, times(1)).setTitle(eq(R.string.address_toolbar_title_new));
        verify(mockActionBarBuilder, times(1)).build();
    }

    @Test
    public void initActionBar_EditAddress() throws Exception {
        testPresenter = new AddressPresenterImpl(testUserInfo, 0);
        testPresenter.takeView(mockView);

        verify(mockRootPresenter, times(1)).newActionBarBuilder();
        verify(mockActionBarBuilder, times(1)).setBackArrow(true);
        verify(mockActionBarBuilder, times(1)).setTitle(eq(R.string.address_toolbar_title_edit));
        verify(mockActionBarBuilder, times(1)).build();
    }
}