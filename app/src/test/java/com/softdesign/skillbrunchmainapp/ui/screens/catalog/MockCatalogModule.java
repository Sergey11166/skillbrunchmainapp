package com.softdesign.skillbrunchmainapp.ui.screens.catalog;

import static org.mockito.Mockito.mock;

/**
 * @author Sergey Vorobyev
 */
public class MockCatalogModule extends CatalogScreen.Module {

    private CatalogModel model;

    public MockCatalogModule() {
        model = mock(CatalogModel.class);
    }

    public MockCatalogModule(CatalogModel model) {
        this.model = model;
    }

    @Override
    CatalogModel provideModel() {
        return model;
    }
}
