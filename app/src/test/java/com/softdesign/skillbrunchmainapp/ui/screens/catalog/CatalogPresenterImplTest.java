package com.softdesign.skillbrunchmainapp.ui.screens.catalog;

import android.content.Context;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.di.components.AppComponent;
import com.softdesign.skillbrunchmainapp.di.components.DaggerAppComponent;
import com.softdesign.skillbrunchmainapp.di.modules.AppModule;
import com.softdesign.skillbrunchmainapp.di.modules.MockRootModule;
import com.softdesign.skillbrunchmainapp.ui.screens.product.ProductView;
import com.softdesign.skillbrunchmainapp.ui.screens.root.ActionBarBuilder;
import com.softdesign.skillbrunchmainapp.ui.screens.root.DaggerRootActivity_RootComponent;
import com.softdesign.skillbrunchmainapp.ui.screens.root.MenuItemHolder;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import rx.Observable;

import static mortar.bundler.BundleServiceRunner.SERVICE_NAME;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Sergey Vorobyev
 */
public class CatalogPresenterImplTest {

    @Mock
    private Flow mockFlow;

    @Mock
    private Context mockContext;

    @Mock
    private RootView mockRootView;

    @Mock
    private CatalogModel mockModel;

    @Mock
    private CatalogViewImpl mockView;

    @Mock
    private ProductView mockProductView;

    @Mock
    private RootPresenter mockRootPresenter;

    @Mock
    private ActionBarBuilder mockActionBarBuilder;

    private CatalogScreen.Component catalogDaggerComponent;
    private CatalogPresenterImpl testPresenter;

    @Before
    @SuppressWarnings("WrongConstant")
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        prepareDagger();

        BundleServiceRunner bundleServiceRunner = new BundleServiceRunner();
        MortarScope mockScope = MortarScope.buildRootScope()
                .withService(SERVICE_NAME, bundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, catalogDaggerComponent)
                .build("MockScope");

        String flowService = "flow.InternalContextWrapper.FLOW_SERVICE";
        when(mockContext.getSystemService(flowService)).thenReturn(mockFlow);
        when(mockContext.getSystemService(SERVICE_NAME)).thenReturn(bundleServiceRunner);
        when(mockView.getContext()).thenReturn(mockContext);
        when(mockRootPresenter.getView()).thenReturn(mockRootView);
        when(mockContext.getSystemService(MortarScope.class.getName())).thenReturn(mockScope);

        when(mockActionBarBuilder.setTitle(anyInt())).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.setBackArrow(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.setVisibility(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockRootPresenter.newActionBarBuilder()).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.addAction(any(MenuItemHolder.class))).thenReturn(mockActionBarBuilder);

        when(mockModel.getProducts()).thenReturn(Observable.just(new ProductRealm()));
        when(mockView.getCurrentProductView()).thenReturn(mockProductView);

        testPresenter = new CatalogPresenterImpl();
    }

    private void prepareDagger() {

        AppComponent appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(mockContext))
                .build();

        RootActivity.RootComponent rootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(appComponent)
                .rootModule(new MockRootModule(mockRootPresenter))
                .build();

        catalogDaggerComponent = DaggerCatalogScreen_Component.builder()
                .rootComponent(rootComponent)
                .module(new MockCatalogModule(mockModel))
                .build();
    }

    @Test
    public void onBuyButtonClick_AuthorisationTrue() throws Exception {
        int cartCount = 5;
        ProductRealm product = new ProductRealm();

        when(mockModel.isAuthenticated()).thenReturn(true);
        when(mockModel.getCartCount()).thenReturn(cartCount);

        testPresenter.takeView(mockView);
        testPresenter.onBuyButtonClick(product);

        verify(mockModel, times(1)).addToCart(eq(product));
        verify(mockModel, times(2)).getCartCount();
        verify(mockRootPresenter, times(2)).setCartCount(cartCount);
    }

    @Test
    public void onEnterScope() throws Exception {

        when(mockModel.getCartCount()).thenReturn(5);

        testPresenter.takeView(mockView);

        verify(mockModel, times(1)).getCartCount();
    }

    @Test
    public void onLoad_GetProductsSuccess() throws Exception {

        when(mockModel.getCartCount()).thenReturn(5);

        testPresenter.takeView(mockView);

        verify(mockRootView, times(1)).showProgress();
        verify(mockModel, times(1)).getProducts();
        verify(mockRootView, times(1)).hideProgress();
        verify(mockView, times(1)).addPage(any(ProductRealm.class));
    }

    @Test
    public void onLoad_GetProductsError() throws Exception {
        String exceptionMessage = "test exception";

        when(mockModel.getProducts()).thenReturn(Observable.error(new Exception(exceptionMessage)));
        when(mockModel.getCartCount()).thenReturn(5);

        testPresenter.takeView(mockView);

        verify(mockRootView, times(1)).showProgress();
        verify(mockModel, times(1)).getProducts();
        verify(mockRootView, times(1)).hideProgress();
        verify(mockRootView, times(1)).showMessage(eq(exceptionMessage));
        verify(mockView, never()).addPage(any(ProductRealm.class));
    }

    @Test
    public void dropView() throws Exception {
        when(mockModel.getCartCount()).thenReturn(5);

        testPresenter.takeView(mockView);
        testPresenter.dropView(mockView);

        verify(mockView, times(1)).getCurrentPagerPosition();
    }

    @Test
    public void initActionBar() throws Exception {
        testPresenter.takeView(mockView);

        verify(mockRootPresenter, times(1)).newActionBarBuilder();
        verify(mockActionBarBuilder, times(1)).setBackArrow(false);
        verify(mockActionBarBuilder, times(1)).setTitle(eq(R.string.menu_drawer_catalog));
        verify(mockActionBarBuilder, times(1)).addAction(any(MenuItemHolder.class));
        verify(mockActionBarBuilder, times(1)).build();
    }
}