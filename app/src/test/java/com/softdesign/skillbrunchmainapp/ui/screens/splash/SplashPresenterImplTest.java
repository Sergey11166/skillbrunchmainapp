package com.softdesign.skillbrunchmainapp.ui.screens.splash;

import android.annotation.SuppressLint;
import android.content.Context;

import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.di.components.AppComponent;
import com.softdesign.skillbrunchmainapp.di.components.DaggerAppComponent;
import com.softdesign.skillbrunchmainapp.di.modules.AppModule;
import com.softdesign.skillbrunchmainapp.di.modules.MockRootModule;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.ActionBarBuilder;
import com.softdesign.skillbrunchmainapp.ui.screens.root.DaggerRootActivity_RootComponent;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import rx.Observable;

import static flow.Direction.REPLACE;
import static mortar.bundler.BundleServiceRunner.SERVICE_NAME;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Sergey Vorobyev
 */
public class SplashPresenterImplTest {

    @Mock
    private Flow mockFlow;

    @Mock
    private Context mockContext;

    @Mock
    private SplashModel mockModel;

    @Mock
    private RootView mockRootView;

    @Mock
    private SplashViewImpl mockView;

    @Mock
    private RootPresenter mockRootPresenter;

    @Mock
    private ActionBarBuilder mockActionBarBuilder;

    private SplashScreen.Component splashDaggerComponent;
    private SplashPresenterImpl testPresenter;

    @Before
    @SuppressLint("WrongConstant")
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        prepareDagger();

        BundleServiceRunner bundleServiceRunner = new BundleServiceRunner();
        MortarScope mockScope = MortarScope.buildRootScope()
                .withService(SERVICE_NAME, bundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, splashDaggerComponent)
                .build("MockScope");

        String flowService = "flow.InternalContextWrapper.FLOW_SERVICE";
        when(mockContext.getSystemService(flowService)).thenReturn(mockFlow);
        when(mockContext.getSystemService(SERVICE_NAME)).thenReturn(bundleServiceRunner);
        when(mockView.getContext()).thenReturn(mockContext);
        when(mockRootPresenter.getView()).thenReturn(mockRootView);
        when(mockContext.getSystemService(MortarScope.class.getName())).thenReturn(mockScope);

        when(mockActionBarBuilder.setBackArrow(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.setVisibility(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockRootPresenter.newActionBarBuilder()).thenReturn(mockActionBarBuilder);
        when(mockModel.loadProducts()).thenReturn(Observable.empty());

        testPresenter = new SplashPresenterImpl();
    }

    private void prepareDagger() {

        AppComponent appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(mockContext))
                .build();

        RootActivity.RootComponent rootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(appComponent)
                .rootModule(new MockRootModule(mockRootPresenter))
                .build();

        splashDaggerComponent = DaggerSplashScreen_Component.builder()
                .rootComponent(rootComponent)
                .module(new MockSplashModule(mockModel))
                .build();
    }

    @Test
    public void onLoad_Success() throws Exception {

        testPresenter.takeView(mockView);

        verify(mockModel, times(1)).startUpdateProductsWithTimer();
        verify(mockFlow, times(1)).replaceTop(any(CatalogScreen.class), eq(REPLACE));
    }

    @Test
    public void onLoad_Error() throws Exception {
        when(mockModel.loadProducts()).thenReturn(Observable.error(new Exception("test exception")));

        testPresenter.takeView(mockView);

        verify(mockModel, never()).startUpdateProductsWithTimer();
        verify(mockFlow, never()).replaceTop(any(CatalogScreen.class), eq(REPLACE));
    }

    @Test
    public void initActionBar() throws Exception {
        testPresenter.takeView(mockView);

        verify(mockRootPresenter, times(1)).newActionBarBuilder();
        verify(mockActionBarBuilder, times(1)).setBackArrow(true);
        verify(mockActionBarBuilder, times(1)).setVisibility(false);
        verify(mockActionBarBuilder, times(1)).build();
    }
}