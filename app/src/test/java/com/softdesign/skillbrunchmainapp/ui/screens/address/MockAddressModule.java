package com.softdesign.skillbrunchmainapp.ui.screens.address;

import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.ui.view_models.AddressViewModel;

import static org.mockito.Mockito.mock;

/**
 * @author Sergey Vorobyev
 */
class MockAddressModule extends AddressScreen.Module {

    private AddressViewModel viewModel;

    MockAddressModule(AddressViewModel viewModel) {
        super(mock(UserInfoRealm.class), 0);
        this.viewModel = viewModel;
    }

    @Override
    AddressViewModel provideViewModel() {
        return viewModel;
    }
}
