package com.softdesign.skillbrunchmainapp.ui.screens.product;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductViewModel;

import static org.mockito.Mockito.mock;

/**
 * @author Sergey Vorobyev
 */
class MockProductModule extends ProductScreen.Module {

    private ProductViewModel viewModel;

    MockProductModule(ProductViewModel viewModel) {
        super(mock(ProductRealm.class));
        this.viewModel = viewModel;
    }

    @Override
    ProductViewModel provideViewModel() {
        return viewModel;
    }
}
