package com.softdesign.skillbrunchmainapp.ui.screens.account;

import android.content.ContentResolver;
import android.content.Context;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ActivityResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.dto.PermissionsResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.realm.AddressRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.di.components.AppComponent;
import com.softdesign.skillbrunchmainapp.di.components.DaggerAppComponent;
import com.softdesign.skillbrunchmainapp.di.modules.AppModule;
import com.softdesign.skillbrunchmainapp.di.modules.MockRootModule;
import com.softdesign.skillbrunchmainapp.ui.screens.address.AddressScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.ActionBarBuilder;
import com.softdesign.skillbrunchmainapp.ui.screens.root.DaggerRootActivity_RootComponent;
import com.softdesign.skillbrunchmainapp.ui.screens.root.MenuItemHolder;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;
import com.softdesign.skillbrunchmainapp.ui.view_models.AccountViewModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;

import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import rx.Observable;
import rx.subjects.PublishSubject;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.PERMISSION_DENIED;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.softdesign.skillbrunchmainapp.utils.Constants.CAMERA_PERMISSION_REQUEST_CODE;
import static com.softdesign.skillbrunchmainapp.utils.Constants.GALLERY_PERMISSION_REQUEST_CODE;
import static com.softdesign.skillbrunchmainapp.utils.Constants.REQUEST_CODE_CAMERA;
import static com.softdesign.skillbrunchmainapp.utils.Constants.REQUEST_CODE_GALLERY;
import static com.softdesign.skillbrunchmainapp.utils.Constants.REQUEST_CODE_SETTING_CAMERA;
import static com.softdesign.skillbrunchmainapp.utils.Constants.REQUEST_CODE_SETTING_GALLERY;
import static mortar.bundler.BundleServiceRunner.SERVICE_NAME;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static rx.subjects.PublishSubject.create;

/**
 * @author Sergey Vorobyev
 */
public class AccountPresenterImplTest {

    @Mock
    private Flow mockFlow;

    @Mock
    private Context mockContext;

    @Mock
    private RootView mockRootView;

    @Mock
    private AccountModel mockModel;

    @Mock
    private AccountViewImpl mockView;

    @Mock
    private AccountViewModel mockViewModel;

    @Mock
    private RootPresenter mockRootPresenter;

    @Mock
    private ActionBarBuilder mockActionBarBuilder;

    private PublishSubject<PermissionsResultDto> testPermissionsResultSubject = create();
    private PublishSubject<ActivityResultDto> testActivityResultSubject = create();
    private AccountScreen.Component accountDaggerComponent;
    private AccountPresenterImpl testPresenter;
    private UserInfoRealm testUserInfo;

    @Before
    @SuppressWarnings("WrongConstant")
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        prepareDagger();

        BundleServiceRunner bundleServiceRunner = new BundleServiceRunner();
        MortarScope mockScope = MortarScope.buildRootScope()
                .withService(SERVICE_NAME, bundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, accountDaggerComponent)
                .build("MockScope");

        String flowService = "flow.InternalContextWrapper.FLOW_SERVICE";
        when(mockContext.getSystemService(flowService)).thenReturn(mockFlow);
        when(mockContext.getSystemService(SERVICE_NAME)).thenReturn(bundleServiceRunner);
        when(mockContext.getExternalFilesDir(anyString())).thenReturn(new File("someFile"));
        when(mockContext.getContentResolver()).thenReturn(mock(ContentResolver.class));
        when(mockView.getContext()).thenReturn(mockContext);
        when(mockRootPresenter.getView()).thenReturn(mockRootView);
        when(mockContext.getSystemService(MortarScope.class.getName())).thenReturn(mockScope);

        when(mockActionBarBuilder.setTitle(anyInt())).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.setBackArrow(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.setVisibility(anyBoolean())).thenReturn(mockActionBarBuilder);
        when(mockRootPresenter.newActionBarBuilder()).thenReturn(mockActionBarBuilder);
        when(mockActionBarBuilder.addAction(any(MenuItemHolder.class))).thenReturn(mockActionBarBuilder);

        testUserInfo = new UserInfoRealm("userId", "username", "+79022582585", "http://avatar_url",
                true, true);
        when(mockModel.getUserInfo()).thenReturn(Observable.just(testUserInfo));

        testPresenter = new AccountPresenterImpl(testActivityResultSubject, testPermissionsResultSubject);
    }

    private void prepareDagger() {

        AppComponent appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(mockContext))
                .build();

        RootActivity.RootComponent rootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(appComponent)
                .rootModule(new MockRootModule(mockRootPresenter))
                .build();

        accountDaggerComponent = DaggerAccountScreen_Component.builder()
                .rootComponent(rootComponent)
                .module(new MockAccountModule(mockViewModel, mockModel))
                .build();
    }

    @Test
    public void onAddressButtonClick() throws Exception {
        testPresenter.takeView(mockView);

        mockFlow.set(any(AddressScreen.class));
    }

    @Test
    public void onFabClick_EditedModeSelectedAvatar() throws Exception {
        when(mockViewModel.isEditMode()).thenReturn(true);
        testPresenter.setMockSelectedAvatar();

        testPresenter.takeView(mockView);
        testPresenter.onFabClick();

        verify(mockViewModel, times(2)).setAvatarUrl(anyString());
        verify(mockModel, times(1))
                .updateUserInfo(any(UserInfoRealm.class), any(UserInfoRealm.class), eq(true));
        verify(mockViewModel, times(1)).setEditMode(false);
    }

    @Test
    public void onFabClick_EditedModeNotSelectedAvatar() throws Exception {
        when(mockViewModel.isEditMode()).thenReturn(true);

        testPresenter.takeView(mockView);
        testPresenter.onFabClick();

        verify(mockViewModel, times(1)).setAvatarUrl(anyString());
        verify(mockModel, times(1))
                .updateUserInfo(any(UserInfoRealm.class), any(UserInfoRealm.class), eq(false));
        verify(mockViewModel, times(1)).setEditMode(false);
    }

    @Test
    public void onFabClick_NotEditedMode() throws Exception {
        when(mockViewModel.isEditMode()).thenReturn(false);

        testPresenter.takeView(mockView);
        testPresenter.onFabClick();

        verify(mockViewModel, times(1)).setEditMode(eq(true));
        verify(mockViewModel, times(1)).setUsernameVisibility(VISIBLE);
        verify(mockViewModel, times(2)).setAvatarUrl(anyString());
        verify(mockModel, never()).updateUserInfo(any(), any(), anyBoolean());
    }

    @Test
    public void onAvatarClick_EditMode() throws Exception {
        when(mockViewModel.isEditMode()).thenReturn(true);

        testPresenter.takeView(mockView);
        testPresenter.onAvatarClick();

        verify(mockView, times(1)).showChangePhotoDialog();
        verify(mockView, never()).showFullScreenAvatar();
    }

    @Test
    public void onAvatarClick_NotEditMode() throws Exception {
        when(mockViewModel.isEditMode()).thenReturn(false);

        testPresenter.takeView(mockView);
        testPresenter.onAvatarClick();

        verify(mockView, never()).showChangePhotoDialog();
        verify(mockView, times(1)).showFullScreenAvatar();
    }

    @Test
    public void onChoiceFromGallery_PermissionsGranted() throws Exception {
        when(mockRootPresenter.checkAndRequestPermission(any(String[].class), eq(REQUEST_CODE_GALLERY)))
                .thenReturn(true);

        testPresenter.takeView(mockView);
        testPresenter.onChoiceFromGallery();

        String[] expectedPermissions = {READ_EXTERNAL_STORAGE};
        verify(mockRootPresenter, times(1))
                .checkAndRequestPermission(eq(expectedPermissions), eq(REQUEST_CODE_GALLERY));
        verify(mockRootPresenter, times(1))
                .startGalleryForResult(eq(REQUEST_CODE_GALLERY));
    }

    @Test
    public void onChoiceFromGallery_PermissionsNotGranted() throws Exception {
        when(mockRootPresenter.checkAndRequestPermission(any(String[].class), eq(REQUEST_CODE_GALLERY)))
                .thenReturn(false);

        testPresenter.takeView(mockView);
        testPresenter.onChoiceFromGallery();

        String[] expectedPermissions = {READ_EXTERNAL_STORAGE};
        verify(mockRootPresenter, times(1))
                .checkAndRequestPermission(eq(expectedPermissions), eq(REQUEST_CODE_GALLERY));
        verify(mockRootPresenter, never()).startGalleryForResult(anyInt());
    }

    @Test
    public void onChoiceTakePhoto_PermissionsGranted() throws Exception {
        when(mockRootPresenter.checkAndRequestPermission(any(String[].class), eq(REQUEST_CODE_CAMERA)))
                .thenReturn(true);

        testPresenter.takeView(mockView);
        testPresenter.onChoiceTakePhoto();

        String[] expectedPermissions = {CAMERA, WRITE_EXTERNAL_STORAGE};
        verify(mockRootPresenter, times(1))
                .checkAndRequestPermission(eq(expectedPermissions), eq(REQUEST_CODE_CAMERA));
        verify(mockRootPresenter, times(1))
                .startCameraForResult(any(File.class), eq(REQUEST_CODE_CAMERA));
    }

    @Test
    public void onChoiceTakePhoto_PermissionsNotGranted() throws Exception {
        when(mockRootPresenter.checkAndRequestPermission(any(String[].class), eq(REQUEST_CODE_CAMERA)))
                .thenReturn(false);

        testPresenter.takeView(mockView);
        testPresenter.onChoiceTakePhoto();

        String[] expectedPermissions = {CAMERA, WRITE_EXTERNAL_STORAGE};
        verify(mockRootPresenter, times(1))
                .checkAndRequestPermission(eq(expectedPermissions), eq(REQUEST_CODE_CAMERA));
        verify(mockRootPresenter, never()).startCameraForResult(any(File.class), anyInt());
    }

    @Test
    public void onSwipeAddressRemove() throws Exception {
        AddressRealm address = new AddressRealm();

        testPresenter.takeView(mockView);
        testPresenter.onSwipeAddressRemove(address);

        verify(mockModel, times(1))
                .removeAddress(eq(testUserInfo), eq(address));
    }

    @Test
    public void onSwipeAddressEdit() throws Exception {
        testPresenter.takeView(mockView);
        testPresenter.onSwipeAddressEdit(new AddressRealm());

        verify(mockFlow, times(1)).set(any(AddressScreen.class));
    }

    @Test
    public void onSettingButtonClickInCameraPermissionDialog() throws Exception {
        testPresenter.takeView(mockView);
        testPresenter.onSettingButtonClickInCameraPermissionDialog();

        verify(mockRootPresenter, times(1))
                .startSettingForResult(eq(REQUEST_CODE_SETTING_CAMERA));
    }

    @Test
    public void onSettingButtonClickInGalleryPermissionDialog() throws Exception {
        testPresenter.takeView(mockView);
        testPresenter.onSettingButtonClickInGalleryPermissionDialog();

        verify(mockRootPresenter, times(1))
                .startSettingForResult(eq(REQUEST_CODE_SETTING_GALLERY));
    }

    @Test
    public void onBackPressed_EditMode() throws Exception {
        when(mockViewModel.isEditMode()).thenReturn(true);

        testPresenter.takeView(mockView);
        testPresenter.onBackPressed();

        verify(mockViewModel, times(1)).setEditMode(eq(false));
        verify(mockViewModel, times(2)).setUsernameVisibility(GONE);
        verify(mockViewModel, times(2)).setUsername(eq(testUserInfo.getUsername()));
        verify(mockViewModel, times(2)).setPhone(eq(testUserInfo.getPhone()));
        verify(mockViewModel, times(2)).setAvatarUrl(eq(testUserInfo.getAvatarUrl()));
    }

    @Test
    public void onEnterScope() throws Exception {
        testPresenter.takeView(mockView);

        verify(mockModel, times(1)).getUserInfo();
    }

    @Test
    public void onLoad() throws Exception {
        testPresenter.takeView(mockView);

        verify(mockView, times(2)).showAddresses(anyList());
    }

    @Test
    public void initActionBar() throws Exception {
        testPresenter.takeView(mockView);

        verify(mockRootPresenter, times(1)).newActionBarBuilder();
        verify(mockActionBarBuilder, times(1)).setBackArrow(false);
        verify(mockActionBarBuilder, times(1)).setTitle(eq(R.string.menu_drawer_account));
        verify(mockActionBarBuilder, times(1)).addAction(any(MenuItemHolder.class));
        verify(mockActionBarBuilder, times(1)).build();
    }

    @Test
    public void should_StartCameraForResult_SettingCameraAllow() throws Exception {
        testPresenter.takeView(mockView);
        when(mockRootPresenter.checkAndRequestPermission(any(String[].class), anyInt())).thenReturn(true);

        testActivityResultSubject.onNext(new ActivityResultDto(REQUEST_CODE_SETTING_CAMERA,
                1, null));

        verify(mockRootPresenter, times(1))
                .startCameraForResult(any(File.class), eq(REQUEST_CODE_CAMERA));
    }

    @Test
    public void should_StartGalleryForResult_SettingGalleryAllow() throws Exception {
        testPresenter.takeView(mockView);
        when(mockRootPresenter.checkAndRequestPermission(any(String[].class), anyInt())).thenReturn(true);

        testActivityResultSubject.onNext(new ActivityResultDto(REQUEST_CODE_SETTING_GALLERY,
                1, null));

        verify(mockRootPresenter, times(1))
                .startGalleryForResult(REQUEST_CODE_GALLERY);
    }

    @Test
    public void should_StartCameraForResult_SettingCameraDenied() throws Exception {
        testPresenter.takeView(mockView);
        when(mockRootPresenter.checkAndRequestPermission(any(String[].class), anyInt())).thenReturn(false);

        testActivityResultSubject.onNext(new ActivityResultDto(REQUEST_CODE_SETTING_CAMERA,
                1, null));

        verify(mockRootPresenter, never()).startCameraForResult(any(File.class), eq(REQUEST_CODE_CAMERA));
    }

    @Test
    public void should_StartGalleryForResult_SettingGalleryDenied() throws Exception {
        testPresenter.takeView(mockView);
        when(mockRootPresenter.checkAndRequestPermission(any(String[].class), anyInt())).thenReturn(false);

        testActivityResultSubject.onNext(new ActivityResultDto(REQUEST_CODE_SETTING_GALLERY,
                1, null));

        verify(mockRootPresenter, never()).startGalleryForResult(REQUEST_CODE_GALLERY);
    }

    @Test
    public void should_StartCameraForResult_PermissionsGranted() throws Exception {
        testPresenter.takeView(mockView);

        //when(mockRootPresenter.checkAndRequestPermission(any(String[].class), anyInt())).thenReturn(true);

        String[] permissions = {CAMERA, WRITE_EXTERNAL_STORAGE};
        int[] permissionsResult = {PERMISSION_GRANTED, PERMISSION_GRANTED};

        testPermissionsResultSubject.onNext(new PermissionsResultDto(CAMERA_PERMISSION_REQUEST_CODE,
                permissions, permissionsResult));

        verify(mockRootPresenter, times(1))
                .startCameraForResult(any(File.class), eq(REQUEST_CODE_CAMERA));
    }

    @Test
    public void should_ShowCameraNeedGrandPermissionsDialog_PermissionsNotGranted() throws Exception {
        testPresenter.takeView(mockView);

        String[] permissions = {CAMERA, WRITE_EXTERNAL_STORAGE};
        int[] permissionsResult = {PERMISSION_DENIED, PERMISSION_DENIED};

        testPermissionsResultSubject.onNext(new PermissionsResultDto(CAMERA_PERMISSION_REQUEST_CODE,
                permissions, permissionsResult));

        verify(mockRootPresenter, never()).startCameraForResult(any(File.class), eq(REQUEST_CODE_CAMERA));
        verify(mockView, times(1)).showCameraNeedGrandPermissionsDialog();
    }

    @Test
    public void should_StartGalleryForResult_PermissionsGranted() throws Exception {
        testPresenter.takeView(mockView);

        String[] permissions = {WRITE_EXTERNAL_STORAGE};
        int[] permissionsResult = {PERMISSION_GRANTED};

        testPermissionsResultSubject.onNext(new PermissionsResultDto(GALLERY_PERMISSION_REQUEST_CODE,
                permissions, permissionsResult));

        verify(mockRootPresenter, times(1))
                .startGalleryForResult(REQUEST_CODE_GALLERY);
    }

    @Test
    public void should_ShowGalleryNeedGrandPermissionsDialog_PermissionsNotGranted() throws Exception {
        testPresenter.takeView(mockView);

        String[] permissions = {WRITE_EXTERNAL_STORAGE};
        int[] permissionsResult = {PERMISSION_DENIED};

        testPermissionsResultSubject.onNext(new PermissionsResultDto(GALLERY_PERMISSION_REQUEST_CODE,
                permissions, permissionsResult));

        verify(mockRootPresenter, never()).startGalleryForResult(REQUEST_CODE_GALLERY);
        verify(mockView, times(1)).showGalleryNeedGrandPermissionsDialog();
    }
}