package com.softdesign.skillbrunchmainapp.data.managers;

import com.softdesign.skillbrunchmainapp.data.network.RestCallTransformer;
import com.softdesign.skillbrunchmainapp.data.network.RestService;
import com.softdesign.skillbrunchmainapp.data.network.adapters.ProductCommentAddedJsonAdapter;
import com.softdesign.skillbrunchmainapp.data.network.adapters.ProductCommentJsonAdapter;
import com.softdesign.skillbrunchmainapp.data.network.error.ApiError;
import com.softdesign.skillbrunchmainapp.data.network.error.ForbiddenApiError;
import com.softdesign.skillbrunchmainapp.data.network.requests.CartRequest;
import com.softdesign.skillbrunchmainapp.data.network.requests.ProductCommentRequest;
import com.softdesign.skillbrunchmainapp.data.network.responses.AvatarUrlResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.CartResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.ProductCommentAddedResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.ProductResponse;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.squareup.moshi.Moshi;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;
import rx.Observable;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;

import static com.softdesign.skillbrunchmainapp.MockResponses.SUCCESS_CART;
import static com.softdesign.skillbrunchmainapp.MockResponses.SUCCESS_FAVORITES;
import static com.softdesign.skillbrunchmainapp.MockResponses.SUCCESS_GET_FB_PROFILE;
import static com.softdesign.skillbrunchmainapp.MockResponses.SUCCESS_GET_PRODUCTS;
import static com.softdesign.skillbrunchmainapp.MockResponses.SUCCESS_GET_VK_PROFILE;
import static com.softdesign.skillbrunchmainapp.MockResponses.SUCCESS_LOGIN;
import static com.softdesign.skillbrunchmainapp.MockResponses.SUCCESS_UPLOAD_AVATAR;
import static com.softdesign.skillbrunchmainapp.MockResponses.SUCCESS_UPLOAD_COMMENT;
import static com.softdesign.skillbrunchmainapp.utils.Constants.DEFAULT_LAST_MODIFIED;
import static com.softdesign.skillbrunchmainapp.utils.Constants.LAST_MODIFIED_HEADER;
import static okhttp3.MultipartBody.Part.createFormData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Sergey Vorobyev
 */
public class DataManagerTest {

    @Mock
    private RealmManager mockRealmManager;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private PreferencesManager mockPrefManager;

    private DataManager dataManager;
    private MockWebServer mockWebServer = new MockWebServer();
    private UserInfoRealm userInfo = new UserInfoRealm("userId", "username", "+79022582585",
            "http://avatar_url", true, true);
    private CartRequest cartRequest = new CartRequest("productId", 2);

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        RestCallTransformer restCallTransformer = new RestCallTransformer();
        restCallTransformer.setTestMode();

        dataManager = new DataManager(createMockServer().create(RestService.class),
                mockRealmManager, mockPrefManager, restCallTransformer);

        prepareRxSchedulers();
    }

    @After
    public void tearDown() throws Exception {
        mockWebServer.shutdown();
    }

    @Test
    public void login_200() throws Exception {
        prepareDispatcher_200();

        TestSubscriber<UserInfoRealm> testSubscriber = new TestSubscriber<>();
        dataManager.login("anyemail@mail.ru", "password").subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertValueCount(1);
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();

        UserInfoRealm userInfo = testSubscriber.getOnNextEvents().get(0);
        assertNotNull(userInfo);
        assertNotNull(userInfo.getUsername());
        assertNotNull(userInfo.getAvatarUrl());
        assertNotNull(userInfo.getPhone());
        assertNotNull(userInfo.isPushOffers());
        assertNotNull(userInfo.isPushOrderStatus());
        assertNotNull(userInfo.getAddresses());
        assertFalse(userInfo.getAddresses().isEmpty());

        verify(mockPrefManager, times(1)).saveToken(anyString());
        verify(mockRealmManager, times(2)).saveUserInfo(any(UserInfoRealm.class));
    }

    @Test
    public void login_403() throws Exception {
        mockWebServer.enqueue(new MockResponse().setResponseCode(403));

        TestSubscriber<UserInfoRealm> testSubscriber = new TestSubscriber<>();
        dataManager.login("anyemail@mail.ru", "password").subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertError(ForbiddenApiError.class);
        Throwable error = testSubscriber.getOnErrorEvents().get(0);
        assertNotNull(error);
        assertEquals("Wrong username or password", error.getMessage());

        verify(mockPrefManager, never()).saveToken(any());
        verify(mockRealmManager, times(1)).saveUserInfo(any(UserInfoRealm.class));
    }

    @Test
    public void login_500() throws Exception {
        mockWebServer.enqueue(new MockResponse().setResponseCode(500));

        TestSubscriber<UserInfoRealm> testSubscriber = new TestSubscriber<>();
        dataManager.login("anyemail@mail.ru", "password").subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertError(ApiError.class);
        Throwable error = testSubscriber.getOnErrorEvents().get(0);
        assertNotNull(error);
        assertEquals("Server error 500", error.getMessage());

        verify(mockPrefManager, never()).saveToken(any());
        verify(mockRealmManager, times(1)).saveUserInfo(any(UserInfoRealm.class));
    }

    @Test
    public void getProductsFromServer_200() {
        prepareDispatcher_200();

        when(mockPrefManager.getProductsLastModified()).thenReturn(DEFAULT_LAST_MODIFIED);

        TestSubscriber<ProductRealm> testSubscriber = new TestSubscriber<>();
        dataManager.getProductsFromServer().subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertCompleted();
        testSubscriber.assertNoErrors();
        testSubscriber.assertNoValues();

        verify(mockPrefManager, never()).saveProductsLastModified(any());
        verify(mockRealmManager, times(7)).saveProductsFromResponse(any(ProductResponse.class));
        verify(mockRealmManager, times(2)).deleteEntity(eq(ProductRealm.class), anyString());
    }

    @Test
    public void getProductsFromServer_304() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(304));

        when(mockPrefManager.getProductsLastModified()).thenReturn(DEFAULT_LAST_MODIFIED);

        TestSubscriber<ProductRealm> testSubscriber = new TestSubscriber<>();
        dataManager.getProductsFromServer().subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertNoValues();

        verify(mockRealmManager, never()).saveProductsFromResponse(any(ProductResponse.class));
        verify(mockRealmManager, never()).deleteEntity(eq(ProductRealm.class), anyString());
    }

    @Test
    public void uploadUserAvatar_200() {
        prepareDispatcher_200();

        TestSubscriber<AvatarUrlResponse> testSubscriber = new TestSubscriber<>();
        dataManager.uploadUserAvatar(createFormData("name", "value")).subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(1);

        AvatarUrlResponse response = testSubscriber.getOnNextEvents().get(0);
        assertNotNull(response);
        assertNotNull(response.getAvatarUrl());
    }

    @Test
    public void sendProductComment_200() {
        prepareDispatcher_200();

        ProductCommentRequest request = new ProductCommentRequest("any id", "username",
                "avatar url", 3.2f, "comment", new Date());

        TestSubscriber<ProductCommentAddedResponse> testSubscriber = new TestSubscriber<>();
        dataManager.sendProductComment(request, "584f93c0ed0d00001179ac89").subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(1);

        ProductCommentAddedResponse response = testSubscriber.getOnNextEvents().get(0);
        assertNotNull(response);
        assertNotNull(response.getId());
        assertNotNull(response.getProductId());
        assertNotNull(response.getUsername());
        assertNotNull(response.getAvatar());
        assertNotNull(response.getRating());
        assertNotNull(response.getComment());
        assertNotNull(response.getCommentDate());
        assertNotNull(response.isActive());
    }

    @Test
    public void addFavoriteOnServer_200() {
        prepareDispatcher_200();

        when(mockRealmManager.getUserInfo()).thenReturn(Observable.just(userInfo));
        when(mockPrefManager.getToken()).thenReturn("token");

        TestSubscriber<String> testSubscriber = new TestSubscriber<>();
        dataManager.addFavoriteOnServer("productId").subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(3);

        List<String> responses = testSubscriber.getOnNextEvents();

        for (String response : responses) {
            assertNotNull(response);
            assertFalse(response.isEmpty());
        }
    }

    @Test
    public void deleteFavoriteOnServer_200() {
        prepareDispatcher_200();

        when(mockRealmManager.getUserInfo()).thenReturn(Observable.just(userInfo));
        when(mockPrefManager.getToken()).thenReturn("token");

        TestSubscriber<String> testSubscriber = new TestSubscriber<>();
        dataManager.deleteFavoriteOnServer("productId").subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(3);

        List<String> responses = testSubscriber.getOnNextEvents();

        for (String response : responses) {
            assertNotNull(response);
            assertFalse(response.isEmpty());
        }
    }

    @Test
    public void getAllFavoritesFromServer_200() {
        prepareDispatcher_200();

        when(mockRealmManager.getAllProducts()).thenReturn(Observable.empty());
        when(mockRealmManager.getUserInfo()).thenReturn(Observable.just(userInfo));
        when(mockPrefManager.getToken()).thenReturn("token");

        TestSubscriber<String> testSubscriber = new TestSubscriber<>();
        dataManager.getAllFavoritesFromServer().subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(3);

        List<String> responses = testSubscriber.getOnNextEvents();

        for (String response : responses) {
            assertNotNull(response);
            assertFalse(response.isEmpty());
        }
    }

    @Test
    public void addToCartOnServer_200() {
        prepareDispatcher_200();

        when(mockRealmManager.getUserInfo()).thenReturn(Observable.just(userInfo));
        when(mockPrefManager.getToken()).thenReturn("token");

        TestSubscriber<CartResponse> testSubscriber = new TestSubscriber<>();
        dataManager.addToCartOnServer(cartRequest).subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(3);

        assertNotNull(testSubscriber.getOnNextEvents().get(0));
        assertNotNull(testSubscriber.getOnNextEvents().get(1));
        assertNotNull(testSubscriber.getOnNextEvents().get(2));
    }

    @Test
    public void updateProductInCartOnServer_200() {
        prepareDispatcher_200();

        when(mockRealmManager.getUserInfo()).thenReturn(Observable.just(userInfo));
        when(mockPrefManager.getToken()).thenReturn("token");

        TestSubscriber<CartResponse> testSubscriber = new TestSubscriber<>();
        dataManager.updateProductInCartOnServer(cartRequest).subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(3);

        assertNotNull(testSubscriber.getOnNextEvents().get(0));
        assertNotNull(testSubscriber.getOnNextEvents().get(1));
        assertNotNull(testSubscriber.getOnNextEvents().get(2));
    }

    @Test
    public void deleteProductFromCartOnServer_200() {
        prepareDispatcher_200();

        when(mockRealmManager.getUserInfo()).thenReturn(Observable.just(userInfo));
        when(mockPrefManager.getToken()).thenReturn("token");

        TestSubscriber<CartResponse> testSubscriber = new TestSubscriber<>();
        dataManager.deleteProductFromCartOnServer("productId").subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(3);

        assertNotNull(testSubscriber.getOnNextEvents().get(0));
        assertNotNull(testSubscriber.getOnNextEvents().get(1));
        assertNotNull(testSubscriber.getOnNextEvents().get(2));
    }

    @Test
    public void getAllFromCartOnServer_200() {
        prepareDispatcher_200();

        when(mockRealmManager.getUserInfo()).thenReturn(Observable.just(userInfo));
        when(mockPrefManager.getToken()).thenReturn("token");

        TestSubscriber<CartResponse> testSubscriber = new TestSubscriber<>();
        dataManager.getAllFromCartOnServer().subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(3);

        assertNotNull(testSubscriber.getOnNextEvents().get(0));
        assertNotNull(testSubscriber.getOnNextEvents().get(1));
        assertNotNull(testSubscriber.getOnNextEvents().get(2));
    }

    @Test
    public void getProfileVk_200() {
        prepareDispatcher_200();

        TestSubscriber<UserInfoRealm> testSubscriber = new TestSubscriber<>();
        String url = mockWebServer.url("users.get").toString();
        dataManager.getProfileVk(url, "token").subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(1);

        UserInfoRealm profile = testSubscriber.getOnNextEvents().get(0);
        assertNotNull(profile);
        assertNotNull(profile.getUsername());
        assertNotNull(profile.getPhone());
        assertNotNull(profile.getAvatarUrl());
    }

    @Test
    public void getProfileFb_200() {
        prepareDispatcher_200();

        TestSubscriber<UserInfoRealm> testSubscriber = new TestSubscriber<>();
        String url = mockWebServer.url("me").toString();
        dataManager.getProfileFb(url, "token").subscribe(testSubscriber);
        testSubscriber.awaitTerminalEvent();

        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        testSubscriber.assertValueCount(1);

        UserInfoRealm profile = testSubscriber.getOnNextEvents().get(0);
        assertNotNull(profile);
        assertNotNull(profile.getUsername());
        assertNotNull(profile.getAvatarUrl());
    }

    private Retrofit createMockServer() {
        return new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(MoshiConverterFactory.create(new Moshi.Builder()
                        .add(new ProductCommentJsonAdapter())
                        .add(new ProductCommentAddedJsonAdapter())
                        .build()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(new OkHttpClient.Builder().build())
                .build();
    }

    private void prepareDispatcher_200() {
        Dispatcher dispatcher = new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                String path = request.getPath();
                if (path.contains("?")) {
                    path = path.substring(0, path.indexOf("?"));
                }
                switch (path) {
                    case "/login":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setBody(SUCCESS_LOGIN);

                    case "/products":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setHeader(LAST_MODIFIED_HEADER, DEFAULT_LAST_MODIFIED)
                                .setBody(SUCCESS_GET_PRODUCTS);

                    case "/avatar":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setBody(SUCCESS_UPLOAD_AVATAR);

                    case "/products/584f93c0ed0d00001179ac89/comments":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setBody(SUCCESS_UPLOAD_COMMENT);

                    case "/users.get":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setBody(SUCCESS_GET_VK_PROFILE);

                    case "/me":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setBody(SUCCESS_GET_FB_PROFILE);

                    case "/user/userId/favorite":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setBody(SUCCESS_FAVORITES);

                    case "/user/userId/favorite/productId":
                        return new MockResponse()
                                .setResponseCode(202)
                                .setBody(SUCCESS_FAVORITES);

                    case "/user/userId/cart":
                        return new MockResponse()
                                .setResponseCode(200)
                                .setBody(SUCCESS_CART);

                    case "/user/userId/cart/productId":
                        return new MockResponse()
                                .setResponseCode(202)
                                .setBody(SUCCESS_CART);

                    default:
                        return new MockResponse().setResponseCode(404);
                }
            }
        };
        mockWebServer.setDispatcher(dispatcher);
    }

    private void prepareRxSchedulers() {
        RxAndroidPlugins.getInstance().reset();
        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });
    }
}