package com.softdesign.skillbrunchmainapp.di.modules;

import com.softdesign.skillbrunchmainapp.data.storage.dto.ActivityResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.dto.PermissionsResultDto;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootModel;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.view_models.RootViewModel;

import rx.subjects.PublishSubject;

import static org.mockito.Mockito.mock;

/**
 * @author Sergey Vorobyev
 */
public class MockRootModule extends RootModule {

    private RootPresenter presenter;

    public MockRootModule(RootPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    RootPresenter providePresenter(RootModel model, RootViewModel viewModel,
                                   PublishSubject<ActivityResultDto> activityResultSubject,
                                   PublishSubject<PermissionsResultDto> permissionsResultSubject) {
        return presenter;
    }

    @Override
    RootModel provideRootModel() {
        return mock(RootModel.class);
    }
}
