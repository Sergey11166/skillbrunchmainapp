package com.softdesign.skillbrunchmainapp.services;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.softdesign.skillbrunchmainapp.App;
import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;
import com.softdesign.skillbrunchmainapp.utils.L;
import com.softdesign.skillbrunchmainapp.utils.NotificationHelper;

import java.util.Date;

/**
 * @author Sergey Vorobyev
 */
public class PushService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        L.d("onMessageReceived: " + remoteMessage);

        RemoteMessage.Notification notification = remoteMessage.getNotification();

        String id = remoteMessage.getMessageId();
        String type = remoteMessage.getData().get("type");
        String meta = remoteMessage.getData().get("meta");
        String title = notification.getTitle();
        String content = notification.getBody();
        Date date = new Date(remoteMessage.getSentTime());

        NotificationRealm notificationRealm = new NotificationRealm(id, type, meta, title, content, date);
        App.getModelComponent().getDataManager().saveNotificationToRealm(notificationRealm);

        NotificationHelper.createNotification(notification, type, meta);
        super.onMessageReceived(remoteMessage);

    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }
}
