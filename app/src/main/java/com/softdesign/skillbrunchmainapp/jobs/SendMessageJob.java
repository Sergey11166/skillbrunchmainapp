package com.softdesign.skillbrunchmainapp.jobs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.softdesign.skillbrunchmainapp.App;
import com.softdesign.skillbrunchmainapp.data.network.requests.ProductCommentRequest;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductCommentRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.utils.L;

import java.util.Date;

import io.realm.Realm;

import static com.softdesign.skillbrunchmainapp.utils.Constants.INITIAL_BACK_OFF_IN_MS;

/**
 * @author Sergey Vorobyev
 */
public class SendMessageJob extends Job {

    private static final String TAG = "SendMessageJob";

    private final ProductCommentRealm comment;
    private final String productId;

    public SendMessageJob(ProductCommentRealm comment, String productId) {
        super(new Params(JobPriority.MID)
                .requireNetwork()
                .persist()
                .groupBy("Comments"));
        this.productId = productId;
        this.comment = comment;
    }

    @Override
    public void onAdded() {
        L.d(TAG, "MESSAGE onAdded");
        Realm realm = Realm.getDefaultInstance();
        ProductRealm product = realm.where(ProductRealm.class)
                .equalTo("id", productId)
                .findFirst();

        realm.executeTransaction(r -> product.getComments().add(comment));
        realm.close();
    }

    @Override
    public void onRun() throws Throwable {
        L.d(TAG, "MESSAGE onRun");
        Realm realm = Realm.getDefaultInstance();
        UserInfoRealm userInfo = realm.where(UserInfoRealm.class).findFirst();
        comment.setUsername(userInfo.getUsername());
        if (userInfo.getAvatarUrl().contains("http")) {
            comment.setAvatar(userInfo.getAvatarUrl());
        } else {
            comment.setAvatar("https://mir-s3-cdn-cf.behance.net/project_modules/disp/3c3ad823775971.5632898dbfbfa.png");
        }
        App.getModelComponent().getDataManager()
                .sendProductComment(new ProductCommentRequest(comment), productId)
                .subscribe(commentAddedResponse -> {
                    ProductCommentRealm oldComment = realm.where(ProductCommentRealm.class)
                            .equalTo("id", comment.getId())
                            .findFirst();

                    ProductRealm product = realm.where(ProductRealm.class)
                            .equalTo("id", productId)
                            .findFirst();

                    ProductCommentRealm newComment = new ProductCommentRealm(commentAddedResponse);
                    realm.executeTransaction(r -> {
                        oldComment.deleteFromRealm();
                        product.getComments().add(newComment);
                    });
                    realm.close();
                });
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        L.d(TAG, "MESSAGE onCancel");
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        L.d(TAG, "MESSAGE shouldReRunOnThrowable " + runCount + " of " + maxRunCount + " s " + new Date());
        return RetryConstraint.createExponentialBackoff(runCount, INITIAL_BACK_OFF_IN_MS);
    }
}
