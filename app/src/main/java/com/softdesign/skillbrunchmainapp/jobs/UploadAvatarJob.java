package com.softdesign.skillbrunchmainapp.jobs;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.softdesign.skillbrunchmainapp.App;
import com.softdesign.skillbrunchmainapp.data.managers.DataManager;
import com.softdesign.skillbrunchmainapp.utils.L;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody.Part;
import okhttp3.RequestBody;

import static com.softdesign.skillbrunchmainapp.utils.Constants.INITIAL_BACK_OFF_IN_MS;

/**
 * @author Sergey Vorobyev
 */
public class UploadAvatarJob extends Job {

    private static final String TAG = "UploadAvatarJob";

    private String imageUri;

    public UploadAvatarJob(String imageUri) {
        super(new Params(JobPriority.HIGH)
                .requireNetwork()
                .persist());
        this.imageUri = imageUri;
    }

    @Override
    public void onAdded() {
        L.d(TAG, "UPLOAD onAdded");
    }

    @Override
    public void onRun() throws Throwable {
        L.d(TAG, "UPLOAD onRun");
        File file = new File(Uri.parse(imageUri).getPath());
        RequestBody body = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        Part part = Part.createFormData("avatar", file.getName(), body);
        DataManager dataManager = App.getModelComponent().getDataManager();
        dataManager.uploadUserAvatar(part).subscribe(
                response -> dataManager.saveUserAvatarToRealm(response.getAvatarUrl()),
                L::e);
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        L.d(TAG, "UPLOAD onCancel");
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        L.d(TAG, "UPLOAD shouldReRunOnThrowable");
        return RetryConstraint.createExponentialBackoff(runCount, INITIAL_BACK_OFF_IN_MS);
    }
}
