package com.softdesign.skillbrunchmainapp.di.modules;

import com.softdesign.skillbrunchmainapp.data.storage.dto.ActivityResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.dto.PermissionsResultDto;
import com.softdesign.skillbrunchmainapp.di.scopes.DaggerScope;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootModel;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootModelImpl;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenterImpl;
import com.softdesign.skillbrunchmainapp.ui.view_models.RootViewModel;

import dagger.Module;
import dagger.Provides;
import rx.subjects.PublishSubject;

/**
 * @author Sergey Vorobyev.
 */
@Module
public class RootModule {

    @Provides
    @DaggerScope(RootActivity.class)
    RootViewModel provideViewModel() {
        return new RootViewModel();
    }

    @Provides
    @DaggerScope(RootActivity.class)
    RootModel provideRootModel() {
        return new RootModelImpl();
    }

    @Provides
    @DaggerScope(RootActivity.class)
    PublishSubject<ActivityResultDto> provideActivityResultSubject() {
        return PublishSubject.create();
    }

    @Provides
    @DaggerScope(RootActivity.class)
    PublishSubject<PermissionsResultDto> providePermissionsResultSubject() {
        return PublishSubject.create();
    }

    @Provides
    @DaggerScope(RootActivity.class)
    RootPresenter providePresenter(RootModel model, RootViewModel viewModel,
                                   PublishSubject<ActivityResultDto> activityResultSubject,
                                   PublishSubject<PermissionsResultDto> permissionsResultSubject) {

        return new RootPresenterImpl(model, viewModel, activityResultSubject, permissionsResultSubject);
    }
}
