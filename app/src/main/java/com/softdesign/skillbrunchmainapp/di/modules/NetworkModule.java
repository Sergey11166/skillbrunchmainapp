package com.softdesign.skillbrunchmainapp.di.modules;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.softdesign.skillbrunchmainapp.BuildConfig;
import com.softdesign.skillbrunchmainapp.data.network.RestCallTransformer;
import com.softdesign.skillbrunchmainapp.data.network.RestService;
import com.softdesign.skillbrunchmainapp.data.network.adapters.ProductCommentAddedJsonAdapter;
import com.softdesign.skillbrunchmainapp.data.network.adapters.ProductCommentJsonAdapter;
import com.squareup.moshi.Moshi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static com.softdesign.skillbrunchmainapp.utils.Constants.MAX_CONNECT_TIMEOUT;
import static com.softdesign.skillbrunchmainapp.utils.Constants.MAX_READ_TIMEOUT;
import static com.softdesign.skillbrunchmainapp.utils.Constants.MAX_WRITE_TIMEOUT;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;

/**
 * @author Sergey Vorobyev
 */
@Module
public class NetworkModule {

    @Provides
    @Singleton
    OkHttpClient provideHttpClient() {
        return createHttpClient();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient httpClient) {
        return createRetrofit(httpClient);
    }

    @Provides
    @Singleton
    RestService provideRestService(Retrofit retrofit) {
        return retrofit.create(RestService.class);
    }

    @Provides
    @Singleton
    RestCallTransformer provideRestCallTransformer() {
        return new RestCallTransformer();
    }

    private Retrofit createRetrofit(OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.HOST)
                .addConverterFactory(createConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient)
                .build();
    }

    private Converter.Factory createConverterFactory() {
        return MoshiConverterFactory.create(new Moshi.Builder()
                .add(new ProductCommentJsonAdapter())
                .add(new ProductCommentAddedJsonAdapter())
                .build());
    }

    private OkHttpClient createHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(BODY))
                .addInterceptor(new StethoInterceptor())
                .connectTimeout(MAX_CONNECT_TIMEOUT, MILLISECONDS)
                .readTimeout(MAX_READ_TIMEOUT, MILLISECONDS)
                .writeTimeout(MAX_WRITE_TIMEOUT, MILLISECONDS)
                .build();
    }
}
