package com.softdesign.skillbrunchmainapp.di.components;

import android.content.Context;

import com.softdesign.skillbrunchmainapp.di.modules.AppModule;

import dagger.Component;

/**
 * @author Sergey Vorobyev
 */
@Component(modules = AppModule.class)
public interface AppComponent {

    Context getApplication();
}
