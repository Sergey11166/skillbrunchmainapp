package com.softdesign.skillbrunchmainapp.di.modules;

import android.content.Context;

import com.softdesign.skillbrunchmainapp.data.managers.PreferencesManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Sergey Vorobyev
 */
@Module
public class LocalModule extends FlavorLocalModule {

    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context) {
        return new PreferencesManager(context);
    }
}
