package com.softdesign.skillbrunchmainapp.di.modules;

import com.softdesign.skillbrunchmainapp.data.managers.DataManager;
import com.softdesign.skillbrunchmainapp.data.managers.PreferencesManager;
import com.softdesign.skillbrunchmainapp.data.managers.RealmManager;
import com.softdesign.skillbrunchmainapp.data.network.RestCallTransformer;
import com.softdesign.skillbrunchmainapp.data.network.RestService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Sergey Vorobyev
 */
@Module
public class ModelModule extends FlavorModelModule {

    @Provides
    @Singleton
    DataManager provideDataManager(RestCallTransformer transformer, PreferencesManager preferencesManager,
                                   RealmManager realmManager, RestService restService) {
        return new DataManager(restService, realmManager, preferencesManager, transformer);
    }
}
