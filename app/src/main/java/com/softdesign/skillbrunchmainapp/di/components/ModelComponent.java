package com.softdesign.skillbrunchmainapp.di.components;

import com.softdesign.skillbrunchmainapp.data.managers.DataManager;
import com.softdesign.skillbrunchmainapp.di.modules.LocalModule;
import com.softdesign.skillbrunchmainapp.di.modules.ModelModule;
import com.softdesign.skillbrunchmainapp.di.modules.NetworkModule;
import com.softdesign.skillbrunchmainapp.mvp.AbsModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Sergey Vorobyev
 */
@Singleton
@Component(dependencies = AppComponent.class,
        modules = {ModelModule.class, LocalModule.class, NetworkModule.class})
public interface ModelComponent {
    void inject(AbsModel model);

    DataManager getDataManager();
}
