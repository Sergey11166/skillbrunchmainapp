package com.softdesign.skillbrunchmainapp.di;

import android.content.Context;

/**
 * @author Sergey Vorobyev
 */
public class DaggerService {

    public static final String SERVICE_NAME = "DAGGER_SERVICE";

    @SuppressWarnings({"unchecked", "ResourceType"})
    public static <T> T getComponent(Context context) {
        return (T) context.getSystemService(SERVICE_NAME);
    }
}
