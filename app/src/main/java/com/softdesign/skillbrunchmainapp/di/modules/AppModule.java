package com.softdesign.skillbrunchmainapp.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * @author Sergey Vorobyev
 */
@Module
public class AppModule {

    private Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext() {
        return context;
    }
}
