package com.softdesign.skillbrunchmainapp.mortar;

import android.support.annotation.Nullable;

import com.softdesign.skillbrunchmainapp.flow.AbsScreen;
import com.softdesign.skillbrunchmainapp.utils.Constants;
import com.softdesign.skillbrunchmainapp.utils.L;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import mortar.MortarScope;

import static com.softdesign.skillbrunchmainapp.di.DaggerService.SERVICE_NAME;

/**
 * @author Sergey Vorobyev.
 */
public class ScreenScoper {
    private static final String TAG = Constants.LOG_TAG_PREFIX + "ScreenScoper";

    private static Map<String, MortarScope> scopeMap = new HashMap<>();

    public static MortarScope getScreenScope(AbsScreen screen) {
        if (!scopeMap.containsKey(screen.getScopeName())) {
            L.d(TAG, "getScreenScope: create new scope");
            return createScreenScope(screen);
        } else {
            L.d(TAG, "getScreenScope: return has scope");
            return scopeMap.get(screen.getScopeName());
        }
    }

    public static void registerScope(MortarScope scope) {
        scopeMap.put(scope.getName(), scope);
    }

    @SuppressWarnings("all")
    private static void cleanScopeMap() {
        Iterator<Map.Entry<String, MortarScope>> iterator = scopeMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, MortarScope> entry = iterator.next();
            if (entry.getValue().isDestroyed()) {
                iterator.remove();
            }
        }
    }

    public static void destroyScreenScope(String scopeName) {
        MortarScope mortarScope = scopeMap.get(scopeName);
        mortarScope.destroy();
        cleanScopeMap();
    }

    @Nullable
    private static String getParentScopeName(AbsScreen screen) {
        try {
            String genericName = ((Class) ((ParameterizedType) screen.getClass().getGenericSuperclass())
                    .getActualTypeArguments()[0]).getName();

            String parentScopeName = genericName;
            if (parentScopeName.contains("$")) {
                parentScopeName = parentScopeName.substring(0, genericName.indexOf("$"));
            }

            return parentScopeName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private static MortarScope createScreenScope(AbsScreen screen) {
        L.d(TAG, "createScreenScope: with name : " + screen.getScopeName());
        MortarScope parentScope = scopeMap.get(getParentScopeName(screen));
        Object screenComponent = screen.createScreenComponent(parentScope.getService(SERVICE_NAME));
        MortarScope newScope = parentScope.buildChild()
                .withService(SERVICE_NAME, screenComponent)
                .build(screen.getScopeName());
        registerScope(newScope);
        return newScope;
    }
}
