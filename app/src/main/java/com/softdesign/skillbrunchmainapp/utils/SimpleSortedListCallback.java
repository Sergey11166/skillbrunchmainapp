package com.softdesign.skillbrunchmainapp.utils;

import android.support.v7.util.SortedList;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public abstract class SimpleSortedListCallback<T> extends SortedList.Callback<T> {

    @Override
    public void onChanged(int position, int count) {
    }

    @Override
    public boolean areContentsTheSame(T oldItem, T newItem) {
        return oldItem.equals(newItem);
    }

    @Override
    public boolean areItemsTheSame(T item1, T item2) {
        return item1.equals(item2);
    }

    @Override
    public void onInserted(int position, int count) {
    }

    @Override
    public void onRemoved(int position, int count) {
    }

    @Override
    public void onMoved(int fromPosition, int toPosition) {
    }
}
