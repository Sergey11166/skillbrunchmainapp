package com.softdesign.skillbrunchmainapp.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.RemoteMessage.Notification;
import com.softdesign.skillbrunchmainapp.App;
import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static android.content.Context.NOTIFICATION_SERVICE;
import static android.media.RingtoneManager.TYPE_NOTIFICATION;
import static com.softdesign.skillbrunchmainapp.utils.Constants.NOTIFICATION_MANAGER_ID;
import static com.softdesign.skillbrunchmainapp.utils.Constants.NOTIFICATION_OFFER_TYPE;
import static com.softdesign.skillbrunchmainapp.utils.Constants.NOTIFICATION_ORDER_TYPE;
import static com.softdesign.skillbrunchmainapp.utils.Constants.PUSH_REQUEST_CODE;

/**
 * @author Sergey Vorobyev
 */
public class NotificationHelper {

    private NotificationHelper() {
    }

    public static void createNotification(Notification notification,
                                          @Nullable String type, @Nullable String meta) {
        Intent intent = new Intent(App.get(), RootActivity.class);

        String title = notification.getTitle();
        if (title == null || title.isEmpty()) {
            title = App.get().getString(R.string.app_name);
        }
        String content = notification.getBody();

        PendingIntent pendingIntent = PendingIntent.getActivity(App.get(),
                PUSH_REQUEST_CODE, intent, FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(App.get())
                .setContentTitle(title)
                .setContentText(content)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.logo_firebase)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(TYPE_NOTIFICATION))
                .setColor(ContextCompat.getColor(App.get(), R.color.colorAccent));

        if (type != null && !type.isEmpty()) {
            switch (type) {
                case NOTIFICATION_ORDER_TYPE:
                    builder.addAction(R.drawable.ic_close_black_24dp, "Close", pendingIntent)
                            .addAction(R.drawable.ic_visibility_black_24dp, "Status", pendingIntent);
                    break;
                case NOTIFICATION_OFFER_TYPE:
                    builder.addAction(R.drawable.ic_visibility_black_24dp, "Details", pendingIntent);
                    break;
            }
        }

        NotificationManager manager = (NotificationManager) App.get().getSystemService(NOTIFICATION_SERVICE);
        manager.notify(NOTIFICATION_MANAGER_ID, builder.build());
    }
}
