package com.softdesign.skillbrunchmainapp.utils;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import static com.softdesign.skillbrunchmainapp.BuildConfig.DEBUG;
import static com.squareup.picasso.NetworkPolicy.OFFLINE;

/**
 * @author Sergey Vorobyev.
 */
@SuppressWarnings("unused")
public class BindingUtils {

    private BindingUtils() {
    }

    @BindingAdapter("textRes")
    public static void bindTextRes(TextView view, @StringRes int resId) {
        if (resId < 1) {
            return;
        }
        view.setText(resId);
    }

    @BindingAdapter({"hasError", "errorMessage"})
    public static void bindError(TextInputLayout view, Boolean hasError, String errorMessage) {
        view.setErrorEnabled(hasError);
        view.setError(hasError ? errorMessage : null);
    }

    @BindingAdapter({"srcUri", "placeholder"})
    public static void loadImage(ImageView view, String srcUri, Drawable placeholder) {
        if (srcUri == null || srcUri.isEmpty()) {
            srcUri = "null";
        }
        int srcResId = 0;
        if (srcUri.startsWith("drawable")) {
            srcResId = view.getResources().getIdentifier(srcUri.split("/")[1], "drawable",
                    view.getContext().getPackageName());
        }

        if (srcResId != 0) {
            view.setImageResource(srcResId);
            return;
        }

        String finalSrcUri = srcUri;
        Picasso.with(view.getContext())
                .load(srcUri)
                .error(placeholder)
                .placeholder(placeholder)
                .networkPolicy(OFFLINE)
                .into(view, new Callback() {
                    @Override
                    public void onSuccess() {
                        if (DEBUG) {
                            L.d("image loaded from cache");
                        }
                    }

                    @Override
                    public void onError() {
                        Picasso.with(view.getContext())
                                .load(finalSrcUri)
                                .placeholder(placeholder)
                                .into(view, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        if (DEBUG) {
                                            L.d("image loaded from server or resources");
                                        }
                                    }

                                    @Override
                                    public void onError() {
                                        if (DEBUG) {
                                            L.d("Can't load image from server or resources");
                                        }
                                    }
                                });
                    }
                });
    }
}
