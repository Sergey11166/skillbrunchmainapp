package com.softdesign.skillbrunchmainapp.utils;

import android.content.Context;
import android.support.v4.view.LayoutInflaterFactory;
import android.support.v7.app.AppCompatDelegate;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

@SuppressWarnings("unused")
public class InflaterFactory implements LayoutInflaterFactory {
    private AppCompatDelegate appCompatDelegate;

    public InflaterFactory(AppCompatDelegate appCompatDelegate) {
        this.appCompatDelegate = appCompatDelegate;
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attributeSet) {

        if (name.equalsIgnoreCase(ImageView.class.getSimpleName())) {
            return overrideImageViewIfNeed(context, attributeSet);
        } else if (name.equalsIgnoreCase(TextView.class.getSimpleName())) {
            return overrideTextViewIfNeed(context, attributeSet);
        }

        return appCompatDelegate.createView(parent, name, context, attributeSet);
    }

    private View overrideImageViewIfNeed(Context context, AttributeSet attributeSet) {
        boolean isCircular = attributeSet.getAttributeBooleanValue(
                "http://schemas.android.com/apk/res-auto", "circular", false);
        if (isCircular) return new CircleImageView(context, attributeSet);
        return null;
    }

    private View overrideTextViewIfNeed(Context context, AttributeSet attributeSet) {
        TextView textView = new TextView(context, attributeSet);
        TypefaceHelper.applyFontToTextView(context, textView, attributeSet);
        return textView;
    }
}
