package com.softdesign.skillbrunchmainapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

@SuppressWarnings({"unused", "WeakerAccess"})
public class TypefaceHelper {

    private static final String FONT_PT_BEBAS_NEUE_BOOK = "fonts/PTBebasNeueBook.ttf";
    private static final String FONT_PT_BEBAS_NEUE_REGULAR = "fonts/PTBebasNeueRegular.ttf";

    public static void applyFontToTextView(@NonNull Context context, @NonNull TextView textView, @NonNull AttributeSet attrs) {
        int customFont = attrs.getAttributeIntValue("http://schemas.android.com/apk/res-auto", "customFont", -1);
        if (customFont == -1) return;

        Typeface typeface = getTypeface(context, customFont);
        if (typeface != null) {
            textView.setTypeface(typeface);
        }
    }

    @Nullable
    private static Typeface getTypeface(@NonNull Context context, int res) {
        final String fontPath = getTypefacePathByAttr(res);
        if (fontPath == null)
            return null;

        return getTypefaceFromAssets(context, fontPath);
    }

    @Nullable
    private static String getTypefacePathByAttr(int res) {
        switch (res) {
            case 1:
                return FONT_PT_BEBAS_NEUE_BOOK;
            case 2:
                return FONT_PT_BEBAS_NEUE_REGULAR;
            default:
                return null;
        }
    }

    @Nullable
    private static Typeface getTypefaceFromAssets(@NonNull Context context, @Nullable String path) {
        try {
            return Typeface.createFromAsset(context.getAssets(), path);
        } catch (Exception ignored) {
            return null;
        }
    }
}