package com.softdesign.skillbrunchmainapp.utils;

import android.text.TextWatcher;

/**
 * @author Sergey Vorobyev.
 */
@SuppressWarnings("unused")
public abstract class SimpleTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }
}
