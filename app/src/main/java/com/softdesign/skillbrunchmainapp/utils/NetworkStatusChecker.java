package com.softdesign.skillbrunchmainapp.utils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.softdesign.skillbrunchmainapp.App;

import rx.Observable;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class NetworkStatusChecker {

    private NetworkStatusChecker() {
    }

    public static Observable<Boolean> isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) App.get().getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return Observable.just(networkInfo != null && networkInfo.isConnectedOrConnecting());
    }
}
