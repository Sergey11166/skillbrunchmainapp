package com.softdesign.skillbrunchmainapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.FileProvider;

import com.softdesign.skillbrunchmainapp.App;
import com.softdesign.skillbrunchmainapp.BuildConfig;

import java.io.File;
import java.util.List;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.content.Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
import static android.content.pm.PackageManager.MATCH_DEFAULT_ONLY;
import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;
import static android.provider.MediaStore.EXTRA_OUTPUT;

/**
 * @author Sergey Vorobyev.
 */
@SuppressWarnings("unused")
public class NavUtils {

    private NavUtils() {
    }

    /**
     * Open system settings of this app
     */
    public static void goToSettingsApp(Activity activity, int requestCode) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + activity.getPackageName()));
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * Open camera app to take photo
     *
     * @param activity    {@link Activity}
     * @param tempFile    File to write data from camera
     * @param requestCode Code for {@link Activity#onActivityResult(int, int, Intent)}
     */
    public static void goToCameraApp(Activity activity, File tempFile, int requestCode) {
        Intent i = new Intent(ACTION_IMAGE_CAPTURE);
        if (tempFile != null) {
            Uri photoUri = FileProvider.getUriForFile(activity,
                    BuildConfig.APPLICATION_ID + ".file_provider", tempFile);
            i.putExtra(EXTRA_OUTPUT, photoUri);

            List<ResolveInfo> resInfoList = App.get().getPackageManager().queryIntentActivities(i,
                    MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                App.get().grantUriPermission(packageName, photoUri, FLAG_GRANT_WRITE_URI_PERMISSION |
                        FLAG_GRANT_READ_URI_PERMISSION);
            }

            activity.startActivityForResult(i, requestCode);
        }
    }

    /**
     * Open gallery app to choose image
     *
     * @param activity    {@link Activity}
     * @param requestCode Code for {@link Activity#onActivityResult(int, int, Intent)}
     */
    public static void goToGalleryApp(Activity activity, int requestCode) {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        i.setType("image/*");
        activity.startActivityForResult(i, requestCode);
    }

    /**
     * Go to url
     *
     * @param context {@link Context}
     * @param url     Target url
     */
    public static void goToUrl(Context context, String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        try {
            context.startActivity(i);
        } catch (android.content.ActivityNotFoundException ex) {
            UIUtils.showToast("Browser not found");
        }
    }

    /**
     * Send email
     *
     * @param context {@link Context}
     * @param address Target email addresses
     */
    public static void sendEmail(Context context, String address) {
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setData(Uri.parse("mailto:")); // only email apps should handle this
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
        try {
            context.startActivity(i);
        } catch (android.content.ActivityNotFoundException ex) {
            UIUtils.showToast("Email app not found");
        }
    }

    /**
     * Open phone app
     *
     * @param context {@link Context}
     * @param number  Target phone number
     */
    public static void openPhoneApp(Context context, String number) {
        Intent i = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null));
        try {
            context.startActivity(i);
        } catch (android.content.ActivityNotFoundException ex) {
            UIUtils.showToast("Phone app not found");
        }
    }
}
