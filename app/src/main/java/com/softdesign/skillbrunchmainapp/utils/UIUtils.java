package com.softdesign.skillbrunchmainapp.utils;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Toast;

import com.softdesign.skillbrunchmainapp.App;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.WINDOW_SERVICE;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class UIUtils {

    private UIUtils() {
    }

    public static void showToast(Handler handler, String message) {
        if (handler != null) {
            handler.post(() -> showToast(message));
        } else {
            showToast(message);
        }
    }

    public static void showToast(String message) {
        Toast.makeText(App.get(), message, Toast.LENGTH_LONG).show();
    }

    public static void measureView(View view) {
        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(measureSpec, measureSpec);
    }

    public static float getDensity(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService(WINDOW_SERVICE))
                .getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.density;
    }

    public static List<View> getChildrenExcludeView(ViewGroup container, @IdRes int... excludeChild) {
        List<View> result = new ArrayList<>(excludeChild.length);
        for (int i = 0; i < container.getChildCount(); i++) {
            View child = container.getChildAt(i);
            for (int exclude : excludeChild) {
                if (child.getId() != exclude) {
                    result.add(child);
                }
            }
        }
        return result;
    }

    public static void waiteForMeasure(View view, OnMeasureCallback callback) {
        int width = view.getWidth();
        int height = view.getHeight();

        if (width > 0 && height > 0) {
            callback.onMeasure(view, width, height);
            return;
        }

        view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                final ViewTreeObserver observer = view.getViewTreeObserver();
                if (observer.isAlive()) {
                    observer.removeOnPreDrawListener(this);
                }
                callback.onMeasure(view, view.getWidth(), view.getHeight());
                return true;
            }
        });
    }

    public interface OnMeasureCallback {
        void onMeasure(View view, int width, int height);
    }
}
