package com.softdesign.skillbrunchmainapp.mvp;

import android.support.annotation.NonNull;

import com.softdesign.skillbrunchmainapp.ui.view_models.AbsViewModel;

/**
 * @author Sergey Vorobyev.
 */
public interface BaseView {

    void bindViewModel(@NonNull AbsViewModel viewModel);

    boolean onViewBackPressed();
}
