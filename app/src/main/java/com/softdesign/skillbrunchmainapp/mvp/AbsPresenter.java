package com.softdesign.skillbrunchmainapp.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;

import javax.inject.Inject;

import mortar.MortarScope;
import mortar.ViewPresenter;
import rx.subscriptions.CompositeSubscription;

/**
 * @author Sergey Vorobyev.
 */
public abstract class AbsPresenter<V extends AbsView, M> extends ViewPresenter<V> {

    @Inject
    protected M model;

    @Inject
    protected RootPresenter rootPresenter;

    protected CompositeSubscription subscriptions;

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        subscriptions = new CompositeSubscription();
        initDagger(scope);
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        initActionBar();
    }

    @Override
    protected void onExitScope() {
        subscriptions.unsubscribe();
        super.onExitScope();
    }

    protected void initActionBar() {
    }

    @Nullable
    public RootView getRootView() {
        return rootPresenter.getView();
    }

    protected abstract void initDagger(MortarScope scope);
}
