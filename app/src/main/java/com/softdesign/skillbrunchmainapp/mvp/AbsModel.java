package com.softdesign.skillbrunchmainapp.mvp;

import com.birbit.android.jobqueue.JobManager;
import com.softdesign.skillbrunchmainapp.App;
import com.softdesign.skillbrunchmainapp.data.managers.DataManager;

import javax.inject.Inject;

/**
 * @author Sergey Vorobyev
 */
public abstract class AbsModel {

    @Inject
    protected JobManager jobManager;

    @Inject
    protected DataManager dataManager;

    protected AbsModel() {
        App.getModelComponent().inject(this);
    }
}
