package com.softdesign.skillbrunchmainapp.mvp;

import android.support.annotation.Nullable;
import android.view.View;

import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;

/**
 * @author Sergey Vorobyev.
 */
public interface BasePresenter<V extends View> {

    void takeView(V view);

    void dropView(V view);

    @Nullable
    RootView getRootView();
}
