package com.softdesign.skillbrunchmainapp.mvp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;
import com.softdesign.skillbrunchmainapp.ui.view_models.AbsViewModel;

import javax.inject.Inject;

/**
 * @author Sergey Vorobyev
 */
public abstract class AbsView<P extends BasePresenter> extends FrameLayout implements BaseView {

    @Inject
    protected P presenter;

    public AbsView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            initDagger(context);
        }
    }

    @Override
    public void bindViewModel(@NonNull AbsViewModel viewModel) {

    }

    public boolean onViewBackPressed() {
        return false;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            //noinspection unchecked
            presenter.takeView(this);
            setCheckedNavigationItem(getRootMvpView());
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            //noinspection unchecked
            presenter.dropView(this);
        }
    }

    @Nullable
    protected RootView getRootMvpView() {
        return presenter.getRootView();
    }

    protected void setCheckedNavigationItem(RootView rootView) {
    }

    protected abstract void initDagger(Context context);
}
