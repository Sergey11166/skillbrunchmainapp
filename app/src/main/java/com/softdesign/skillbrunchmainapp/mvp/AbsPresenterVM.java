package com.softdesign.skillbrunchmainapp.mvp;

import android.os.Bundle;

import com.softdesign.skillbrunchmainapp.ui.view_models.AbsViewModel;

import javax.inject.Inject;

public abstract class AbsPresenterVM<V extends AbsView, M, VM extends AbsViewModel>
        extends AbsPresenter<V, M> {

    @Inject
    protected VM viewModel;

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        getView().bindViewModel(viewModel);
        super.onLoad(savedInstanceState);
    }
}