package com.softdesign.skillbrunchmainapp.data.network.adapters;

import com.softdesign.skillbrunchmainapp.data.network.requests.ProductCommentRequest;
import com.softdesign.skillbrunchmainapp.data.network.responses.ProductCommentResponse;
import com.squareup.moshi.FromJson;
import com.squareup.moshi.Json;
import com.squareup.moshi.ToJson;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;

import io.realm.internal.android.ISO8601Utils;

import static java.util.Locale.ENGLISH;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class ProductCommentJsonAdapter {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", ENGLISH);

    @FromJson
    ProductCommentResponse fromJson(ProductCommentResJson json) {
        try {
            return new ProductCommentResponse(
                    json.id,
                    json.username,
                    json.avatar,
                    json.rating,
                    ISO8601Utils.parse(json.commentDate, new ParsePosition(0)),
                    json.comment,
                    json.active);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    @ToJson
    ProductCommentResJson toJson(ProductCommentResponse response) {
        ProductCommentResJson json = new ProductCommentResJson();
        json.id = response.getId();
        json.username = response.getUsername();
        json.avatar = response.getAvatar();
        json.comment = response.getAvatar();
        json.commentDate = sdf.format(response.getCommentDate());
        json.rating = response.getRating();
        json.active = response.isActive();
        return json;
    }

    @FromJson
    ProductCommentRequest fromJson(ProductCommentReqJson json) {
        try {
            return new ProductCommentRequest(
                    json.id,
                    json.username,
                    json.avatar,
                    json.rating,
                    json.comment,
                    ISO8601Utils.parse(json.commentDate, new ParsePosition(0)));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    @ToJson
    ProductCommentReqJson toJson(ProductCommentRequest request) {
        ProductCommentReqJson json = new ProductCommentReqJson();
        json.id = request.getId();
        json.username = request.getUsername();
        json.avatar = request.getAvatar();
        json.comment = request.getComment();
        json.commentDate = sdf.format(request.getCommentDate());
        json.rating = request.getRating();
        json.active = true;
        return json;
    }

    private static class ProductCommentResJson {
        @Json(name = "_id")
        String id;
        @Json(name = "userName")
        String username;
        String avatar;
        String commentDate;
        String comment;
        @Json(name = "raiting")
        float rating;
        boolean active;
    }

    private static class ProductCommentReqJson {
        @Json(name = "_id")
        String id;
        @Json(name = "userName")
        String username;
        String avatar;
        String commentDate;
        String comment;
        @Json(name = "raiting")
        float rating;
        boolean active;
    }
}
