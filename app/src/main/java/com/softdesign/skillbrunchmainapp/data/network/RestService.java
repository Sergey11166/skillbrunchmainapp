package com.softdesign.skillbrunchmainapp.data.network;

import com.softdesign.skillbrunchmainapp.data.network.requests.CartRequest;
import com.softdesign.skillbrunchmainapp.data.network.requests.EmailPasswordLoginRequest;
import com.softdesign.skillbrunchmainapp.data.network.requests.ProductCommentRequest;
import com.softdesign.skillbrunchmainapp.data.network.requests.SocialLoginRequest;
import com.softdesign.skillbrunchmainapp.data.network.responses.AvatarUrlResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.CartResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.FbProfileResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.ProductCommentAddedResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.ProductResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.UserResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.VkProfileResponse;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

import static com.softdesign.skillbrunchmainapp.utils.Constants.AUTHORISATION;
import static com.softdesign.skillbrunchmainapp.utils.Constants.IF_MODIFIED_SINCE_HEADER;

/**
 * @author Sergey Vorobyev
 */
public interface RestService {

    @POST("login")
    @Headers("Content-Type: application/json")
    Observable<Response<UserResponse>> login(@Body EmailPasswordLoginRequest request);

    @POST("socialLogin")
    @Headers("Content-Type: application/json")
    Observable<Response<UserResponse>> loginSocial(@Body SocialLoginRequest request);

    @Multipart
    @POST("avatar")
    Observable<AvatarUrlResponse> uploadUserAvatar(@Part MultipartBody.Part file);

    @GET("products")
    Observable<Response<List<ProductResponse>>> getAllProducts(
            @Header(IF_MODIFIED_SINCE_HEADER) String lastEntityUpdate);

    @POST("products/{productId}/comments")
    @Headers("Content-Type: application/json")
    Observable<ProductCommentAddedResponse> sendProductComment(
            @Path("productId") String productId,
            @Body ProductCommentRequest commentRequest);

    @GET("user/{userId}/favorite")
    Observable<Response<List<String>>> getAllFavorites(
            @Path("userId") String userId,
            @Header(AUTHORISATION) String token);

    @POST("user/{userId}/favorite")
    Observable<Response<List<String>>> addToFavorite(
            @Path("userId") String userId,
            @Body String productId,
            @Header(AUTHORISATION) String token);

    @DELETE("user/{userId}/favorite/{productId}")
    Observable<Response<List<String>>> deleteFavorite(
            @Path("userId") String userId,
            @Path("productId") String productId,
            @Header(AUTHORISATION) String token);

    @GET("user/{userId}/cart")
    Observable<Response<List<CartResponse>>> getAllFromCart(
            @Path("userId") String userId,
            @Header(AUTHORISATION) String token);

    @POST("user/{userId}/cart")
    Observable<Response<List<CartResponse>>> addToCart(
            @Path("userId") String userId,
            @Body CartRequest product,
            @Header(AUTHORISATION) String token);

    @PUT("user/{userId}/cart")
    Observable<Response<List<CartResponse>>> updateProductInCart(
            @Path("userId") String userId,
            @Body CartRequest products,
            @Header(AUTHORISATION) String token);

    @DELETE("user/{userId}/cart/{productId}")
    Observable<Response<List<CartResponse>>> deleteProductFromCart(
            @Path("userId") String userId,
            @Path("productId") String product,
            @Header(AUTHORISATION) String token);

    @GET
    Observable<Response<VkProfileResponse>> getVkProfile(
            @Url String baseVkApiUrl,
            @Query("access_token") String token);

    @GET
    Observable<Response<FbProfileResponse>> getFbProfile(
            @Url String fbBaseUrl,
            @Query("access_token") String token);
}