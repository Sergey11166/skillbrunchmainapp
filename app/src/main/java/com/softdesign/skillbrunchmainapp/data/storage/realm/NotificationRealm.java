package com.softdesign.skillbrunchmainapp.data.storage.realm;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class NotificationRealm extends RealmObject {

    @PrimaryKey
    private String id;
    private String type;
    private String meta;
    private String title;
    private String content;
    private Date date;

    public NotificationRealm() {
    }

    public NotificationRealm(String id, String type, String meta, String title, String content, Date date) {
        this.id = id;
        this.type = type;
        this.meta = meta;
        this.title = title;
        this.content = content;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getMeta() {
        return meta;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Date getDate() {
        return date;
    }
}
