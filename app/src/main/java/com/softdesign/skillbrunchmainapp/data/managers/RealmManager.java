package com.softdesign.skillbrunchmainapp.data.managers;

import com.softdesign.skillbrunchmainapp.data.network.responses.ProductCommentResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.ProductResponse;
import com.softdesign.skillbrunchmainapp.data.storage.realm.AddressRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductCommentRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import rx.Observable;

/**
 * @author Sergey Vorobyev
 */
public class RealmManager {

    private Realm realmInstance;

    void saveProductsFromResponse(ProductResponse productResp) {
        ProductRealm productRealm = new ProductRealm(productResp);
        if (!productResp.getComments().isEmpty()) {
            Observable.from(productResp.getComments())
                    .doOnNext(commentResponse -> {
                        if (!commentResponse.isActive())
                            deleteEntity(ProductCommentRealm.class, commentResponse.getId());
                    })
                    .filter(ProductCommentResponse::isActive)
                    .map(ProductCommentRealm::new)
                    .subscribe(commentRealm -> productRealm.getComments().add(commentRealm));
        }
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> realm.insertOrUpdate(productRealm));
        realm.close();
    }

    Observable<ProductRealm> getAllProductsHot() {
        RealmResults<ProductRealm> managedProducts = getQueryRealmInstance()
                .where(ProductRealm.class).findAllAsync();
        return managedProducts.asObservable() // hot observable
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from);
    }

    Observable<ProductRealm> getAllProducts() {
        RealmResults<ProductRealm> managedProducts = getQueryRealmInstance()
                .where(ProductRealm.class).findAllAsync();
        return managedProducts.asObservable() // hot observable
                .filter(RealmResults::isLoaded)
                .first() //convert to cold observable
                .flatMap(Observable::from);
    }

    void saveProductComment(ProductRealm product, ProductCommentRealm comment) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> product.getComments().add(comment));
        realm.close();
    }

    void saveUserInfo(UserInfoRealm userInfoRealm) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> realm.insertOrUpdate(userInfoRealm));
        realm.close();
    }

    void updateUserInfo(UserInfoRealm to, UserInfoRealm from, boolean isAvatarChanged) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> {
            to.setUsername(from.getUsername());
            to.setPhone(from.getPhone());
            to.setPushOffers(from.isPushOffers());
            to.setPushOrderStatus(from.isPushOrderStatus());
            if (isAvatarChanged) {
                to.setAvatarUrl(from.getAvatarUrl());
            }
        });
        realm.close();
    }

    void removeAddress(UserInfoRealm userInfo, AddressRealm address) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> {
            userInfo.getAddresses().remove(address);
            address.deleteFromRealm();
        });
        realm.close();
    }

    void insertAddress(UserInfoRealm userInfo, AddressRealm address) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> userInfo.getAddresses().add(address));
        realm.close();
    }

    void updateAddress(UserInfoRealm userInfo, int addressPosition, AddressRealm address) {
        AddressRealm editAddress = userInfo.getAddresses().get(addressPosition);
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> {
            editAddress.setPlace(address.getPlace());
            editAddress.setFullAddress(address.getFullAddress());
            editAddress.setComment(address.getComment());
        });
        realm.close();
    }

    Observable<UserInfoRealm> getUserInfo() {
        RealmResults<UserInfoRealm> managedUserInfo = getQueryRealmInstance()
                .where(UserInfoRealm.class).findAllAsync();
        return managedUserInfo.asObservable()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from);
    }

    boolean isUserInfoExist() {
        Realm realm = Realm.getDefaultInstance();
        boolean isExist = realm.where(UserInfoRealm.class).count() != 0;
        realm.close();
        return isExist;
    }

    void saveUserAvatar(String avatarUrl) {
        Realm realm = Realm.getDefaultInstance();
        UserInfoRealm userInfo = realm.where(UserInfoRealm.class).findFirst();
        realm.executeTransaction(r -> userInfo.setAvatarUrl(avatarUrl));
        realm.close();
    }

    void incrementProductCount(ProductRealm product) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> product.incrementCount());
        realm.close();
    }

    void decrementProductCount(ProductRealm product) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> product.decrementCount());
        realm.close();
    }

    void changeFavoriteStatus(ProductRealm product) {
        Realm r = Realm.getDefaultInstance();
        r.executeTransaction(realm -> product.setFavorite(!product.isFavorite()));
        r.close();
    }

    Observable<NotificationRealm> getNotifications() {
        RealmResults<NotificationRealm> managedNotifications = getQueryRealmInstance()
                .where(NotificationRealm.class).findAllAsync();

        return managedNotifications.asObservable()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from);
    }

    void saveNotification(NotificationRealm notification) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> realm.insertOrUpdate(notification));
        realm.close();
    }

    void deleteEntity(Class<? extends RealmObject> entityRealmClass, String id) {
        Realm realm = Realm.getDefaultInstance();

        RealmObject entity = realm.where(entityRealmClass)
                .equalTo("id", id)
                .findFirst();

        if (entity != null) {
            realm.executeTransaction(r -> entity.deleteFromRealm());
            realm.close();
        }
    }

    <T extends RealmObject> T getCopyFromRealm(T source) {
        T result;
        Realm realm = Realm.getDefaultInstance();
        result = realm.copyFromRealm(source);
        realm.close();
        return result;
    }

    private Realm getQueryRealmInstance() {
        if (realmInstance == null || realmInstance.isClosed()) {
            realmInstance = Realm.getDefaultInstance();
        }
        return realmInstance;
    }
}
