package com.softdesign.skillbrunchmainapp.data.storage.realm;

import com.softdesign.skillbrunchmainapp.data.network.responses.ProductCommentAddedResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.ProductCommentResponse;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @author Sergey Vorobyev
 */

@SuppressWarnings("unused")
public class ProductCommentRealm extends RealmObject implements Serializable {

    @PrimaryKey
    private String id;
    private String username;
    private String avatar;
    private String comment;
    private Date commentDate;
    private float rating;

    public ProductCommentRealm() {
    }

    public ProductCommentRealm(String comment, float rating) {
        this.id = UUID.randomUUID().toString().replace("-", "").substring(0, 24);
        this.comment = comment;
        this.commentDate = new Date();
        this.rating = rating;
    }

    public ProductCommentRealm(ProductCommentResponse response) {
        this.id = response.getId();
        this.username = response.getUsername();
        this.avatar = response.getAvatar();
        this.comment = response.getComment();
        this.commentDate = response.getCommentDate();
        this.rating = response.getRating();
    }

    public ProductCommentRealm(ProductCommentAddedResponse response) {
        this.id = response.getId();
        this.username = response.getUsername();
        this.avatar = response.getAvatar();
        this.comment = response.getComment();
        this.commentDate = response.getCommentDate();
        this.rating = response.getRating();
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getComment() {
        return comment;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public float getRating() {
        return rating;
    }
}