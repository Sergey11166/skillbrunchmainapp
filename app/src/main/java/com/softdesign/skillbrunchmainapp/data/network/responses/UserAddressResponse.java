package com.softdesign.skillbrunchmainapp.data.network.responses;

import com.squareup.moshi.Json;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class UserAddressResponse {

    @Json(name = "_id")
    private String id;
    private String name;
    private String street;
    private String house;
    private String apartment;
    private String comment;
    private int floor;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getApartment() {
        return apartment;
    }

    public String getComment() {
        return comment;
    }

    public int getFloor() {
        return floor;
    }
}
