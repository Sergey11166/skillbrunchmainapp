package com.softdesign.skillbrunchmainapp.data.storage.realm;

import com.softdesign.skillbrunchmainapp.data.network.responses.ProductResponse;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @author Sergey Vorobyev
 */

@SuppressWarnings("unused")
public class ProductRealm extends RealmObject {

    @PrimaryKey
    private String id;
    private String productName;
    private String imageUrl;
    private String description;
    private int price;
    private int count = 1;
    private float rating;
    private boolean favorite;
    private RealmList<ProductCommentRealm> comments = new RealmList<>();

    public ProductRealm() {
    }

    public ProductRealm(String id, String productName, String imageUrl, String description,
                        int price, int count, float rating, boolean favorite) {
        this.id = id;
        this.productName = productName;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
        this.count = count;
        this.rating = rating;
        this.favorite = favorite;
    }

    public ProductRealm(ProductResponse productResp) {
        id = productResp.getId();
        productName = productResp.getProductName();
        imageUrl = productResp.getImageUrl();
        description = productResp.getDescription();
        price = productResp.getPrice();
        rating = productResp.getRating();
    }

    public void addComment(ProductCommentRealm comment) {
        comments.add(comment);
    }

    public String getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    public float getRating() {
        return rating;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public RealmList<ProductCommentRealm> getComments() {
        return comments;
    }

    public void incrementCount() {
        count++;
    }

    public void decrementCount() {
        count--;
    }
}
