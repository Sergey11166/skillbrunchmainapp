package com.softdesign.skillbrunchmainapp.data.network.requests;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("all")
public class SocialLoginRequest {

    private String uid;
    private String fullName;
    private String phone;
    private String avatarUrl;

    public SocialLoginRequest(String uid, String fullName, String phone, String avatarUrl) {
        this.uid = uid;
        this.fullName = fullName;
        this.phone = phone;
        this.avatarUrl = avatarUrl;
    }
}
