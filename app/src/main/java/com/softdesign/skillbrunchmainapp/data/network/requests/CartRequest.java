package com.softdesign.skillbrunchmainapp.data.network.requests;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.squareup.moshi.Json;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class CartRequest {

    public CartRequest(String productId, int count) {
        this.productId = productId;
        this.count = count;
    }

    @Json(name = "_id")
    private String productId;
    private int count;

    public CartRequest(ProductRealm product) {
        this.productId = product.getId();
        this.count = 1;
    }
}
