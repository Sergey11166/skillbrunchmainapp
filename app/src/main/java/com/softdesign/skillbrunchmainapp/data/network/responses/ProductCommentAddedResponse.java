package com.softdesign.skillbrunchmainapp.data.network.responses;

import com.squareup.moshi.Json;

import java.util.Date;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class ProductCommentAddedResponse {

    @Json(name = "_id")
    private String id;
    private String productId;
    @Json(name = "userName")
    private String username;
    private String avatar;
    @Json(name = "raiting")
    private float rating;
    private Date commentDate;
    private String comment;
    private boolean active;

    public ProductCommentAddedResponse(String id, String productId, String username, String avatar,
                                       float rating, Date commentDate, String comment, boolean active) {
        this.id = id;
        this.productId = productId;
        this.username = username;
        this.avatar = avatar;
        this.rating = rating;
        this.commentDate = commentDate;
        this.comment = comment;
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
