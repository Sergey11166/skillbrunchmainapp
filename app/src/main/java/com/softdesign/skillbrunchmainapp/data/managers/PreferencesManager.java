package com.softdesign.skillbrunchmainapp.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import static com.softdesign.skillbrunchmainapp.utils.Constants.DEFAULT_LAST_MODIFIED;

/**
 * @author Sergey Vorobyev
 */
public class PreferencesManager {

    private static final String PREF_KEY_PRODUCTS_LAST_MODIFIED = "PREF_KEY_PRODUCTS_LAST_MODIFIED";
    private static final String PREF_KEY_AUTH_TOKEN = "PREF_KEY_AUTH_TOKEN";

    private SharedPreferences preferences;

    public PreferencesManager(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    void saveToken(String token) {
        saveString(PREF_KEY_AUTH_TOKEN, token);
    }

    String getToken() {
        return preferences.getString(PREF_KEY_AUTH_TOKEN, "");
    }

    void deleteToken() {
        deleteString(PREF_KEY_AUTH_TOKEN);
    }

    void saveProductsLastModified(String lastModified) {
        saveString(PREF_KEY_PRODUCTS_LAST_MODIFIED, lastModified);
    }

    String getProductsLastModified() {
        return preferences.getString(PREF_KEY_PRODUCTS_LAST_MODIFIED, DEFAULT_LAST_MODIFIED);
    }

    private void saveString(String key, String value) {
        Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private void deleteString(String key) {
        Editor editor = preferences.edit();
        editor.remove(key);
        editor.apply();
    }
}
