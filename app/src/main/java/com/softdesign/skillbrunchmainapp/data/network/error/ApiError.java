package com.softdesign.skillbrunchmainapp.data.network.error;

/**
 * @author Sergey Vorobyev
 */
public class ApiError extends Throwable {

    public ApiError(int statusCode) {
        super(getMessageByStatusCode(statusCode));
    }

    ApiError(String message) {
        super(message);
    }

    private static String getMessageByStatusCode(int statusCode) {
        if (isClientError(statusCode)) {
            return "Client error " + statusCode;
        } else if (isSeverError(statusCode)) {
            return "Server error " + statusCode;
        } else {
            return "Unknown error " + statusCode;
        }
    }

    private static boolean isSeverError(int statusCode) {
        return statusCode >= 500 && statusCode < 600;
    }

    private static boolean isClientError(int statusCode) {
        return statusCode >= 400 && statusCode < 500;
    }
}
