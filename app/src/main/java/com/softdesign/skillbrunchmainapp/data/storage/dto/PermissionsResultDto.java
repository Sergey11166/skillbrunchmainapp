package com.softdesign.skillbrunchmainapp.data.storage.dto;

import android.support.annotation.NonNull;

/**
 * @author Sergey Vorobyev
 */

@SuppressWarnings("unused")
public class PermissionsResultDto {

    @NonNull
    private int[] grantResults;
    private int requestCode;

    @NonNull
    private String[] permissions;

    public PermissionsResultDto(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        this.requestCode = requestCode;
        this.permissions = permissions;
        this.grantResults = grantResults;
    }

    public int getRequestCode() {
        return requestCode;
    }

    @NonNull
    public String[] getPermissions() {
        return permissions;
    }

    @NonNull
    public int[] getGrantResults() {
        return grantResults;
    }
}
