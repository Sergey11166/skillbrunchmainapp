package com.softdesign.skillbrunchmainapp.data.network.responses;

import com.squareup.moshi.Json;

import java.util.Date;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class ProductCommentResponse {

    @Json(name = "_id")
    private String id;
    @Json(name = "userName")
    private String username;
    private String avatar;
    @Json(name = "raiting")
    private float rating;
    private Date commentDate;
    private String comment;
    private boolean active;

    public ProductCommentResponse(String id, String username, String avatar, float rating,
                                  Date commentDate, String comment, boolean active) {
        this.id = id;
        this.username = username;
        this.avatar = avatar;
        this.rating = rating;
        this.commentDate = commentDate;
        this.comment = comment;
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getAvatar() {
        return avatar;
    }

    public float getRating() {
        return rating;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public String getComment() {
        return comment;
    }

    public boolean isActive() {
        return active;
    }
}
