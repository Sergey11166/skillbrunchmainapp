package com.softdesign.skillbrunchmainapp.data.storage.realm;

import com.softdesign.skillbrunchmainapp.data.network.responses.UserAddressResponse;
import com.softdesign.skillbrunchmainapp.ui.view_models.AddressViewModel;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @author Sergey Vorobyev
 */

@SuppressWarnings("unused")
public class AddressRealm extends RealmObject {

    @PrimaryKey
    private String id;
    private String place;
    private String fullAddress;
    private String comment;

    public AddressRealm() {
    }

    public AddressRealm(String place, String fullAddress, String comment) {
        this.id = UUID.randomUUID().toString();
        this.place = place;
        this.fullAddress = fullAddress;
        this.comment = comment;
    }

    public AddressRealm(UserAddressResponse response) {
        id = response.getId();
        place = response.getName();
        fullAddress = extractFullAddress(response);
        comment = response.getComment();
    }

    public AddressRealm(AddressViewModel viewModel) {
        this.place = viewModel.getPlace();
        this.comment = viewModel.getComment();
        this.fullAddress = viewModel.getFullAddress();
    }

    private String extractFullAddress(UserAddressResponse response) {
        StringBuilder addressBuilder = new StringBuilder();
        if (response.getStreet() != null && !response.getStreet().isEmpty()) {
            addressBuilder.append("ул. ").append(response.getStreet()).append(", ");
        }
        if (response.getHouse() != null && !response.getHouse().isEmpty()) {
            addressBuilder.append("д. ").append(response.getHouse()).append(", ");
        }
        if (response.getFloor() != 0) {
            addressBuilder.append("этаж ").append(response.getFloor()).append(", ");
        }
        if (response.getApartment() != null && !response.getApartment().isEmpty()) {
            addressBuilder.append("кв. ").append(response.getApartment());
        }
        return addressBuilder.toString();
    }

    public String getId() {
        return id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
