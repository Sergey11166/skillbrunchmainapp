package com.softdesign.skillbrunchmainapp.data.network.responses;

import com.squareup.moshi.Json;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class CartResponse {

    @Json(name = "_id")
    private String productId;
    private int count;
}
