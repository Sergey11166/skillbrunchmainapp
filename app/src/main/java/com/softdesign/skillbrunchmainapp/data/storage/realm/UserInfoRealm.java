package com.softdesign.skillbrunchmainapp.data.storage.realm;

import com.softdesign.skillbrunchmainapp.data.network.responses.FbProfileResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.UserAddressResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.UserResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.VkProfileResponse;
import com.softdesign.skillbrunchmainapp.ui.view_models.AccountViewModel;
import com.twitter.sdk.android.core.models.User;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * @author Sergey Vorobyev
 */

@SuppressWarnings("unused")
public class UserInfoRealm extends RealmObject {

    private String id;
    private String username;
    private String phone;
    private String avatarUrl;
    private boolean isPushOrderStatus;
    private boolean isPushOffers;
    private RealmList<AddressRealm> addresses = new RealmList<>();

    public UserInfoRealm() {
    }

    public UserInfoRealm(String id, String username, String phone, String avatarUrl,
                         boolean isPushOrderStatus, boolean isPushOffers) {
        this.id = id;
        this.username = username;
        this.phone = phone;
        this.avatarUrl = avatarUrl;
        this.isPushOrderStatus = isPushOrderStatus;
        this.isPushOffers = isPushOffers;
    }

    public UserInfoRealm(UserResponse response) {
        id = response.getId();
        username = response.getUsername();
        phone = response.getPhone();
        avatarUrl = response.getAvatar();
        isPushOrderStatus = true;
        isPushOffers = true;
        if (response.getAddresses() != null) {
            for (UserAddressResponse addressResponse : response.getAddresses()) {
                addresses.add(new AddressRealm(addressResponse));
            }
        }
    }

    public UserInfoRealm(FbProfileResponse response) {
        this.username = response.getFullName();
        this.avatarUrl = response.getAvatarUrl();
        this.isPushOrderStatus = true;
        this.isPushOffers = true;
    }

    public UserInfoRealm(VkProfileResponse response) {
        this.username = response.getFullName();
        this.phone = response.getPhone();
        this.avatarUrl = response.getAvatarUrl();
        this.isPushOrderStatus = true;
        this.isPushOffers = true;
    }

    public UserInfoRealm(User twitterUser) {
        this.username = twitterUser.name;
        this.avatarUrl = twitterUser.profileImageUrl;
        this.isPushOrderStatus = true;
        this.isPushOffers = true;
    }

    public UserInfoRealm(AccountViewModel viewModel) {
        this.phone = viewModel.getPhone();
        this.username = viewModel.getUsername();
        this.avatarUrl = viewModel.getAvatarUrl();
        this.isPushOffers = viewModel.isPushOffers();
        this.isPushOrderStatus = viewModel.isPushOrderStatus();
    }

    public void addAddress(AddressRealm address) {
        addresses.add(address);
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public boolean isPushOrderStatus() {
        return isPushOrderStatus;
    }

    public void setPushOrderStatus(boolean pushOrderStatus) {
        isPushOrderStatus = pushOrderStatus;
    }

    public boolean isPushOffers() {
        return isPushOffers;
    }

    public void setPushOffers(boolean pushOffers) {
        isPushOffers = pushOffers;
    }

    public RealmList<AddressRealm> getAddresses() {
        return addresses;
    }
}
