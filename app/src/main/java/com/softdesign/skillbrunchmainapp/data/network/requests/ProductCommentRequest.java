package com.softdesign.skillbrunchmainapp.data.network.requests;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductCommentRealm;
import com.squareup.moshi.Json;

import java.util.Date;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class ProductCommentRequest {

    @Json(name = "_id")
    private String id;
    @Json(name = "userName")
    private String username;
    private String avatar;
    @Json(name = "raiting")
    private float rating;
    private String comment;
    private Date commentDate;

    public ProductCommentRequest(ProductCommentRealm productCommentRealm) {
        this.id = productCommentRealm.getId();
        this.username = productCommentRealm.getUsername();
        this.avatar = productCommentRealm.getAvatar();
        this.rating = productCommentRealm.getRating();
        String comment = productCommentRealm.getComment();
        this.comment = comment != null ? comment : "";
        this.commentDate = productCommentRealm.getCommentDate();
    }

    public ProductCommentRequest(String id, String username, String avatar, float rating,
                                 String comment, Date commentDate) {
        this.id = id;
        this.username = username;
        this.avatar = avatar;
        this.rating = rating;
        this.commentDate = commentDate;
        this.comment = comment;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getAvatar() {
        return avatar;
    }

    public float getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    public Date getCommentDate() {
        return commentDate;
    }
}
