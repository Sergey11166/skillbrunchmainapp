package com.softdesign.skillbrunchmainapp.data.network;

import android.support.annotation.VisibleForTesting;

import com.softdesign.skillbrunchmainapp.data.managers.DataManager;
import com.softdesign.skillbrunchmainapp.data.network.error.ApiError;
import com.softdesign.skillbrunchmainapp.data.network.error.ForbiddenApiError;
import com.softdesign.skillbrunchmainapp.data.network.error.NetworkAvailableError;
import com.softdesign.skillbrunchmainapp.utils.NetworkStatusChecker;

import retrofit2.Response;
import rx.Observable;

import static android.support.annotation.VisibleForTesting.NONE;
import static com.softdesign.skillbrunchmainapp.utils.Constants.LAST_MODIFIED_HEADER;
import static rx.Observable.error;

/**
 * @author Sergey Vorobyev.
 */
public class RestCallTransformer<R> implements Observable.Transformer<Response<R>, R> {
    private DataManager dataManager;
    private boolean isTestMode;

    @Override
    public Observable<R> call(Observable<Response<R>> responseObs) {
        Observable<Boolean> networkStatus;
        if (isTestMode) {
            networkStatus = Observable.just(true);
        } else {
            networkStatus = NetworkStatusChecker.isNetworkAvailable();
        }
        return networkStatus
                .flatMap(aBoolean -> aBoolean ? responseObs : error(new NetworkAvailableError()))
                .flatMap(rResponse -> {
                    switch (rResponse.code()) {
                        case 200:
                            if (!isTestMode) {
                                if (dataManager != null) {
                                    String lastModified = rResponse.headers().get(LAST_MODIFIED_HEADER);
                                    dataManager.saveProductsLastModified(lastModified);
                                } else {
                                    throw new IllegalStateException("Property dataManager is null. Use method 'setDataManager()'");
                                }
                            }
                            return Observable.just(rResponse.body());
                        case 201:
                        case 202:
                            return Observable.just(rResponse.body());
                        case 304:
                            return Observable.empty();
                        case 403:
                            return Observable.error(new ForbiddenApiError());
                        default:
                            return Observable.error(new ApiError(rResponse.code()));
                    }
                });
    }

    public void setDataManager(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @VisibleForTesting(otherwise = NONE)
    public void setTestMode() {
        isTestMode = true;
    }
}
