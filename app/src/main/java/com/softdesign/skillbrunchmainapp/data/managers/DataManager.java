package com.softdesign.skillbrunchmainapp.data.managers;

import android.support.annotation.VisibleForTesting;

import com.facebook.AccessToken;
import com.facebook.LoginStatusCallback;
import com.facebook.login.LoginManager;
import com.softdesign.skillbrunchmainapp.App;
import com.softdesign.skillbrunchmainapp.data.network.RestCallTransformer;
import com.softdesign.skillbrunchmainapp.data.network.RestService;
import com.softdesign.skillbrunchmainapp.data.network.requests.CartRequest;
import com.softdesign.skillbrunchmainapp.data.network.requests.EmailPasswordLoginRequest;
import com.softdesign.skillbrunchmainapp.data.network.requests.ProductCommentRequest;
import com.softdesign.skillbrunchmainapp.data.network.requests.SocialLoginRequest;
import com.softdesign.skillbrunchmainapp.data.network.responses.AvatarUrlResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.CartResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.FbProfileResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.ProductCommentAddedResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.ProductResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.UserResponse;
import com.softdesign.skillbrunchmainapp.data.network.responses.VkProfileResponse;
import com.softdesign.skillbrunchmainapp.data.storage.realm.AddressRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductCommentRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.utils.L;
import com.softdesign.skillbrunchmainapp.utils.NetworkStatusChecker;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MultipartBody.Part;
import rx.Emitter;
import rx.Observable;

import static android.support.annotation.VisibleForTesting.NONE;
import static com.softdesign.skillbrunchmainapp.utils.Constants.RETRY_REQUEST_BASE_DELAY;
import static com.softdesign.skillbrunchmainapp.utils.Constants.RETRY_REQUEST_COUNT;
import static com.softdesign.skillbrunchmainapp.utils.Constants.UPDATE_DATA_INTERVAL;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * @author Sergey Vorobyev.
 */
public class DataManager {

    private RestCallTransformer restCallTransformer;
    private PreferencesManager preferenceManager;
    private RealmManager realmManager;
    private RestService restService;
    private List<ProductRealm> mockCart = new ArrayList<>();

    public DataManager(RestService restService, RealmManager realmManager,
                       PreferencesManager preferenceManager, RestCallTransformer restCallTransformer) {
        this.restCallTransformer = restCallTransformer;
        this.preferenceManager = preferenceManager;
        this.realmManager = realmManager;
        this.restService = restService;
        this.restCallTransformer.setDataManager(this);
        generateMockUserInfo();
    }

    @VisibleForTesting(otherwise = NONE)
    public void setServer(RestService restService) {
        this.restService = restService;
    }

    public boolean isAuthenticate() {
        return !preferenceManager.getToken().isEmpty();
    }

    public void deleteToken() {
        preferenceManager.deleteToken();
        generateMockUserInfo();
    }

    @SuppressWarnings("unchecked")
    public Observable<UserInfoRealm> login(String email, String password) {
        return restService.login(new EmailPasswordLoginRequest(email, password))
                .compose(((RestCallTransformer<UserResponse>) restCallTransformer))
                .doOnNext(userResponse -> preferenceManager.saveToken(userResponse.getToken()))
                .flatMap(userResponse -> Observable.just(new UserInfoRealm(userResponse)))
                .doOnNext(userInfoRealm -> realmManager.saveUserInfo(userInfoRealm));
    }

    public Observable<UserInfoRealm> getUserInfoFromRealm() {
        return realmManager.getUserInfo();
    }

    public void updateUserInfoInRealm(UserInfoRealm to, UserInfoRealm from, boolean isAvatarChanged) {
        realmManager.updateUserInfo(to, from, isAvatarChanged);
    }

    public void removeAddressFromRealm(UserInfoRealm userInfo, AddressRealm address) {
        realmManager.removeAddress(userInfo, address);
    }

    public void insertAddressToRealm(UserInfoRealm userInfo, AddressRealm address) {
        realmManager.insertAddress(userInfo, address);
    }

    public void updateAddressInRealm(UserInfoRealm userInfo, int addressPosition, AddressRealm address) {
        realmManager.updateAddress(userInfo, addressPosition, address);
    }

    public Observable<AvatarUrlResponse> uploadUserAvatar(Part part) {
        return restService.uploadUserAvatar(part);
    }

    public void saveUserAvatarToRealm(String avatarUrl) {
        realmManager.saveUserAvatar(avatarUrl);
    }

    public void startUpdateProductsWithTimer() {
        L.d("LOCAL UPDATE start " + " " + new Date());
        Observable.interval(UPDATE_DATA_INTERVAL, UPDATE_DATA_INTERVAL, SECONDS)
                .flatMap(aLong -> NetworkStatusChecker.isNetworkAvailable())
                .filter(aBoolean -> aBoolean)
                .flatMap(aBoolean -> getProductsFromServer())
                .subscribe(productRealm -> L.d("LOCAL_UPDATE complete"), L::e);
    }

    @SuppressWarnings("unchecked")
    public Observable<ProductRealm> getProductsFromServer() {
        return restService.getAllProducts(getProductsLastModified())
                .compose(((RestCallTransformer<List<ProductResponse>>) restCallTransformer))
                //.doOnNext(response -> getAllFavoritesFromServer())
                .flatMap(Observable::from)
                .doOnNext(productResponse -> {
                    if (!productResponse.isActive()) {
                        realmManager.deleteEntity(ProductRealm.class, productResponse.getId());
                    } else {
                        realmManager.saveProductsFromResponse(productResponse);
                    }
                })
                //retry request when occur error
                .retryWhen(errorObservable ->
                        errorObservable // generate sequence from 1 to 5
                                .zipWith(Observable.range(1, RETRY_REQUEST_COUNT),
                                        (throwable, retryCount) -> retryCount)
                                .doOnNext(retryCount -> L.d("LOCAL UPDATE request, retryCount = " +
                                        retryCount + " " + new Date()))
                                // calculate exponential delay
                                .map(retryCount -> ((long) (RETRY_REQUEST_BASE_DELAY *
                                        Math.pow(Math.E, retryCount))))
                                .doOnNext(delay -> L.d("LOCAL UPDATE delay = " + delay))
                                // create and return delay in millis
                                .flatMap(delay -> Observable.timer(delay, MILLISECONDS))
                )
                .flatMap(productResponse -> Observable.empty());
    }

    public Observable<ProductCommentAddedResponse> sendProductComment(ProductCommentRequest comment,
                                                                      String productId) {
        return restService.sendProductComment(productId, comment);
    }

    public Observable<ProductRealm> getProductsFromRealm() {
        return realmManager.getAllProductsHot();
    }

    public void saveProductCommentToRealm(ProductRealm product, ProductCommentRealm comment) {
        realmManager.saveProductComment(product, comment);
    }

    public void incrementProductCountInRealm(ProductRealm product) {
        realmManager.incrementProductCount(product);
    }

    public void decrementProductCountInRealm(ProductRealm product) {
        realmManager.decrementProductCount(product);
    }

    public void changeFavoriteStatusInRealm(ProductRealm product) {
        realmManager.changeFavoriteStatus(product);
    }

    public Observable<NotificationRealm> getNotificationsFromRealm() {
        return realmManager.getNotifications();
    }

    public void saveNotificationToRealm(NotificationRealm notification) {
        realmManager.saveNotification(notification);
    }

    @SuppressWarnings("unchecked")
    public Observable<String> addFavoriteOnServer(String productId) {
        return getUserInfoFromRealm().first()
                .flatMap(userInfo -> restService.addToFavorite(userInfo.getId(),
                        productId, preferenceManager.getToken()))
                .compose(((RestCallTransformer<List<String>>) restCallTransformer))
                .flatMap(Observable::from);
    }

    @SuppressWarnings("unchecked")
    public Observable<String> deleteFavoriteOnServer(String productId) {
        return getUserInfoFromRealm().first()
                .flatMap(userInfo -> restService.deleteFavorite(userInfo.getId(), productId,
                        preferenceManager.getToken()))
                .compose(((RestCallTransformer<List<String>>) restCallTransformer))
                .flatMap(Observable::from);
    }

    @SuppressWarnings("unchecked")
    public Observable<String> getAllFavoritesFromServer() {
        return getUserInfoFromRealm().first()
                .flatMap(userInfo -> restService.getAllFavorites(userInfo.getId(),
                        preferenceManager.getToken()))
                .compose(((RestCallTransformer<List<String>>) restCallTransformer))
                .flatMap(Observable::from)
                .doOnNext(favorite -> realmManager.getAllProducts()
                        .filter(product -> favorite.equals(product.getId()))
                        .subscribe(product -> product.setFavorite(true), L::e));
    }

    @SuppressWarnings("unchecked")
    public Observable<CartResponse> addToCartOnServer(CartRequest cartRequest) {
        return getUserInfoFromRealm().first()
                .flatMap(userInfo -> restService.addToCart(userInfo.getId(),
                        cartRequest, preferenceManager.getToken()))
                .compose(((RestCallTransformer<List<CartResponse>>) restCallTransformer))
                .flatMap(Observable::from);
    }

    @SuppressWarnings("unchecked")
    public Observable<CartResponse> updateProductInCartOnServer(CartRequest cartRequest) {
        return getUserInfoFromRealm().first()
                .flatMap(userInfo -> restService.updateProductInCart(userInfo.getId(),
                        cartRequest, preferenceManager.getToken()))
                .compose(((RestCallTransformer<List<CartResponse>>) restCallTransformer))
                .flatMap(Observable::from);
    }

    @SuppressWarnings("unchecked")
    public Observable<CartResponse> deleteProductFromCartOnServer(String productId) {
        return getUserInfoFromRealm().first()
                .flatMap(userInfo -> restService.deleteProductFromCart(userInfo.getId(),
                        productId, preferenceManager.getToken()))
                .compose(((RestCallTransformer<List<CartResponse>>) restCallTransformer))
                .flatMap(Observable::from);
    }

    @SuppressWarnings("unchecked")
    public Observable<CartResponse> getAllFromCartOnServer() {
        return getUserInfoFromRealm().first()
                .flatMap(userInfo -> restService.getAllFromCart(userInfo.getId(),
                        preferenceManager.getToken()))
                .compose(((RestCallTransformer<List<CartResponse>>) restCallTransformer))
                .flatMap(Observable::from);
    }

    private String getProductsLastModified() {
        return preferenceManager.getProductsLastModified();
    }

    public void saveProductsLastModified(String lastModified) {
        preferenceManager.saveProductsLastModified(lastModified);
    }

    public Observable<Boolean> checkVkAuth() {
        return Observable.create(emitter -> VKSdk.wakeUpSession(App.get(),
                new VKCallback<VKSdk.LoginState>() {
                    @Override
                    public void onResult(VKSdk.LoginState res) {
                        switch (res) {
                            case LoggedOut:
                                emitter.onNext(false);
                                emitter.onCompleted();
                                break;
                            case LoggedIn:
                                emitter.onNext(true);
                                emitter.onCompleted();
                                break;
                        }
                    }

                    @Override
                    public void onError(VKError error) {
                        emitter.onError(new Exception(error.errorMessage));
                    }
                }), Emitter.BackpressureMode.BUFFER);
    }

    public Observable<Boolean> checkFbAuth() {
        return Observable.create(emitter ->
                LoginManager.getInstance().retrieveLoginStatus(App.get(), new LoginStatusCallback() {
                    @Override
                    public void onCompleted(AccessToken accessToken) {
                        emitter.onNext(true);
                        emitter.onCompleted();
                    }

                    @Override
                    public void onFailure() {
                        emitter.onNext(false);
                        emitter.onCompleted();
                    }

                    @Override
                    public void onError(Exception e) {
                        emitter.onError(e);
                    }
                }), Emitter.BackpressureMode.BUFFER);
    }

    public Observable<Boolean> checkTwitterAuth() {
        TwitterSession session = Twitter.getSessionManager().getActiveSession();
        return Observable.just(
                session != null && session.getAuthToken() != null && !session.getAuthToken().isExpired());
    }

    @SuppressWarnings("unchecked")
    public Observable<UserInfoRealm> socialLogin(SocialLoginRequest request) {
        return restService.loginSocial(request)
                .compose(((RestCallTransformer<UserResponse>) restCallTransformer))
                .doOnNext(userResponse -> preferenceManager.saveToken(userResponse.getToken()))
                .flatMap(userResponse -> Observable.just(new UserInfoRealm(userResponse)))
                .doOnNext(userInfoRealm -> realmManager.saveUserInfo(userInfoRealm));
    }

    @SuppressWarnings("unchecked")
    public Observable<UserInfoRealm> getProfileVk(String url, String token) {
        return restService.getVkProfile(url, token)
                .compose(((RestCallTransformer<VkProfileResponse>) restCallTransformer))
                .flatMap(response -> Observable.just(new UserInfoRealm(response)));
    }

    @SuppressWarnings("unchecked")
    public Observable<UserInfoRealm> getProfileFb(String url, String token) {
        return restService.getFbProfile(url, token)
                .compose(((RestCallTransformer<FbProfileResponse>) restCallTransformer))
                .flatMap(response -> Observable.just(new UserInfoRealm(response)));
    }

    public Observable<UserInfoRealm> getProfileTwitter() {
        return Observable.create(emitter -> {
            TwitterSession session = Twitter.getSessionManager().getActiveSession();
            Twitter.getApiClient(session).getAccountService().verifyCredentials(true, true)
                    .enqueue(new Callback<User>() {
                        @Override
                        public void success(Result<User> result) {
                            emitter.onNext(new UserInfoRealm(result.data));
                            emitter.onCompleted();
                        }

                        @Override
                        public void failure(TwitterException e) {
                            emitter.onError(e);
                        }
                    });
        }, Emitter.BackpressureMode.BUFFER);
    }

    public void addToCart(ProductRealm product) {
        // TODO: 23.11.2016 save products to cart in DB
        mockCart.add(realmManager.getCopyFromRealm(product));
    }

    public int getCartCount() {
        // TODO: 23.11.2016 get cart count from DB
        int totalCount = 0;
        for (ProductRealm product : mockCart) {
            totalCount += product.getCount();
        }
        return totalCount;
    }

    private void generateMockUserInfo() {
        if (realmManager.isUserInfoExist()) return;

        UserInfoRealm userInfo = new UserInfoRealm("id", "Сергей Воробьёв", "+79022589588",
                "file:///android_asset/user_avatar.jpg", true, true);

        userInfo.addAddress(new AddressRealm("Дом", "Автостроителей 24-43", "6 этаж"));
        userInfo.addAddress(new AddressRealm("Работа", "Автостроителей 24-43", "1 этаж"));

        realmManager.saveUserInfo(userInfo);
    }
}
