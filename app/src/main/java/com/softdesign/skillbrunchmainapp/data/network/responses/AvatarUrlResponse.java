package com.softdesign.skillbrunchmainapp.data.network.responses;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class AvatarUrlResponse {

    private String avatarUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
