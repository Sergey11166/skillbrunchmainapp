package com.softdesign.skillbrunchmainapp.data.network.responses;

import com.squareup.moshi.Json;

import java.util.List;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("all")
public class VkProfileResponse {

    private List<Response> response;

    private static class Response {
        @Json(name = "first_name")
        private String firstName;
        @Json(name = "last_name")
        private String lastName;
        @Json(name = "photo_200")
        private String avatarUrl;
        @Json(name = "mobile_phone")
        private String phone;
    }

    public String getFullName() {
        return response.get(0).firstName + " " + response.get(0).lastName;
    }

    public String getAvatarUrl() {
        return response.get(0).avatarUrl;
    }

    public String getPhone() {
        return response.get(0).phone;
    }
}
