package com.softdesign.skillbrunchmainapp.data.network.error;

/**
 * @author Sergey Vorobyev
 */
public class ForbiddenApiError extends ApiError {
    public ForbiddenApiError() {
        super("Wrong username or password");
    }
}
