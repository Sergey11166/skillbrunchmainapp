package com.softdesign.skillbrunchmainapp.data.network.adapters;

import com.softdesign.skillbrunchmainapp.data.network.responses.ProductCommentAddedResponse;
import com.squareup.moshi.FromJson;
import com.squareup.moshi.Json;
import com.squareup.moshi.ToJson;

import java.text.ParseException;
import java.text.ParsePosition;

import io.realm.internal.android.ISO8601Utils;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class ProductCommentAddedJsonAdapter {

    @FromJson
    ProductCommentAddedResponse fromJson(ProductCommentAddedJson json) {
        try {
            return new ProductCommentAddedResponse(
                    json.id,
                    json.productId,
                    json.username,
                    json.avatar,
                    json.rating,
                    ISO8601Utils.parse(json.commentDate, new ParsePosition(0)),
                    json.comment,
                    json.active);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    @ToJson
    ProductCommentAddedJson toJson(ProductCommentAddedResponse commentResponse) {
        ProductCommentAddedJson json = new ProductCommentAddedJson();
        json.id = commentResponse.getId();
        json.username = commentResponse.getUsername();
        json.avatar = commentResponse.getAvatar();
        json.comment = commentResponse.getAvatar();
        json.commentDate = commentResponse.getCommentDate().toString();
        json.rating = commentResponse.getRating();
        json.active = commentResponse.isActive();
        return json;
    }

    private static class ProductCommentAddedJson {
        @Json(name = "_id")
        String id;
        String productId;
        @Json(name = "userName")
        String username;
        String avatar;
        String commentDate;
        String comment;
        @Json(name = "raiting")
        float rating;
        boolean active;
    }
}
