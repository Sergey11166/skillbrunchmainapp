package com.softdesign.skillbrunchmainapp.data.network.requests;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings({"unused", "FieldCanBeLocal"})
public class EmailPasswordLoginRequest {

    private String login;
    private String password;

    public EmailPasswordLoginRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
