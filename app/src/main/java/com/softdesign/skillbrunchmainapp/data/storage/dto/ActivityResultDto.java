package com.softdesign.skillbrunchmainapp.data.storage.dto;

import android.content.Intent;

/**
 * @author Sergey Vorobyev
 */

@SuppressWarnings("unused")
public class ActivityResultDto {

    private int requestCode;
    private int resultCode;
    private Intent intent;

    public ActivityResultDto(int requestCode, int resultCode, Intent intent) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.intent = intent;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public int getResultCode() {
        return resultCode;
    }

    public Intent getIntent() {
        return intent;
    }
}
