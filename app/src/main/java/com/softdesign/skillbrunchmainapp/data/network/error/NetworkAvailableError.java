package com.softdesign.skillbrunchmainapp.data.network.error;

/**
 * @author Sergey Vorobyev
 */
public class NetworkAvailableError extends Throwable {

    public NetworkAvailableError() {
        super("Internet is not available. Try again later");
    }
}
