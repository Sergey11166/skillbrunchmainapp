package com.softdesign.skillbrunchmainapp.data.network.responses;

import android.support.annotation.VisibleForTesting;

import com.squareup.moshi.Json;

import java.util.List;

import static android.support.annotation.VisibleForTesting.NONE;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class UserResponse {

    @Json(name = "_id")
    private String id;
    @Json(name = "fullName")
    private String username;
    @Json(name = "avatarUrl")
    private String avatar;
    private String token;
    private String phone;
    @Json(name = "address")
    private List<UserAddressResponse> addresses;

    @VisibleForTesting(otherwise = NONE)
    public UserResponse(String id, String username, String avatar, String token, String phone,
                        List<UserAddressResponse> addresses) {
        this.id = id;
        this.username = username;
        this.avatar = avatar;
        this.token = token;
        this.phone = phone;
        this.addresses = addresses;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getToken() {
        return token;
    }

    public String getPhone() {
        return phone;
    }

    public List<UserAddressResponse> getAddresses() {
        return addresses;
    }
}
