package com.softdesign.skillbrunchmainapp.data.network.responses;

import com.squareup.moshi.Json;

import java.util.List;

/**
 * @author Sergey Vorobyev.
 */
@SuppressWarnings("unused")
public class ProductResponse {

    @Json(name = "_id")
    private String id;
    private String productName;
    private String imageUrl;
    private String description;
    private int price;
    @Json(name = "raiting")
    private float rating;
    private boolean active;
    private List<ProductCommentResponse> comments;

    public String getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public float getRating() {
        return rating;
    }

    public boolean isActive() {
        return active;
    }

    public List<ProductCommentResponse> getComments() {
        return comments;
    }
}
