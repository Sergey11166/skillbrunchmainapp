package com.softdesign.skillbrunchmainapp.data.network.responses;

import com.squareup.moshi.Json;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class FbProfileResponse {

    private Picture picture;
    @Json(name = "first_name")
    private String firstName;
    @Json(name = "last_name")
    private String lastName;

    private static class Picture {
        private Data data;

        private static class Data {
            @Json(name = "url")
            private String avatarUrl;
        }
    }

    public String getAvatarUrl() {
        return picture.data.avatarUrl;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }
}
