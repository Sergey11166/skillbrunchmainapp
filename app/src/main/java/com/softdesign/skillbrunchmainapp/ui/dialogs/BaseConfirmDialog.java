package com.softdesign.skillbrunchmainapp.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * @author Sergey Vorobyev.
 */
@SuppressWarnings("unused")
abstract class BaseConfirmDialog extends DialogFragment {

    protected static final String KEY_DIALOG_MASSAGE = "KEY_DIALOG_MASSAGE";

    private OnClickListener onPositiveButtonClickListener;
    private OnClickListener onNegativeButtonClickListener;
    private OnClickListener onNeutralButtonClickListener;
    private int neutralButtonTextResId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int messageResId = getArguments().getInt(KEY_DIALOG_MASSAGE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(android.R.string.dialog_alert_title)
                .setMessage(messageResId)
                .setPositiveButton(android.R.string.yes, onPositiveButtonClickListener)
                .setNegativeButton(android.R.string.no, onNegativeButtonClickListener);
        if (neutralButtonTextResId != 0) {
            builder.setNeutralButton(neutralButtonTextResId, onNeutralButtonClickListener);
        }

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        //https://code.google.com/p/android/issues/detail?id=17423
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    public void setOnPositiveButtonClickListener(OnClickListener onPositiveButtonClickListener) {
        this.onPositiveButtonClickListener = onPositiveButtonClickListener;
    }

    public void setOnNegativeButtonClickListener(OnClickListener onNegativeButtonClickListener) {
        this.onNegativeButtonClickListener = onNegativeButtonClickListener;
    }

    public void setOnNeutralButtonClickListener(OnClickListener onNeutralButtonClickListener) {
        this.onNeutralButtonClickListener = onNeutralButtonClickListener;
    }

    public void setNeutralButtonTextResId(int neutralButtonTextResId) {
        this.neutralButtonTextResId = neutralButtonTextResId;
    }
}
