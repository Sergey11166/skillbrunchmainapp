package com.softdesign.skillbrunchmainapp.ui.screens.product_details;

import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.skillbrunchmainapp.App;
import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.databinding.PageProductDetailsDescriptionBinding;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductViewModel;

/**
 * @author Sergey Vorobyev
 */
class ProductDetailsPagerAdapter extends PagerAdapter {

    private View.OnClickListener clickListener;
    private CommentsRecyclerAdapter commentsAdapter;
    private ProductViewModel viewModel;

    ProductDetailsPagerAdapter(ProductViewModel viewModel, View.OnClickListener clickListener) {
        this.viewModel = viewModel;
        this.clickListener = clickListener;
        commentsAdapter = new CommentsRecyclerAdapter();
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = null;
        switch (position) {
            case 0:
                view = LayoutInflater.from(container.getContext())
                        .inflate(R.layout.page_product_details_description, container, false);
                PageProductDetailsDescriptionBinding binding = DataBindingUtil.bind(view);
                binding.setProductModel(viewModel);
                binding.plusBtn.setOnClickListener(clickListener);
                binding.minusBtn.setOnClickListener(clickListener);
                binding.favoriteFab.setOnClickListener(clickListener);
                break;
            case 1:
                view = LayoutInflater.from(container.getContext())
                        .inflate(R.layout.page_product_details_reviews, container, false);
                RecyclerView recycler = (RecyclerView) view.findViewById(R.id.recycler);
                recycler.setAdapter(commentsAdapter);
                view.findViewById(R.id.add_fab).setOnClickListener(clickListener);
                break;
        }
        if (view == null) return null;
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return App.get().getString(R.string.product_details_tab_describes);
            case 1:
                return App.get().getString(R.string.product_details_tab_reviews);
            default:
                return "";
        }
    }

    CommentsRecyclerAdapter getCommentsAdapter() {
        return commentsAdapter;
    }
}
