package com.softdesign.skillbrunchmainapp.ui.screens.notifications;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsView;
import com.softdesign.skillbrunchmainapp.ui.screens.notifications.NotificationRecyclerAdapter.OnItemClickListener;
import com.softdesign.skillbrunchmainapp.ui.screens.notifications.NotificationsScreen.Component;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;

/**
 * @author Sergey Vorobyev
 */
public class NotificationsViewImpl extends AbsView<NotificationsPresenter> implements NotificationsView {

    private NotificationRecyclerAdapter adapter;

    public NotificationsViewImpl(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            initRecyclerView();
        }
    }

    @Override
    protected void setCheckedNavigationItem(RootView rootView) {
        if (rootView != null) {
            rootView.setCheckedNavigationItem(R.id.nav_notifications);
        }
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<Component>getComponent(context).inject(this);
    }

    private void initRecyclerView() {
        OnItemClickListener onItemClickListener = (position, view) -> {
            switch (view.getId()) {
                case R.id.cancel_btn:
                    presenter.clickOnCancelButton(position);
                    break;
                case R.id.action_btn:
                    presenter.clickOnActionButton(position);
                    break;
            }
        };

        adapter = new NotificationRecyclerAdapter(onItemClickListener);
        ((RecyclerView) findViewById(R.id.recycler)).setAdapter(adapter);
    }

    @Override
    public void addNotification(NotificationRealm notification) {
        adapter.addItem(notification);
    }
}
