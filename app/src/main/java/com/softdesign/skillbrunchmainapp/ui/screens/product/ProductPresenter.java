package com.softdesign.skillbrunchmainapp.ui.screens.product;

import android.view.View;

import com.softdesign.skillbrunchmainapp.mvp.BasePresenter;

/**
 * @author Sergey Vorobyev.
 */
interface ProductPresenter<V extends View> extends BasePresenter<V> {

    void onPlusButtonClick();

    void onMinusButtonClick();

    void onFavoriteClick();

    void onDetailsClick();
}
