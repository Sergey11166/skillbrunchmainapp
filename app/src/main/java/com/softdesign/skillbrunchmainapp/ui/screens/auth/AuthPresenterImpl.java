package com.softdesign.skillbrunchmainapp.ui.screens.auth;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ActivityResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsPresenterVM;
import com.softdesign.skillbrunchmainapp.ui.view_models.AuthViewModel;
import com.softdesign.skillbrunchmainapp.utils.L;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import flow.Flow;
import mortar.MortarScope;
import rx.subjects.PublishSubject;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.softdesign.skillbrunchmainapp.ui.screens.auth.SocialSdkType.FB;
import static com.softdesign.skillbrunchmainapp.ui.screens.auth.SocialSdkType.TWITTER;
import static com.softdesign.skillbrunchmainapp.ui.screens.auth.SocialSdkType.VK;
import static com.softdesign.skillbrunchmainapp.utils.Constants.PATTERN_EMAIL;

/**
 * @author Sergey Vorobyev
 */
class AuthPresenterImpl extends AbsPresenterVM<AuthViewImpl, AuthModel, AuthViewModel>
        implements AuthPresenter<AuthViewImpl> {

    private PublishSubject<ActivityResultDto> activityResultSubject;

    private CallbackManager fbCallbackManager;
    private TwitterAuthClient twitterAuthClient;

    AuthPresenterImpl(PublishSubject<ActivityResultDto> activityResultSubject,
                      CallbackManager fbCallbackManager, TwitterAuthClient twitterAuthClient) {
        this.activityResultSubject = activityResultSubject;
        this.fbCallbackManager = fbCallbackManager;
        this.twitterAuthClient = twitterAuthClient;
    }

    @Override
    public void onEmailChanged() {
        String email = viewModel.getEmail();
        String password = viewModel.getPassword();
        if (isValidEmail(email)) {
            viewModel.setEmailValid(true);
            if (isValidPassword(password) && viewModel.getVisibilityProgress() == GONE &&
                    viewModel.getVisibilityInputs() == VISIBLE) {
                viewModel.setLoginButtonEnabled(true);
            }
        } else {
            viewModel.setEmailValid(false);
            if (viewModel.getVisibilityInputs() == VISIBLE) {
                viewModel.setLoginButtonEnabled(false);
            }
        }
    }

    @Override
    public void onPasswordChanged() {
        String email = viewModel.getEmail();
        String password = viewModel.getPassword();
        if (isValidPassword(password)) {
            viewModel.setPasswordValid(true);
            if (isValidEmail(email) && viewModel.getVisibilityProgress() == GONE &&
                    viewModel.getVisibilityInputs() == VISIBLE) {
                viewModel.setLoginButtonEnabled(true);
            }
        } else {
            viewModel.setPasswordValid(false);
            if (viewModel.getVisibilityInputs() == VISIBLE) {
                viewModel.setLoginButtonEnabled(false);
            }
        }
    }

    @Override
    public void onLoginButtonClick() {
        if (model.isAuthenticated()) {
            model.logout();
            viewModel.setLoginButtonText(R.string.auth_button_log_in);
        } else {
            if (viewModel.getVisibilityInputs() == GONE) {
                viewModel.setVisibilityInputs(VISIBLE);
                viewModel.setVisibilityCatalogButton(GONE);
                viewModel.setVisibilitySocialButtons(VISIBLE);
                getView().slideAnimateHideShowInputs(true);
                getView().animateHideShowSocialButtons(true);
                getView().animateHideShowCatalogButton(false);
                onEmailChanged();
                onPasswordChanged();
            } else {
                setupUIStateLoading();
                model.login(viewModel.getEmail(), viewModel.getPassword())
                        .subscribe(this::onLoginSuccess, this::onLoginError);
            }
        }
    }

    @Override
    public void onShowCatalogButtonClick() {
        Flow.get(getView()).goBack();
    }

    @Override
    public void onFbButtonClick() {
        setupUIStateLoading();
        model.checkFbAuth().subscribe(aBoolean -> {
            if (aBoolean) {
                requestFbUser();
            } else {
                rootPresenter.startLoginFb();
            }
        }, this::onLoginError);
    }

    @Override
    public void onVkButtonClick() {
        setupUIStateLoading();
        model.checkVkAuth().subscribe(aBoolean -> {
            if (aBoolean) {
                requestVkUser();
            } else {
                rootPresenter.startLoginVk();
            }
        }, this::onLoginError);
    }

    @Override
    public void onTwitterButtonClick() {
        setupUIStateLoading();
        model.checkTwitterAuth().subscribe(aBoolean -> {
            if (aBoolean) {
                requestTwitterUser();
            } else {
                rootPresenter.startLoginTwitter(twitterAuthClient);
            }
        }, this::onLoginError);
    }

    @Override
    public boolean onBackPressed() {
        if (viewModel.getVisibilityInputs() == VISIBLE) {
            viewModel.setVisibilityCatalogButton(VISIBLE);
            viewModel.setVisibilityInputs(GONE);
            viewModel.setLoginButtonEnabled(true);
            viewModel.setVisibilitySocialButtons(INVISIBLE);
            getView().slideAnimateHideShowInputs(false);
            getView().animateHideShowSocialButtons(false);
            return true;
        }
        return false;
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        if (FacebookSdk.isInitialized()) { // for unit tests
            LoginManager.getInstance().registerCallback(fbCallbackManager, null);
        }
        subscriptions.add(activityResultSubject
                .subscribe(this::handleActivityResult, this::onLoginError));
        viewModel.setVisibilityInputs(GONE);
        viewModel.setVisibilityCatalogButton(VISIBLE);
        viewModel.setVisibilitySocialButtons(INVISIBLE);
        viewModel.setVisibilityProgress(GONE);
        viewModel.setLoginButtonEnabled(true);
        if (model.isAuthenticated()) {
            viewModel.setLoginButtonText(R.string.auth_button_log_out);
        } else {
            viewModel.setLoginButtonText(R.string.auth_button_log_in);
        }
    }

    @Override
    protected void initActionBar() {
        rootPresenter.newActionBarBuilder()
                .setBackArrow(true)
                .setVisibility(false)
                .build();
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((AuthScreen.Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    private void onLoginSuccess(UserInfoRealm userInfo) {
        L.d(userInfo.getUsername());
        viewModel.setVisibilityProgress(GONE);
        viewModel.setVisibilityCatalogButton(VISIBLE);
        viewModel.setLoginButtonEnabled(true);
        viewModel.setLoginButtonText(R.string.auth_button_log_out);
        if (getView() != null) {
            getView().animateHideShowCatalogButton(true);
            getView().showMessageLoggedIn();
            getView().animateHideShowProgress(false);
        }
    }

    private void onLoginError(Throwable throwable) {
        L.e(throwable);
        viewModel.setVisibilityProgress(GONE);
        viewModel.setVisibilityInputs(VISIBLE);
        viewModel.setLoginButtonEnabled(true);
        viewModel.setVisibilitySocialButtons(VISIBLE);
        if (getView() != null) {
            getView().animateHideShowSocialButtons(true);
            getView().showMessageLoginError();
            getView().animateHideShowProgress(false);
            getView().alphaAnimateHideShowInputs(true);
        }
    }

    private void setupUIStateLoading() {
        viewModel.setVisibilityProgress(VISIBLE);
        viewModel.setVisibilityInputs(GONE);
        viewModel.setVisibilitySocialButtons(INVISIBLE);
        viewModel.setLoginButtonEnabled(false);
        getView().alphaAnimateHideShowInputs(false);
        getView().animateHideShowProgress(true);
        getView().animateHideShowSocialButtons(false);
    }

    private boolean isValidEmail(String email) {
        return email != null && email.matches(PATTERN_EMAIL);
    }

    private boolean isValidPassword(String password) {
        return password != null && password.length() > 7;
    }

    private void handleActivityResult(ActivityResultDto activityResult) {
        // int vkCode = VKServiceActivity.VKServiceType.Authorization.getOuterCode(); //10485
        // int fbCode = CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode();  //64206

        final int vkCode = 10485;
        final int fbCode = 64206;
        final int twitterCode = 140;

        switch (activityResult.getRequestCode()) {
            case vkCode:
                if (activityResult.getResultCode() == RESULT_OK) {
                    requestVkUser();
                } else {
                    onLoginError(new Exception("user disallow"));
                }
                break;
            case fbCode:
                if (activityResult.getResultCode() == RESULT_OK) {
                    fbCallbackManager.onActivityResult(activityResult.getRequestCode(),
                            activityResult.getResultCode(), activityResult.getIntent());
                    requestFbUser();
                } else {
                    onLoginError(new Exception("user disallow"));
                }
                break;
            case twitterCode:
                if (activityResult.getResultCode() == RESULT_OK) {
                    twitterAuthClient.onActivityResult(activityResult.getRequestCode(),
                            activityResult.getResultCode(), activityResult.getIntent());
                    requestTwitterUser();
                } else {
                    onLoginError(new Exception("user disallow"));
                }
                break;
        }
    }

    private void requestFbUser() {
        model.getFbUser().subscribe(userInfo -> model.loginSocial(userInfo, FB)
                .subscribe(this::onLoginSuccess, this::onLoginError));
    }

    private void requestVkUser() {
        model.getVkUser().subscribe(userInfo -> model.loginSocial(userInfo, VK)
                .subscribe(this::onLoginSuccess, this::onLoginError));
    }

    private void requestTwitterUser() {
        model.getTwitterUser().subscribe(userInfo -> model.loginSocial(userInfo, TWITTER)
                .subscribe(this::onLoginSuccess, this::onLoginError));
    }
}
