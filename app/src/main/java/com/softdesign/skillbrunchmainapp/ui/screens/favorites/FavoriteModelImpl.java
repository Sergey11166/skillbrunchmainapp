package com.softdesign.skillbrunchmainapp.ui.screens.favorites;

import com.softdesign.skillbrunchmainapp.data.network.requests.CartRequest;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.mvp.AbsModel;

import rx.Observable;

/**
 * @author Sergey Vorobyev.
 */

public class FavoriteModelImpl extends AbsModel implements FavoriteModel {

    @Override
    public Observable<ProductRealm> getFavoriteProducts() {
        return dataManager.getProductsFromRealm().filter(ProductRealm::isFavorite);
    }

    @Override
    public void changeFavoriteStatus(ProductRealm product) {
        if (product.isFavorite()) {
            dataManager.addFavoriteOnServer(product.getId());
        } else {
            dataManager.deleteFavoriteOnServer(product.getId());
        }
        dataManager.changeFavoriteStatusInRealm(product);
    }

    @Override
    public void addToCart(ProductRealm product) {
        dataManager.addToCartOnServer(new CartRequest(product));
    }

    @Override
    public boolean isAuthenticated() {
        return dataManager.isAuthenticate();
    }
}
