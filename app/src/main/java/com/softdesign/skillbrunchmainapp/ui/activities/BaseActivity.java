package com.softdesign.skillbrunchmainapp.ui.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.utils.InflaterFactory;

/**
 * @author Sergey Vorobyev.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private static final String IS_PROGRESS_SHOWING_KEY = "IS_PROGRESS_SHOWING_KEY";

    private ProgressDialog progressDialog;
    private boolean isProgressShowing;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory(getLayoutInflater(), new InflaterFactory(getDelegate()));
        super.onCreate(savedInstanceState);
        initProgressDialog();

        if (savedInstanceState != null) {
            isProgressShowing = savedInstanceState.getBoolean(IS_PROGRESS_SHOWING_KEY);
        }
        if (isProgressShowing) showProgress();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(IS_PROGRESS_SHOWING_KEY, isProgressShowing);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null) progressDialog.dismiss();
        super.onDestroy();
    }

    @Nullable
    public View getRootView() {
        ViewGroup rootGroup = (ViewGroup) this.findViewById(android.R.id.content);
        if (rootGroup == null) return null;
        return rootGroup.getChildAt(0);
    }

    public void showSnackBar(@Nullable String text) {
        if (text == null || text.isEmpty()) return;
        View rootView = getRootView();
        if (rootView == null) return;
        Spanned html = Html.fromHtml("<font color=\"#ffffff\">" + text + "</font>");
        Snackbar.make(rootView, html, Snackbar.LENGTH_SHORT).show();
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this, R.style.CustomProgress);
        progressDialog.setCancelable(false);
        Window window = progressDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    public void showProgress() {
        progressDialog.show();
        progressDialog.setContentView(R.layout.progress_splash);
        isProgressShowing = true;
    }

    public void hideProgress() {
        if (progressDialog.isShowing()) progressDialog.hide();
        isProgressShowing = false;
    }
}
