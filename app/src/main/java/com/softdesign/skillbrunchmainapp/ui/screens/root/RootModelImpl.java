package com.softdesign.skillbrunchmainapp.ui.screens.root;

import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.mvp.AbsModel;

import rx.Observable;

/**
 * @author Sergey Vorobyev.
 */
public class RootModelImpl extends AbsModel implements RootModel {

    @Override
    public Observable<UserInfoRealm> getUserInfo() {
        return dataManager.getUserInfoFromRealm();
    }

    @Override
    public void saveNotification(NotificationRealm notification) {
        dataManager.saveNotificationToRealm(notification);
    }
}
