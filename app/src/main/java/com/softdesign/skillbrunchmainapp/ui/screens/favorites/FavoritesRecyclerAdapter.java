package com.softdesign.skillbrunchmainapp.ui.screens.favorites;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.databinding.ItemFavoriteBinding;
import com.softdesign.skillbrunchmainapp.ui.view_models.FavoriteViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Vorobyev.
 */

class FavoritesRecyclerAdapter extends RecyclerView.Adapter<FavoritesRecyclerAdapter.ViewHolder> {

    private List<ProductRealm> data = new ArrayList<>();
    private OnItemClickListener itemClickListener;

    public FavoritesRecyclerAdapter(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemFavoriteBinding binding = ItemFavoriteBinding.inflate(inflater, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProductRealm product = data.get(position);
        FavoriteViewModel viewModel = new FavoriteViewModel();

        // TODO: 28.05.2017 init viewModel by product

        holder.binding.setFavoriteModel(viewModel);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addItem(ProductRealm product) {
        data.add(product);
        notifyDataSetChanged();
    }

    public List<ProductRealm> getData() {
        return data;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemFavoriteBinding binding;

        private ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position, View view);
    }
}
