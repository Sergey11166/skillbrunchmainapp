package com.softdesign.skillbrunchmainapp.ui.screens.product_comment;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductCommentRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsPresenterVM;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogModel;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductCommentViewModel;

import flow.Flow;
import mortar.MortarScope;

/**
 * @author Sergey Vorobyev
 */
class ProductCommentPresenterImpl
        extends AbsPresenterVM<ProductCommentViewImpl, CatalogModel, ProductCommentViewModel>
        implements ProductCommentPresenter<ProductCommentViewImpl> {

    private ProductRealm product;

    ProductCommentPresenterImpl(ProductRealm productRealm) {
        this.product = productRealm;
    }

    @Override
    public void onSaveButtonClick() {
        addComment();
        Flow.get(getView()).goBack();
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((ProductCommentScreen.Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        viewModel.setRating(4);
    }

    @Override
    protected void initActionBar() {
        rootPresenter.newActionBarBuilder()
                .setTitle(product.getProductName())
                .setBackArrow(true)
                .build();
    }

    private void addComment() {
        ProductCommentRealm comment = new ProductCommentRealm(
                viewModel.getComment(),
                viewModel.getRating());
        model.addProductComment(product, comment);
    }
}
