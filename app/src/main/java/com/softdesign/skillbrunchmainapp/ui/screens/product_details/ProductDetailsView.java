package com.softdesign.skillbrunchmainapp.ui.screens.product_details;

import android.support.v4.view.ViewPager;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductCommentRealm;
import com.softdesign.skillbrunchmainapp.mvp.BaseView;

import java.util.List;

/**
 * @author Sergey Vorobyev
 */
interface ProductDetailsView extends BaseView {

    void showComments(List<ProductCommentRealm> comments);

    ViewPager getViewPager();
}
