package com.softdesign.skillbrunchmainapp.ui.screens.root;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.io.File;

/**
 * @author Sergey Vorobyev.
 */
public interface RootPresenter {

    @Nullable
    RootView getView();

    void takeView(@NonNull RootView rootView);

    void dropView();

    void setCartCount(int count);

    void startCameraForResult(File file, int requestCode);

    void startGalleryForResult(int requestCode);

    void startSettingForResult(int requestCode);

    boolean checkAndRequestPermission(@NonNull String[] permissions, int requestCode);

    void onActivityResult(int requestCode, int resultCode, Intent data);

    void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                    @NonNull int[] grantResults);

    void onStartFromNotification(NotificationRealm notification);

    void startLoginVk();

    void startLoginFb();

    void startLoginTwitter(TwitterAuthClient client);

    ActionBarBuilder newActionBarBuilder();
}
