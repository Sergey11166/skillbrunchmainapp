package com.softdesign.skillbrunchmainapp.ui.screens.notifications;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;
import com.softdesign.skillbrunchmainapp.databinding.ItemNotificationBinding;
import com.softdesign.skillbrunchmainapp.ui.view_models.NotificationViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.softdesign.skillbrunchmainapp.R.string.notification_button_action_details;
import static com.softdesign.skillbrunchmainapp.R.string.notification_button_action_status;
import static com.softdesign.skillbrunchmainapp.R.string.notification_title;
import static com.softdesign.skillbrunchmainapp.utils.Constants.NOTIFICATION_ORDER_TYPE;
import static java.util.Locale.ENGLISH;

/**
 * @author Sergey Vorobyev
 */
class NotificationRecyclerAdapter extends RecyclerView.Adapter<NotificationRecyclerAdapter.ViewHolder> {

    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy", ENGLISH);
    private List<NotificationRealm> data = new ArrayList<>();
    private OnItemClickListener itemClickListener;
    private Context context;

    NotificationRecyclerAdapter(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemNotificationBinding binding = ItemNotificationBinding.inflate(inflater, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NotificationRealm notification = data.get(position);
        NotificationViewModel viewModel = new NotificationViewModel();

        if (TextUtils.equals(notification.getType(), NOTIFICATION_ORDER_TYPE)) {
            viewModel.setTitle(context.getString(notification_title) + " " + notification.getMeta());
            viewModel.setIcon("drawable/shape_notification_status");
            viewModel.setActionButtonText(context.getString(notification_button_action_status));
            holder.binding.cancelBtn.setVisibility(VISIBLE);
        } else {
            viewModel.setTitle(notification.getTitle());
            viewModel.setIcon("drawable/shape_notification_offer");
            viewModel.setActionButtonText(context.getString(notification_button_action_details));
            holder.binding.cancelBtn.setVisibility(GONE);
        }

        viewModel.setDate(sdf.format(notification.getDate()));
        viewModel.setContent(notification.getContent());

        holder.binding.setNotificationModel(viewModel);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    void addItem(NotificationRealm item) {
        this.data.add(item);
        notifyDataSetChanged();
    }

    List<NotificationRealm> getData() {
        return data;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ItemNotificationBinding binding;

        private ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position, View view);
    }
}
