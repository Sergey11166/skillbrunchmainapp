package com.softdesign.skillbrunchmainapp.ui.screens.account;

import android.view.View;

import com.softdesign.skillbrunchmainapp.data.storage.realm.AddressRealm;
import com.softdesign.skillbrunchmainapp.mvp.BasePresenter;

/**
 * @author Sergey Vorobyev.
 */
interface AccountPresenter<V extends View> extends BasePresenter<V> {

    void onAddressButtonClick();

    void onFabClick();

    void onAvatarClick();

    void onChoiceFromGallery();

    void onChoiceTakePhoto();

    void onSwipeAddressRemove(AddressRealm address);

    void onSwipeAddressEdit(AddressRealm address);

    void onYesButtonClickInCameraPermissionDialog();

    void onSettingButtonClickInCameraPermissionDialog();

    void onYesButtonClickInGalleryPermissionDialog();

    void onSettingButtonClickInGalleryPermissionDialog();

    boolean onBackPressed();
}
