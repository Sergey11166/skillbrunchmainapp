package com.softdesign.skillbrunchmainapp.ui.screens.favorites;

import android.view.View;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.mvp.BasePresenter;

/**
 * @author Sergey Vorobyev.
 */
public interface FavoritesPresenter<V extends View> extends BasePresenter<V> {

    void onCartClick(ProductRealm product);

    void onFavoriteClick(ProductRealm product);
}
