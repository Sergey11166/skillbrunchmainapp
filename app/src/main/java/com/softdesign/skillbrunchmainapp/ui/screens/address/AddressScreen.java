package com.softdesign.skillbrunchmainapp.ui.screens.address;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.di.scopes.DaggerScope;
import com.softdesign.skillbrunchmainapp.flow.AbsScreen;
import com.softdesign.skillbrunchmainapp.flow.Screen;
import com.softdesign.skillbrunchmainapp.ui.screens.account.AccountScreen;
import com.softdesign.skillbrunchmainapp.ui.view_models.AddressViewModel;

import dagger.Provides;
import flow.TreeKey;

/**
 * @author Sergey Vorobyev.
 */
@Screen(R.layout.screen_address)
public class AddressScreen extends AbsScreen<AccountScreen.Component> implements TreeKey {

    private UserInfoRealm userInfo;
    private int editPosition = -1;

    public AddressScreen(UserInfoRealm userInfo) {
        this.userInfo = userInfo;
    }

    public AddressScreen(UserInfoRealm userInfo, int editPosition) {
        this(userInfo);
        this.editPosition = editPosition;
    }

    @Override
    public Object createScreenComponent(AccountScreen.Component parentComponent) {
        return DaggerAddressScreen_Component.builder()
                .component(parentComponent)
                .module(new Module(userInfo, editPosition))
                .build();
    }

    @Override
    public Object getParentKey() {
        return new AccountScreen();
    }

    @DaggerScope(AddressScreen.class)
    @dagger.Component(dependencies = AccountScreen.Component.class, modules = Module.class)
    public interface Component {
        void inject(AddressPresenterImpl presenter);

        void inject(AddressViewImpl view);
    }

    @dagger.Module
    public static class Module {

        private UserInfoRealm userInfo;
        private int editPosition;

        public Module(UserInfoRealm userInfo, int editPosition) {
            this.userInfo = userInfo;
            this.editPosition = editPosition;
        }

        @Provides
        @DaggerScope(AddressScreen.class)
        AddressPresenter providePresenter() {
            return new AddressPresenterImpl(userInfo, editPosition);
        }

        @Provides
        @DaggerScope(AddressScreen.class)
        AddressViewModel provideViewModel() {
            return new AddressViewModel();
        }
    }
}
