package com.softdesign.skillbrunchmainapp.ui.screens.account;

import com.softdesign.skillbrunchmainapp.data.storage.realm.AddressRealm;
import com.softdesign.skillbrunchmainapp.mvp.BaseView;

import java.util.List;

/**
 * @author Sergey Vorobyev.
 */
interface AccountView extends BaseView {

    void showAddresses(List<AddressRealm> addresses);

    void showChangePhotoDialog();

    void showCameraNeedGrandPermissionsDialog();

    void showGalleryNeedGrandPermissionsDialog();

    void showFullScreenAvatar();
}
