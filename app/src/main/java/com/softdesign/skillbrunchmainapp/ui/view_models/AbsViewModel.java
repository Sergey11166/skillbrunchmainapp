package com.softdesign.skillbrunchmainapp.ui.view_models;

import android.databinding.BaseObservable;

/**
 * @author Sergey Vorobyev.
 */
public abstract class AbsViewModel extends BaseObservable {
}
