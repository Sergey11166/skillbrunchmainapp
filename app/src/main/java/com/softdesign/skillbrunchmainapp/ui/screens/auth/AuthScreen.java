package com.softdesign.skillbrunchmainapp.ui.screens.auth;

import com.facebook.CallbackManager;
import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ActivityResultDto;
import com.softdesign.skillbrunchmainapp.di.scopes.DaggerScope;
import com.softdesign.skillbrunchmainapp.flow.AbsScreen;
import com.softdesign.skillbrunchmainapp.flow.Screen;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogScreen;
import com.softdesign.skillbrunchmainapp.ui.view_models.AuthViewModel;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import dagger.Provides;
import flow.TreeKey;
import rx.subjects.PublishSubject;

/**
 * @author Sergey Vorobyev.
 */
@Screen(R.layout.screen_auth)
public class AuthScreen extends AbsScreen<CatalogScreen.Component> implements TreeKey {

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerAuthScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new CatalogScreen();
    }

    @DaggerScope(AuthScreen.class)
    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    public interface Component {
        void inject(AuthPresenterImpl presenter);

        void inject(AuthViewImpl view);
    }

    @dagger.Module
    public static class Module {
        @Provides
        @DaggerScope(AuthScreen.class)
        AuthPresenter providePresenter(PublishSubject<ActivityResultDto> activityResultSubject,
                                       CallbackManager fbCallbackManager, TwitterAuthClient twitterAuthClient) {
            return new AuthPresenterImpl(activityResultSubject, fbCallbackManager, twitterAuthClient);
        }

        @Provides
        @DaggerScope(AuthScreen.class)
        AuthModel provideModel() {
            return new AuthModelImpl();
        }

        @Provides
        @DaggerScope(AuthScreen.class)
        AuthViewModel provideViewModel() {
            return new AuthViewModel();
        }

        @Provides
        @DaggerScope(AuthScreen.class)
        CallbackManager provideFbCallbackManager() {
            return CallbackManager.Factory.create();
        }

        @Provides
        @DaggerScope(AuthScreen.class)
        TwitterAuthClient provideTwitterAuthClient() {
            return new TwitterAuthClient();
        }
    }
}
