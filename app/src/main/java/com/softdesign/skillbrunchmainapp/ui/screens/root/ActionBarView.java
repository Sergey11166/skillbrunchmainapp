package com.softdesign.skillbrunchmainapp.ui.screens.root;

import android.support.annotation.StringRes;
import android.support.v4.view.ViewPager;

import java.util.List;

/**
 * @author Sergey Vorobyev
 */
interface ActionBarView {

    void setActionBarTitle(CharSequence title);

    void setActionBarTitle(@StringRes int resId);

    void setActionBarVisibility(boolean visible);

    void setBackArrow(boolean enable);

    void setMenuItems(List<MenuItemHolder> items);

    void setTabLayout(ViewPager viewPager);

    void removeTabLayout();
}
