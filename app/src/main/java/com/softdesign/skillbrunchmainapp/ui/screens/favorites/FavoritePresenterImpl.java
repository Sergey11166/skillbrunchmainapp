package com.softdesign.skillbrunchmainapp.ui.screens.favorites;

import android.os.Bundle;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.auth.AuthScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.MenuItemHolder;
import com.softdesign.skillbrunchmainapp.utils.L;

import flow.Flow;
import mortar.MortarScope;
import rx.Subscriber;
import rx.Subscription;

/**
 * @author Sergey Vorobyev.
 */

public class FavoritePresenterImpl
        extends AbsPresenter<FavoriteViewImpl, FavoriteModel>
        implements FavoritesPresenter<FavoriteViewImpl> {

    private Subscription productsSubs;

    @Override
    public void onCartClick(ProductRealm product) {
        if (!model.isAuthenticated() && getView() != null) {
            Flow.get(getView()).set(new AuthScreen());
        } else {
            model.addToCart(product);
        }
    }

    @Override
    public void onFavoriteClick(ProductRealm product) {
        model.changeFavoriteStatus(product);
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        subscribeOnProductsObs();
    }

    @Override
    public void dropView(FavoriteViewImpl view) {
        productsSubs.unsubscribe();
        super.dropView(view);
    }

    @Override
    protected void initActionBar() {
        rootPresenter.newActionBarBuilder()
                .setTitle(R.string.menu_drawer_catalog)
                .setBackArrow(false)
                .addAction(new MenuItemHolder(0, true, "cart", view -> {
                    if (getRootView() != null) {
                        getRootView().showMessage("Go to the cart");
                    }
                }))
                .build();
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((FavoriteScreen.Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    private void subscribeOnProductsObs() {
        if (getRootView() != null) {
            getRootView().showProgress();
        }
        productsSubs = model.getFavoriteProducts().subscribe(new ProductsSubscriber());
    }

    private class ProductsSubscriber extends Subscriber<ProductRealm> {

        @Override
        public void onCompleted() {
            L.d("Product observable is completed");
        }

        @Override
        public void onError(Throwable e) {
            L.e(e);
            if (getRootView() != null) {
                getRootView().hideProgress();
                getRootView().showMessage(e.getMessage());
            }
        }

        @Override
        public void onNext(ProductRealm productRealm) {
            getView().addFavoriteProduct(productRealm);

            if (getRootView() != null) {
                getRootView().hideProgress();
            }
        }
    }
}
