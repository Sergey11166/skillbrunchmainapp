package com.softdesign.skillbrunchmainapp.ui.screens.account;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.skillbrunchmainapp.data.storage.realm.AddressRealm;
import com.softdesign.skillbrunchmainapp.databinding.ItemAddressBinding;
import com.softdesign.skillbrunchmainapp.ui.view_models.AddressViewModel;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * @author Sergey Vorobyev.
 */
class AddressesRecyclerAdapter extends RecyclerView.Adapter<AddressesRecyclerAdapter.ViewHolder> {

    private List<AddressRealm> data = new ArrayList<>();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemAddressBinding binding = ItemAddressBinding.inflate(inflater, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AddressRealm address = data.get(position);
        AddressViewModel addressViewModel = new AddressViewModel();
        addressViewModel.setPlace(address.getPlace());
        addressViewModel.setFullAddress(address.getFullAddress());
        addressViewModel.setComment(address.getComment());
        addressViewModel.setCommentVisibility(
                address.getComment() != null && !address.getComment().isEmpty() ? VISIBLE : GONE);
        holder.binding.setAddressModel(addressViewModel);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    List<AddressRealm> getData() {
        return data;
    }

    void showAddresses(List<AddressRealm> addresses) {
        data.clear();
        data.addAll(addresses);
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ItemAddressBinding binding;

        private ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
