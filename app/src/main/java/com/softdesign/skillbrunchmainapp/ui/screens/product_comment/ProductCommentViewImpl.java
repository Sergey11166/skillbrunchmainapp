package com.softdesign.skillbrunchmainapp.ui.screens.product_comment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.softdesign.skillbrunchmainapp.databinding.ScreenProductCommentBinding;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsView;
import com.softdesign.skillbrunchmainapp.ui.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductCommentViewModel;

/**
 * @author Sergey Vorobyev
 */
public class ProductCommentViewImpl extends AbsView<ProductCommentPresenter>
        implements ProductCommentView {

    private ScreenProductCommentBinding binding;

    public ProductCommentViewImpl(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void bindViewModel(@NonNull AbsViewModel viewModel) {
        binding.setProductCommentModel((ProductCommentViewModel) viewModel);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            binding = DataBindingUtil.bind(this);
            binding.saveButton.setOnClickListener(v -> presenter.onSaveButtonClick());
        }
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<ProductCommentScreen.Component>getComponent(context).inject(this);
    }
}
