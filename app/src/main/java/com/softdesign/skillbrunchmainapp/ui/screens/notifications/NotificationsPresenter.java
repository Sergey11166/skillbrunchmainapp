package com.softdesign.skillbrunchmainapp.ui.screens.notifications;

import android.view.View;

import com.softdesign.skillbrunchmainapp.mvp.BasePresenter;

/**
 * @author Sergey Vorobyev
 */
interface NotificationsPresenter<V extends View> extends BasePresenter<V> {

    void clickOnCancelButton(int position);

    void clickOnActionButton(int position);
}
