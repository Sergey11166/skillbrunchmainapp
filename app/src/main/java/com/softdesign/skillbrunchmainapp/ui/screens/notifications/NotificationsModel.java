package com.softdesign.skillbrunchmainapp.ui.screens.notifications;

import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;

import rx.Observable;

/**
 * @author Sergey Vorobyev
 */
interface NotificationsModel {

    Observable<NotificationRealm> getNotifications();
}
