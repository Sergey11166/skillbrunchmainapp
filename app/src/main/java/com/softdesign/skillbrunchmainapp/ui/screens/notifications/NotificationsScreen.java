package com.softdesign.skillbrunchmainapp.ui.screens.notifications;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.di.scopes.DaggerScope;
import com.softdesign.skillbrunchmainapp.flow.AbsScreen;
import com.softdesign.skillbrunchmainapp.flow.Screen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;

import dagger.Provides;

/**
 * @author Sergey Vorobyev
 */
@Screen(R.layout.screen_notifications)
public class NotificationsScreen extends AbsScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerNotificationsScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    @DaggerScope(NotificationsScreen.class)
    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    public interface Component {
        void inject(NotificationsPresenterImpl presenter);

        void inject(NotificationsViewImpl view);

        //RootPresenter getRootPresenter();
    }

    @dagger.Module
    public static class Module {

        @Provides
        @DaggerScope(NotificationsScreen.class)
        NotificationsModel provideModel() {
            return new NotificationsModelImpl();
        }

        @Provides
        @DaggerScope(NotificationsScreen.class)
        NotificationsPresenter providePresenter() {
            return new NotificationsPresenterImpl();
        }
    }
}
