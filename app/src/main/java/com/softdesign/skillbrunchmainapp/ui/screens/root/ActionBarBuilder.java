package com.softdesign.skillbrunchmainapp.ui.screens.root;

import android.support.annotation.StringRes;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Vorobyev
 */
public class ActionBarBuilder {

    private static final int DEFAULT_MODE = 0;
    private static final int TAB_MODE = 1;

    private RootView rootView;
    private int toolbarMode = DEFAULT_MODE;
    private boolean isGoBack = false;
    private boolean isVisible = true;
    private ViewPager viewPager;
    private CharSequence title;
    private int titleResId;
    private List<MenuItemHolder> menuActions = new ArrayList<>();

    ActionBarBuilder(RootView rootView) {
        this.rootView = rootView;
    }

    public ActionBarBuilder setBackArrow(boolean enable) {
        this.isGoBack = enable;
        return this;
    }

    public ActionBarBuilder setTitle(CharSequence title) {
        this.title = title;
        return this;
    }

    public ActionBarBuilder setTitle(@StringRes int resId) {
        this.titleResId = resId;
        return this;
    }

    public ActionBarBuilder setVisibility(boolean visible) {
        this.isVisible = visible;
        return this;
    }

    public ActionBarBuilder addAction(MenuItemHolder action) {
        this.menuActions.add(action);
        return this;
    }

    public ActionBarBuilder setTabs(ViewPager viewPager) {
        this.toolbarMode = TAB_MODE;
        this.viewPager = viewPager;
        return this;
    }

    public void build() {
        if (rootView != null) {
            if (rootView instanceof RootActivity) {
                RootActivity activity = (RootActivity) rootView;
                activity.setActionBarVisibility(isVisible);
                if (titleResId == 0) {
                    activity.setActionBarTitle(title);
                } else {
                    activity.setActionBarTitle(titleResId);
                }
                activity.setBackArrow(isGoBack);
                activity.setMenuItems(menuActions);
                if (toolbarMode == TAB_MODE) {
                    activity.setTabLayout(viewPager);
                } else {
                    activity.removeTabLayout();
                }
            } else {
                throw new IllegalStateException("RootView must be instance of RootActivity");
            }
        }
    }
}
