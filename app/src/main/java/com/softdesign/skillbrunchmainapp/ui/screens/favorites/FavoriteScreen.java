package com.softdesign.skillbrunchmainapp.ui.screens.favorites;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.di.scopes.DaggerScope;
import com.softdesign.skillbrunchmainapp.flow.AbsScreen;
import com.softdesign.skillbrunchmainapp.flow.Screen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;

import dagger.Provides;

/**
 * @author Sergey Vorobyev.
 */
@Screen(R.layout.screen_favorites)
public class FavoriteScreen extends AbsScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerFavoriteScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module());
    }

    @DaggerScope(FavoriteScreen.class)
    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    public interface Component {

        void inject(FavoritePresenterImpl presenter);

        void inject(FavoriteViewImpl view);
    }

    @dagger.Module
    public static class Module {

        @Provides
        @DaggerScope(FavoriteScreen.class)
        FavoritesPresenter providePresenter() {
            return new FavoritePresenterImpl();
        }

        @Provides
        @DaggerScope(FavoriteScreen.class)
        FavoriteModel provideModel() {
            return new FavoriteModelImpl();
        }
    }
}
