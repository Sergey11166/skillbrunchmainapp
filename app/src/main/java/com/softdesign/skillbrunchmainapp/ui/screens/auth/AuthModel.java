package com.softdesign.skillbrunchmainapp.ui.screens.auth;

import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;

import rx.Observable;

/**
 * @author Sergey Vorobyev.
 */
interface AuthModel {

    void logout();

    boolean isAuthenticated();

    Observable<UserInfoRealm> login(String email, String password);

    Observable<UserInfoRealm> loginSocial(UserInfoRealm userRequest, SocialSdkType type);

    Observable<Boolean> checkVkAuth();

    Observable<Boolean> checkFbAuth();

    Observable<Boolean> checkTwitterAuth();

    Observable<UserInfoRealm> getVkUser();

    Observable<UserInfoRealm> getFbUser();

    Observable<UserInfoRealm> getTwitterUser();
}
