package com.softdesign.skillbrunchmainapp.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.Button;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsView;
import com.softdesign.skillbrunchmainapp.ui.screens.product.ProductView;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;

import me.relex.circleindicator.CircleIndicator;

import static com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogScreen.Component;

/**
 * @author Sergey Vorobyev.
 */
public class CatalogViewImpl extends AbsView<CatalogPresenter> implements CatalogView {

    private CatalogPagerAdapter pagerAdapter;
    private CircleIndicator pagerIndicator;
    private ViewPager catalogPager;
    private Button addToCartBtn;

    public CatalogViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void addPage(ProductRealm product) {
        pagerAdapter.addItem(product);
        pagerIndicator.getDataSetObserver().onChanged();
    }

    @Override
    public void setPagerPosition(int position) {
        catalogPager.setCurrentItem(position, false);
    }

    @Override
    public int getPageCount() {
        return pagerAdapter.getCount();
    }

    @Override
    public ProductView getCurrentProductView() {
        return (ProductView) catalogPager.findViewWithTag("Product_" + catalogPager.getCurrentItem());
    }

    public int getCurrentPagerPosition() {
        return catalogPager.getCurrentItem();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            catalogPager = (ViewPager) findViewById(R.id.catalog_pager);
            pagerIndicator = (CircleIndicator) findViewById(R.id.indicator);
            addToCartBtn = (Button) findViewById(R.id.add_to_card_Btn);

            pagerAdapter = new CatalogPagerAdapter();
            catalogPager.setAdapter(pagerAdapter);
            pagerIndicator.setViewPager(catalogPager);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            addToCartBtn.setOnClickListener(v -> presenter
                    .onBuyButtonClick(pagerAdapter.getItem(catalogPager.getCurrentItem())));
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        catalogPager.setAdapter(null);
    }

    @Override
    public boolean onViewBackPressed() {
        return getCurrentProductView().onViewBackPressed();
    }

    @Override
    protected void setCheckedNavigationItem(RootView rootView) {
        if (rootView != null) {
            rootView.setCheckedNavigationItem(R.id.nav_catalog);
        }
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<Component>getComponent(context).inject(this);
    }
}
