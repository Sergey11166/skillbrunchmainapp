package com.softdesign.skillbrunchmainapp.ui.screens.account;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.AttributeSet;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.AddressRealm;
import com.softdesign.skillbrunchmainapp.databinding.ScreenAccountBinding;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsView;
import com.softdesign.skillbrunchmainapp.ui.dialogs.CameraNeedGrantPermissionsDialog;
import com.softdesign.skillbrunchmainapp.ui.dialogs.ChangeImageDialog;
import com.softdesign.skillbrunchmainapp.ui.dialogs.EditConfirmDialog;
import com.softdesign.skillbrunchmainapp.ui.dialogs.GalleryNeedGrandPermissionsDialog;
import com.softdesign.skillbrunchmainapp.ui.dialogs.RemoveConfirmDialog;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;
import com.softdesign.skillbrunchmainapp.ui.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.ui.view_models.AccountViewModel;

import java.util.List;

/**
 * @author Sergey Vorobyev.
 */
public class AccountViewImpl extends AbsView<AccountPresenter> implements AccountView {

    private GalleryNeedGrandPermissionsDialog galleryNeedGrandPermissionsDialog;
    private CameraNeedGrantPermissionsDialog cameraNeedGrantPermissionsDialog;
    private RemoveConfirmDialog removeConfirmDialog;
    private ChangeImageDialog changeAvatarDialog;
    private EditConfirmDialog editConfirmDialog;
    private AddressesRecyclerAdapter adapter;
    private ScreenAccountBinding binding;

    public AccountViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void showAddresses(List<AddressRealm> addresses) {
        adapter.showAddresses(addresses);
    }

    @Override
    public void showChangePhotoDialog() {
        if (changeAvatarDialog == null) initChangeAvatarDialog();
        showDialog(changeAvatarDialog);
    }

    @Override
    public void showCameraNeedGrandPermissionsDialog() {
        if (cameraNeedGrantPermissionsDialog == null) initCameraNeedGrandPermissionsDialog();
        showDialog(cameraNeedGrantPermissionsDialog);
    }

    @Override
    public void showGalleryNeedGrandPermissionsDialog() {
        if (galleryNeedGrandPermissionsDialog == null) initGalleryNeedGrandPermissionsDialog();
        showDialog(galleryNeedGrandPermissionsDialog);
    }

    @Override
    public void showFullScreenAvatar() {
    }

    public void showRemoveAddressDialog(int position) {
        if (removeConfirmDialog == null) initRemoveConfirmDialog(position);
        showDialog(removeConfirmDialog);
    }

    public void showEditAddressDialog(int position) {
        if (editConfirmDialog == null) initEditConfirmDialog(position);
        showDialog(editConfirmDialog);
    }

    @Override
    public void bindViewModel(@NonNull AbsViewModel viewModel) {
        binding.setAccountModel((AccountViewModel) viewModel);
    }

    @Override
    public boolean onViewBackPressed() {
        return presenter.onBackPressed();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            binding = DataBindingUtil.bind(this);
            initRecyclerView();
            initWidgets();
        }
    }

    @Override
    protected void initDagger(Context context) {
        if (!isInEditMode()) {
            DaggerService.<AccountScreen.Component>getComponent(context).inject(this);
        }
    }

    @Override
    protected void setCheckedNavigationItem(RootView rootView) {
        if (rootView != null) {
            rootView.setCheckedNavigationItem(R.id.nav_account);
        }
    }

    private void initRecyclerView() {
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setAutoMeasureEnabled(true);
        adapter = new AddressesRecyclerAdapter();
        binding.recycler.setLayoutManager(manager);
        binding.recycler.setAdapter(adapter);

        ItemSwipeCallback swipeCallback = new ItemSwipeCallback(getContext(), 0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    showRemoveAddressDialog(position);
                } else {
                    showEditAddressDialog(position);
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeCallback);
        itemTouchHelper.attachToRecyclerView(binding.recycler);
    }

    private void initWidgets() {
        binding.addAddressBtn.setOnClickListener(v -> presenter.onAddressButtonClick());
        binding.avatar.setOnClickListener(v -> presenter.onAvatarClick());
        binding.fab.setOnClickListener(v -> presenter.onFabClick());
    }

    private void showDialog(DialogFragment dialog) {
        AppCompatActivity activity = (AppCompatActivity) getRootMvpView();
        if (activity != null) {
            FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
            transaction.add(dialog, dialog.getClass().getName());
            transaction.commitAllowingStateLoss();
        }
    }

    private void initChangeAvatarDialog() {
        changeAvatarDialog = ChangeImageDialog.newInstance((dialog, which) -> {
            switch (which) {
                case 0:
                    presenter.onChoiceTakePhoto();
                    dialog.dismiss();
                    break;
                case 1:
                    presenter.onChoiceFromGallery();
                    dialog.dismiss();
                    break;
                case 2:
                    dialog.dismiss();
                    break;
            }
        });
    }

    private void initRemoveConfirmDialog(int position) {
        removeConfirmDialog = RemoveConfirmDialog.newInstance();
        removeConfirmDialog.setOnPositiveButtonClickListener((dialog, which) ->
                presenter.onSwipeAddressRemove(adapter.getData().get(position)));
        removeConfirmDialog.setOnNegativeButtonClickListener((dialog, which) -> adapter.notifyDataSetChanged());
    }

    private void initEditConfirmDialog(int position) {
        editConfirmDialog = EditConfirmDialog.newInstance();
        editConfirmDialog.setOnPositiveButtonClickListener((dialog, which) ->
                presenter.onSwipeAddressEdit(adapter.getData().get(position)));
        editConfirmDialog.setOnNegativeButtonClickListener((dialog, which) -> adapter.notifyDataSetChanged());
    }

    private void initCameraNeedGrandPermissionsDialog() {
        cameraNeedGrantPermissionsDialog = CameraNeedGrantPermissionsDialog.newInstance();
        cameraNeedGrantPermissionsDialog.setOnPositiveButtonClickListener((dialog, which) ->
                presenter.onYesButtonClickInCameraPermissionDialog());
        cameraNeedGrantPermissionsDialog.setOnNeutralButtonClickListener((dialog, which) ->
                presenter.onSettingButtonClickInCameraPermissionDialog());

    }

    private void initGalleryNeedGrandPermissionsDialog() {
        galleryNeedGrandPermissionsDialog = GalleryNeedGrandPermissionsDialog.newInstance();
        galleryNeedGrandPermissionsDialog.setOnPositiveButtonClickListener((dialog, which) ->
                presenter.onYesButtonClickInGalleryPermissionDialog());
        galleryNeedGrandPermissionsDialog.setOnNeutralButtonClickListener((dialog, which) ->
                presenter.onSettingButtonClickInGalleryPermissionDialog());
    }
}
