package com.softdesign.skillbrunchmainapp.ui.screens.favorites;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsView;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;

/**
 * @author Sergey Vorobyev.
 */

public class FavoriteViewImpl extends AbsView<FavoritesPresenter> implements FavoritesView {

    private FavoritesRecyclerAdapter adapter;

    public FavoriteViewImpl(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            initRecyclerView();
        }
    }

    @Override
    public void addFavoriteProduct(ProductRealm product) {

    }

    @Override
    protected void setCheckedNavigationItem(RootView rootView) {
        if (rootView != null) {
            rootView.setCheckedNavigationItem(R.id.nav_favorites);
        }
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<FavoriteScreen.Component>getComponent(context).inject(this);
    }

    private void initRecyclerView() {
        FavoritesRecyclerAdapter.OnItemClickListener onItemClickListener = (position, view) -> {
            ProductRealm product = adapter.getData().get(position);
            switch (view.getId()) {
                case R.id.cart:
                    presenter.onCartClick(product);
                    break;
                case R.id.favorite:
                    presenter.onFavoriteClick(product);
                    break;
            }
        };

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler);
        adapter = new FavoritesRecyclerAdapter(onItemClickListener);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(adapter);
    }
}
