package com.softdesign.skillbrunchmainapp.ui.screens.notifications;

import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;
import com.softdesign.skillbrunchmainapp.mvp.AbsModel;

import rx.Observable;

/**
 * @author Sergey Vorobyev
 */
public class NotificationsModelImpl extends AbsModel implements NotificationsModel {

    @Override
    public Observable<NotificationRealm> getNotifications() {
        return dataManager.getNotificationsFromRealm().distinct(NotificationRealm::getId);
    }
}
