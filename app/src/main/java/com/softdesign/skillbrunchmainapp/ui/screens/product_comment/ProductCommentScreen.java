package com.softdesign.skillbrunchmainapp.ui.screens.product_comment;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.scopes.DaggerScope;
import com.softdesign.skillbrunchmainapp.flow.AbsScreen;
import com.softdesign.skillbrunchmainapp.flow.Screen;
import com.softdesign.skillbrunchmainapp.ui.screens.product_details.ProductDetailsScreen;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductCommentViewModel;

import dagger.Provides;
import flow.TreeKey;

/**
 * @author Sergey Vorobyev
 */
@Screen(R.layout.screen_product_comment)
public class ProductCommentScreen extends AbsScreen<ProductDetailsScreen.Component> implements TreeKey {

    private ProductRealm productRealm;

    public ProductCommentScreen(ProductRealm productRealm) {
        this.productRealm = productRealm;
    }

    @Override
    public Object createScreenComponent(ProductDetailsScreen.Component parentComponent) {
        return DaggerProductCommentScreen_Component.builder()
                .component(parentComponent)
                .module(new Module(productRealm))
                .build();
    }

    @Override
    public Object getParentKey() {
        return new ProductDetailsScreen(productRealm);
    }

    @DaggerScope(ProductCommentScreen.class)
    @dagger.Component(dependencies = ProductDetailsScreen.Component.class, modules = Module.class)
    interface Component {
        void inject(ProductCommentViewImpl view);

        void inject(ProductCommentPresenterImpl presenter);
    }

    @dagger.Module
    static class Module {

        private ProductRealm product;

        public Module(ProductRealm product) {
            this.product = product;
        }

        @Provides
        @DaggerScope(ProductCommentScreen.class)
        ProductCommentPresenter providePresenter() {
            return new ProductCommentPresenterImpl(product);
        }

        @Provides
        @DaggerScope(ProductCommentScreen.class)
        ProductCommentViewModel provideViewModel() {
            return new ProductCommentViewModel();
        }
    }
}
