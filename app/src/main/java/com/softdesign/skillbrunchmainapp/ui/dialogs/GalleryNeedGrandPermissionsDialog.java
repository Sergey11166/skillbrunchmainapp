package com.softdesign.skillbrunchmainapp.ui.dialogs;

import android.os.Bundle;

import com.softdesign.skillbrunchmainapp.R;

/**
 * @author Sergey Vorobyev
 */
public class GalleryNeedGrandPermissionsDialog extends BaseConfirmDialog {

    public static GalleryNeedGrandPermissionsDialog newInstance() {
        GalleryNeedGrandPermissionsDialog dialog = new GalleryNeedGrandPermissionsDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_DIALOG_MASSAGE, R.string.dialog_message_need_grant_gallery_permission);
        dialog.setArguments(bundle);
        dialog.setNeutralButtonTextResId(R.string.dialog_settings_button);
        return dialog;
    }
}
