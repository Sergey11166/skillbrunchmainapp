package com.softdesign.skillbrunchmainapp.ui.screens.address;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.util.AttributeSet;

import com.softdesign.skillbrunchmainapp.databinding.ScreenAddressBinding;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsView;
import com.softdesign.skillbrunchmainapp.ui.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.ui.view_models.AddressViewModel;

/**
 * @author Sergey Vorobyev.
 */
public class AddressViewImpl extends AbsView<AddressPresenter> implements AddressView {

    private ScreenAddressBinding binding;

    public AddressViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            binding = DataBindingUtil.bind(this);
            binding.saveAddressBtn.setOnClickListener(v -> presenter.onSaveButtonClick());
        }
    }

    @Override
    public void bindViewModel(@NonNull AbsViewModel viewModel) {
        binding.setAddressModel((AddressViewModel) viewModel);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<AddressScreen.Component>getComponent(context).inject(this);
    }
}
