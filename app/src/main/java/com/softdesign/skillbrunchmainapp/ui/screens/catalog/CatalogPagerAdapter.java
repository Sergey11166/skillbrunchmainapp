package com.softdesign.skillbrunchmainapp.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.util.SortedList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.utils.L;
import com.softdesign.skillbrunchmainapp.utils.SimpleSortedListCallback;

import mortar.MortarScope;

import static com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogScreen.Factory.createProductContext;

/**
 * @author Sergey Vorobyev.
 */
class CatalogPagerAdapter extends PagerAdapter {

    private SortedList<ProductRealm> productList;

    CatalogPagerAdapter() {

        productList = new SortedList<>(ProductRealm.class, new SimpleSortedListCallback<ProductRealm>() {
            @Override
            public int compare(ProductRealm o1, ProductRealm o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    void addItem(ProductRealm product) {
        productList.add(product);
        notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object) {
        View view = (View) object;
        ProductRealm product = (ProductRealm) view.getTag(R.id.tag_key_product);
        int newPos = productList.indexOf(product);
        int oldPos = (int) view.getTag(R.id.tag_key_catalog_pager_position);
        if (newPos != -1 && newPos != oldPos) {
            return POSITION_NONE;
        } else {
            return POSITION_UNCHANGED;
        }
    }

    ProductRealm getItem(int position) {
        return productList.get(position);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ProductRealm product = productList.get(position);
        Context context = createProductContext(product, container.getContext());
        View view = LayoutInflater.from(context).inflate(R.layout.screen_product, container, false);
        container.addView(view);
        view.setTag(R.id.tag_key_product, product);
        view.setTag(R.id.tag_key_catalog_pager_position, position);
        view.setTag("Product_" + position);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        MortarScope screenScope = MortarScope.getScope(view.getContext());
        container.removeView(view);
        screenScope.destroy();
        L.d("destroy item with name: " + screenScope.getName());
    }
}