package com.softdesign.skillbrunchmainapp.ui.screens.account;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ActivityResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.dto.PermissionsResultDto;
import com.softdesign.skillbrunchmainapp.di.scopes.DaggerScope;
import com.softdesign.skillbrunchmainapp.flow.AbsScreen;
import com.softdesign.skillbrunchmainapp.flow.Screen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.view_models.AccountViewModel;

import dagger.Provides;
import rx.subjects.PublishSubject;

/**
 * @author Sergey Vorobyev.
 */
@Screen(R.layout.screen_account)
public class AccountScreen extends AbsScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAccountScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    @DaggerScope(AccountScreen.class)
    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    public interface Component {
        void inject(AccountPresenterImpl presenter);

        void inject(AccountViewImpl view);

        RootPresenter getRootPresenter();

        AccountModel getAccountModel();
    }

    @dagger.Module
    public static class Module {
        @Provides
        @DaggerScope(AccountScreen.class)
        AccountPresenter providePresenter(PublishSubject<ActivityResultDto> activityResultSubject,
                                          PublishSubject<PermissionsResultDto> permissionsResultSubject) {
            return new AccountPresenterImpl(activityResultSubject, permissionsResultSubject);
        }

        @Provides
        @DaggerScope(AccountScreen.class)
        AccountModel provideModel() {
            return new AccountModelImpl();
        }

        @Provides
        @DaggerScope(AccountScreen.class)
        AccountViewModel provideViewModel() {
            return new AccountViewModel();
        }
    }
}
