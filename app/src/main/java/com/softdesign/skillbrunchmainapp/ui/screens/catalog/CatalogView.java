package com.softdesign.skillbrunchmainapp.ui.screens.catalog;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.mvp.BaseView;
import com.softdesign.skillbrunchmainapp.ui.screens.product.ProductView;

/**
 * @author Sergey Vorobyev.
 */
interface CatalogView extends BaseView {

    void addPage(ProductRealm product);

    void setPagerPosition(int position);

    int getCurrentPagerPosition();

    int getPageCount();

    ProductView getCurrentProductView();
}
