package com.softdesign.skillbrunchmainapp.ui.screens.product;

import com.softdesign.skillbrunchmainapp.mvp.BaseView;

/**
 * @author Sergey Vorobyev.
 */
public interface ProductView extends BaseView {

    void animateAddToCart();
}
