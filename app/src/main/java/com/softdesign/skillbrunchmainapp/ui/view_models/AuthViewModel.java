package com.softdesign.skillbrunchmainapp.ui.view_models;

import android.databinding.Bindable;
import android.support.annotation.StringRes;

import com.softdesign.skillbrunchmainapp.BR;

/**
 * @author Sergey Vorobyev.
 */
public class AuthViewModel extends AbsViewModel {

    private int visibilityCatalogButton;
    private int visibilitySocialButtons;
    private int visibilityProgress;
    private int visibilityInputs;
    private boolean isLoginButtonEnabled;

    private boolean isPasswordValid;
    private boolean isEmailValid;

    private int loginButtonText;
    private String password;
    private String email;

    @Bindable
    public int getVisibilityCatalogButton() {
        return visibilityCatalogButton;
    }

    public void setVisibilityCatalogButton(int visibilityCatalogButton) {
        this.visibilityCatalogButton = visibilityCatalogButton;
        notifyPropertyChanged(BR.visibilityCatalogButton);
    }

    @Bindable
    public int getVisibilitySocialButtons() {
        return visibilitySocialButtons;
    }

    public void setVisibilitySocialButtons(int visibilitySocialButtons) {
        this.visibilitySocialButtons = visibilitySocialButtons;
        notifyPropertyChanged(BR.visibilitySocialButtons);
    }

    @Bindable
    public int getVisibilityProgress() {
        return visibilityProgress;
    }

    public void setVisibilityProgress(int visibilityProgress) {
        this.visibilityProgress = visibilityProgress;
        notifyPropertyChanged(BR.visibilityProgress);
    }

    @Bindable
    public int getVisibilityInputs() {
        return visibilityInputs;
    }

    public void setVisibilityInputs(int visibilityInputs) {
        this.visibilityInputs = visibilityInputs;
        notifyPropertyChanged(BR.visibilityInputs);
    }

    @Bindable
    public boolean isLoginButtonEnabled() {
        return isLoginButtonEnabled;
    }

    public void setLoginButtonEnabled(boolean loginButtonEnabled) {
        isLoginButtonEnabled = loginButtonEnabled;
        notifyPropertyChanged(BR.loginButtonEnabled);
    }

    @Bindable
    public boolean isPasswordValid() {
        return isPasswordValid;
    }

    public void setPasswordValid(boolean passwordValid) {
        isPasswordValid = passwordValid;
        notifyPropertyChanged(BR.passwordValid);
    }

    @Bindable
    public boolean isEmailValid() {
        return isEmailValid;
    }

    public void setEmailValid(boolean emailValid) {
        isEmailValid = emailValid;
        notifyPropertyChanged(BR.emailValid);
    }

    @Bindable
    @StringRes
    public int getLoginButtonText() {
        return loginButtonText;
    }

    public void setLoginButtonText(@StringRes int loginButtonText) {
        this.loginButtonText = loginButtonText;
        notifyPropertyChanged(BR.loginButtonText);
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }
}
