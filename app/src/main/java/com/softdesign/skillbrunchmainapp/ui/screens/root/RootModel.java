package com.softdesign.skillbrunchmainapp.ui.screens.root;

import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;

import rx.Observable;

/**
 * @author Sergey Vorobyev.
 */
public interface RootModel {

    Observable<UserInfoRealm> getUserInfo();

    void saveNotification(NotificationRealm notificationRealm);
}
