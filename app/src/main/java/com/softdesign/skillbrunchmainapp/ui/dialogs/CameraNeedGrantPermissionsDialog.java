package com.softdesign.skillbrunchmainapp.ui.dialogs;

import android.os.Bundle;

import com.softdesign.skillbrunchmainapp.R;

/**
 * @author Sergey Vorobyev
 */
public class CameraNeedGrantPermissionsDialog extends BaseConfirmDialog {

    public static CameraNeedGrantPermissionsDialog newInstance() {
        CameraNeedGrantPermissionsDialog dialog = new CameraNeedGrantPermissionsDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_DIALOG_MASSAGE, R.string.dialog_message_need_grant_camera_permission);
        dialog.setArguments(bundle);
        dialog.setNeutralButtonTextResId(R.string.dialog_settings_button);
        return dialog;
    }
}
