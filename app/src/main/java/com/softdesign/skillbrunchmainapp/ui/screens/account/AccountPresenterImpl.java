package com.softdesign.skillbrunchmainapp.ui.screens.account;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ActivityResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.dto.PermissionsResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.realm.AddressRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsPresenterVM;
import com.softdesign.skillbrunchmainapp.ui.screens.address.AddressScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.MenuItemHolder;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;
import com.softdesign.skillbrunchmainapp.ui.view_models.AccountViewModel;
import com.softdesign.skillbrunchmainapp.utils.IOUtils;
import com.softdesign.skillbrunchmainapp.utils.L;

import java.io.File;
import java.io.IOException;

import flow.Flow;
import io.realm.RealmChangeListener;
import mortar.MortarScope;
import rx.subjects.PublishSubject;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.support.annotation.VisibleForTesting.NONE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.softdesign.skillbrunchmainapp.utils.Constants.CAMERA_PERMISSION_REQUEST_CODE;
import static com.softdesign.skillbrunchmainapp.utils.Constants.GALLERY_PERMISSION_REQUEST_CODE;
import static com.softdesign.skillbrunchmainapp.utils.Constants.REQUEST_CODE_CAMERA;
import static com.softdesign.skillbrunchmainapp.utils.Constants.REQUEST_CODE_GALLERY;
import static com.softdesign.skillbrunchmainapp.utils.Constants.REQUEST_CODE_SETTING_CAMERA;
import static com.softdesign.skillbrunchmainapp.utils.Constants.REQUEST_CODE_SETTING_GALLERY;

/**
 * @author Sergey Vorobyev
 */
class AccountPresenterImpl extends AbsPresenterVM<AccountViewImpl, AccountModel, AccountViewModel>
        implements AccountPresenter<AccountViewImpl> {

    private PublishSubject<ActivityResultDto> activityResultSubject;

    private PublishSubject<PermissionsResultDto> permissionsResultSubject;

    private Uri photoFilePath; // file path for taking a photo
    private String selectedAvatar;
    private UserInfoRealm userInfo;
    private RealmChangeListener<UserInfoRealm> userInfoChangeListener;

    AccountPresenterImpl(PublishSubject<ActivityResultDto> activityResultSubject,
                         PublishSubject<PermissionsResultDto> permissionsResultSubject) {
        this.permissionsResultSubject = permissionsResultSubject;
        this.activityResultSubject = activityResultSubject;
    }

    @Override
    public void onAddressButtonClick() {
        Flow.get(getView()).set(new AddressScreen(userInfo));
    }

    @Override
    public void onFabClick() {
        if (viewModel.isEditMode()) {
            saveEditedData();
        } else {
            viewModel.setEditMode(true);
            viewModel.setUsernameVisibility(VISIBLE);
            viewModel.setAvatarUrl("drawable/shape_avatar_change");
        }
    }

    @Override
    public void onAvatarClick() {
        if (viewModel.isEditMode()) {
            getView().showChangePhotoDialog();
        } else {
            getView().showFullScreenAvatar();
        }
    }

    @Override
    public void onChoiceFromGallery() {
        checkAndRequestGalleryPermissions();
    }

    @Override
    public void onChoiceTakePhoto() {
        checkAndRequestCameraPermissions();
    }

    @Override
    public void onSwipeAddressRemove(AddressRealm address) {
        model.removeAddress(userInfo, address);
    }

    @Override
    public void onSwipeAddressEdit(AddressRealm address) {
        if (getView() != null) {
            Flow.get(getView()).set(new AddressScreen(userInfo, userInfo.getAddresses().indexOf(address)));
        }
    }

    @Override
    public void onYesButtonClickInCameraPermissionDialog() {
        checkAndRequestCameraPermissions();
    }

    @Override
    public void onSettingButtonClickInCameraPermissionDialog() {
        rootPresenter.startSettingForResult(REQUEST_CODE_SETTING_CAMERA);
    }

    @Override
    public void onYesButtonClickInGalleryPermissionDialog() {
        checkAndRequestGalleryPermissions();
    }

    @Override
    public void onSettingButtonClickInGalleryPermissionDialog() {
        rootPresenter.startSettingForResult(REQUEST_CODE_SETTING_GALLERY);
    }

    @Override
    public boolean onBackPressed() {
        if (viewModel.isEditMode()) {
            cancelEdit();
            return true;
        }
        return false;
    }

    @Nullable
    @Override
    public RootView getRootView() {
        return rootPresenter.getView();
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        subscribeOnActivityResult();
        subscribeOnRequestPermissionsResult();
        subscriptions.add(model.getUserInfo().subscribe(this::onUserLoaded, L::e));
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        if (userInfo != null) {
            getView().showAddresses(userInfo.getAddresses());
        }
    }

    @Override
    protected void onExitScope() {
        super.onExitScope();
        subscriptions.unsubscribe();
        userInfo.removeChangeListener(userInfoChangeListener);
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((AccountScreen.Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected void initActionBar() {
        rootPresenter.newActionBarBuilder()
                .setTitle(R.string.menu_drawer_account)
                .setBackArrow(false)
                .addAction(new MenuItemHolder(0, true, "cart count", item -> {
                    if (getRootView() != null) getRootView().showMessage("Go to the cart");
                }))
                .build();
    }

    private void onUserLoaded(UserInfoRealm userInfo) {
        this.userInfo = userInfo;
        initViewModel();
        getView().showAddresses(userInfo.getAddresses());

        userInfoChangeListener = element -> {
            initViewModel();
            if (getView() != null) {
                getView().showAddresses(element.getAddresses());
            }
        };
        this.userInfo.addChangeListener(userInfoChangeListener);
    }

    private void initViewModel() {
        viewModel.setUsername(userInfo.getUsername());
        viewModel.setUsernameVisibility(GONE);
        viewModel.setAvatarUrl(userInfo.getAvatarUrl());
        viewModel.setPhone(userInfo.getPhone());
        viewModel.setPushOffers(userInfo.isPushOffers());
        viewModel.setPushOrderStatus(userInfo.isPushOrderStatus());
    }

    private void cancelEdit() {
        viewModel.setEditMode(false);
        viewModel.setUsernameVisibility(GONE);
        viewModel.setUsername(userInfo.getUsername());
        viewModel.setPhone(userInfo.getPhone());
        viewModel.setAvatarUrl(userInfo.getAvatarUrl());
    }

    private void saveEditedData() {
        if (selectedAvatar != null) {
            viewModel.setAvatarUrl(selectedAvatar);
        }
        model.updateUserInfo(userInfo, new UserInfoRealm(viewModel), selectedAvatar != null);
        viewModel.setEditMode(false);
    }

    private void subscribeOnActivityResult() {
        subscriptions.add(activityResultSubject
                .filter(result -> result.getRequestCode() == REQUEST_CODE_CAMERA &&
                        result.getResultCode() == RESULT_OK && photoFilePath != null)
                .subscribe(resultDto -> selectedAvatar = photoFilePath.toString(), L::e));

        subscriptions.add(activityResultSubject
                .filter(result -> result.getRequestCode() == REQUEST_CODE_GALLERY &&
                        result.getResultCode() == RESULT_OK && result.getIntent() != null &&
                        result.getIntent().getData() != null)
                .subscribe(result -> selectedAvatar = result.getIntent().getData().toString(), L::e));

        subscriptions.add(activityResultSubject
                .filter(result -> result.getRequestCode() == REQUEST_CODE_SETTING_CAMERA)
                .subscribe(r -> checkAndRequestCameraPermissions()));

        subscriptions.add(activityResultSubject
                .filter(result -> result.getRequestCode() == REQUEST_CODE_SETTING_GALLERY)
                .subscribe(r -> checkAndRequestGalleryPermissions()));
    }

    private void subscribeOnRequestPermissionsResult() {
        subscriptions.add(permissionsResultSubject
                .filter(result -> result.getRequestCode() == CAMERA_PERMISSION_REQUEST_CODE)
                .subscribe(this::handleCameraRequestPermissionsResult, L::e));

        subscriptions.add(permissionsResultSubject
                .filter(result -> result.getRequestCode() == GALLERY_PERMISSION_REQUEST_CODE)
                .subscribe(this::handleGalleryRequestPermissionsResult, L::e));
    }

    private void handleCameraRequestPermissionsResult(PermissionsResultDto result) {
        if (result.getGrantResults()[0] == PERMISSION_GRANTED &&
                result.getGrantResults()[1] == PERMISSION_GRANTED) {
            handleCameraPermissionsGranted();
        } else {
            if (getView() != null) {
                getView().showCameraNeedGrandPermissionsDialog();
            }
        }
    }

    private void handleGalleryRequestPermissionsResult(PermissionsResultDto result) {
        if (result.getGrantResults()[0] == PERMISSION_GRANTED) {
            handleGalleryPermissionsGranted();
        } else {
            if (getView() != null) {
                getView().showGalleryNeedGrandPermissionsDialog();
            }
        }
    }

    private void checkAndRequestCameraPermissions() {
        String[] permissions = {CAMERA, WRITE_EXTERNAL_STORAGE};
        if (rootPresenter.checkAndRequestPermission(permissions, CAMERA_PERMISSION_REQUEST_CODE)) {
            handleCameraPermissionsGranted();
        }
    }

    private void checkAndRequestGalleryPermissions() {
        String[] permissions = {READ_EXTERNAL_STORAGE};
        if (rootPresenter.checkAndRequestPermission(permissions, GALLERY_PERMISSION_REQUEST_CODE)) {
            handleGalleryPermissionsGranted();
        }
    }

    private void handleCameraPermissionsGranted() {
        File file = null;
        try {
            file = IOUtils.createImageFile(getView().getContext());
            photoFilePath = Uri.fromFile(file);
        } catch (IOException e) {
            L.e(e);
            e.printStackTrace();
        }
        rootPresenter.startCameraForResult(file, REQUEST_CODE_CAMERA);
    }

    private void handleGalleryPermissionsGranted() {
        rootPresenter.startGalleryForResult(REQUEST_CODE_GALLERY);
    }

    @VisibleForTesting(otherwise = NONE)
    void setMockSelectedAvatar() {
        this.selectedAvatar = "mockSelectedAvatar";
    }
}
