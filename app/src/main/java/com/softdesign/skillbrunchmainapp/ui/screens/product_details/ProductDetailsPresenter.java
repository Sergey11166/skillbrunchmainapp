package com.softdesign.skillbrunchmainapp.ui.screens.product_details;

import android.view.View;

import com.softdesign.skillbrunchmainapp.mvp.BasePresenter;

/**
 * @author Sergey Vorobyev
 */
interface ProductDetailsPresenter<V extends View> extends BasePresenter<V> {

    void onPlusButtonClick();

    void onMinusButtonClick();

    void onFavoriteClick();

    void onAddCommentClick();
}
