package com.softdesign.skillbrunchmainapp.ui.screens.auth;

import com.facebook.AccessToken;
import com.facebook.Profile;
import com.softdesign.skillbrunchmainapp.data.network.requests.SocialLoginRequest;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.mvp.AbsModel;
import com.twitter.sdk.android.Twitter;
import com.vk.sdk.VKAccessToken;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.softdesign.skillbrunchmainapp.utils.Constants.FB_API_BASE_URL;
import static com.softdesign.skillbrunchmainapp.utils.Constants.VK_API_BASE_URL;

/**
 * @author Sergey Vorobyev.
 */
class AuthModelImpl extends AbsModel implements AuthModel {

    @Override
    public void logout() {
        dataManager.deleteToken();
    }

    @Override
    public boolean isAuthenticated() {
        return dataManager.isAuthenticate();
    }

    @Override
    public Observable<UserInfoRealm> login(String email, String password) {
        return dataManager.login(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UserInfoRealm> loginSocial(UserInfoRealm userInfo, SocialSdkType type) {
        String userId = null;
        switch (type) {
            case VK:
                userId = VKAccessToken.currentToken().userId;
                break;
            case FB:
                userId = Profile.getCurrentProfile().getId();
                break;
            case TWITTER:
                userId = String.valueOf(Twitter.getSessionManager().getActiveSession().getUserId());
        }
        SocialLoginRequest request = new SocialLoginRequest(userId, userInfo.getUsername(),
                userInfo.getPhone(), userInfo.getAvatarUrl());

        return dataManager.socialLogin(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Boolean> checkVkAuth() {
        return dataManager.checkVkAuth();
    }

    @Override
    public Observable<Boolean> checkFbAuth() {
        return dataManager.checkFbAuth();
    }

    @Override
    public Observable<Boolean> checkTwitterAuth() {
        return dataManager.checkTwitterAuth();
    }

    @Override
    public Observable<UserInfoRealm> getVkUser() {
        return dataManager.getProfileVk(VK_API_BASE_URL, VKAccessToken.currentToken().accessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UserInfoRealm> getFbUser() {
        return dataManager.getProfileFb(FB_API_BASE_URL, AccessToken.getCurrentAccessToken().getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UserInfoRealm> getTwitterUser() {
        return dataManager.getProfileTwitter();
    }
}
