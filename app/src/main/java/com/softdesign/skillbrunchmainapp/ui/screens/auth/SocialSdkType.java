package com.softdesign.skillbrunchmainapp.ui.screens.auth;

/**
 * @author Sergey Vorobyev
 */
public enum SocialSdkType {
    FB, VK, TWITTER
}
