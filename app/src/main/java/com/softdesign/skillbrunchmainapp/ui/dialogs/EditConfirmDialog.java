package com.softdesign.skillbrunchmainapp.ui.dialogs;

import android.os.Bundle;

import com.softdesign.skillbrunchmainapp.R;

/**
 * @author Sergey Vorobyev
 */
public class EditConfirmDialog extends BaseConfirmDialog {

    public static EditConfirmDialog newInstance() {
        EditConfirmDialog dialog = new EditConfirmDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_DIALOG_MASSAGE, R.string.dialog_message_confirm_edit);
        dialog.setArguments(bundle);
        return dialog;
    }
}
