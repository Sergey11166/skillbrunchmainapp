package com.softdesign.skillbrunchmainapp.ui.screens.product_details;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.softdesign.skillbrunchmainapp.App;
import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductCommentRealm;
import com.softdesign.skillbrunchmainapp.utils.BindingUtils;

import java.util.ArrayList;
import java.util.List;

import static android.text.format.DateUtils.MINUTE_IN_MILLIS;
import static android.text.format.DateUtils.getRelativeTimeSpanString;
import static java.lang.System.currentTimeMillis;

/**
 * @author Sergey Vorobyev
 */
class CommentsRecyclerAdapter extends RecyclerView.Adapter<CommentsRecyclerAdapter.ViewHolder> {

    private List<ProductCommentRealm> data = new ArrayList<>();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product_comment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProductCommentRealm comment = data.get(position);

        BindingUtils.loadImage(holder.avatar, comment.getAvatar(),
                ContextCompat.getDrawable(App.get(), R.color.color_gray));
        holder.title.setText(comment.getUsername());

        CharSequence relativeDate = getRelativeTimeSpanString(comment.getCommentDate().getTime(),
                currentTimeMillis(), MINUTE_IN_MILLIS);
        holder.date.setText(relativeDate);
        holder.rating.setRating(comment.getRating());
        holder.comment.setText(comment.getComment());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<ProductCommentRealm> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView avatar;
        private TextView title;
        private TextView date;
        private TextView comment;
        private RatingBar rating;

        ViewHolder(View itemView) {
            super(itemView);

            avatar = (ImageView) itemView.findViewById(R.id.avatar);
            title = (TextView) itemView.findViewById(R.id.title);
            date = (TextView) itemView.findViewById(R.id.date_comment);
            comment = (TextView) itemView.findViewById(R.id.comment);
            rating = (RatingBar) itemView.findViewById(R.id.rating_comment);
        }
    }
}
