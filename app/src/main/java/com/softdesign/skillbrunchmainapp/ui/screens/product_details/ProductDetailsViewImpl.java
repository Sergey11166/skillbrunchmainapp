package com.softdesign.skillbrunchmainapp.ui.screens.product_details;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductCommentRealm;
import com.softdesign.skillbrunchmainapp.databinding.ScreenProductDetailsBinding;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsView;
import com.softdesign.skillbrunchmainapp.ui.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductViewModel;

import java.util.List;

/**
 * @author Sergey Vorobyev
 */
public class ProductDetailsViewImpl extends AbsView<ProductDetailsPresenter>
        implements ProductDetailsView, View.OnClickListener {

    private ProductDetailsPagerAdapter pagerAdapter;
    private ScreenProductDetailsBinding binding;

    public ProductDetailsViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void bindViewModel(@NonNull AbsViewModel viewModel) {
        ProductViewModel productViewModel = (ProductViewModel) viewModel;
        binding = DataBindingUtil.bind(this);
        binding.setProductModel(productViewModel);
        pagerAdapter = new ProductDetailsPagerAdapter(productViewModel, this);
        binding.viewPager.setAdapter(pagerAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.plusBtn:
                presenter.onPlusButtonClick();
                break;
            case R.id.minusBtn:
                presenter.onMinusButtonClick();
                break;
            case R.id.favorite_fab:
                presenter.onFavoriteClick();
                break;
            case R.id.add_fab:
                presenter.onAddCommentClick();
                break;
        }
    }

    @Override
    public void showComments(List<ProductCommentRealm> comments) {
        pagerAdapter.getCommentsAdapter().setData(comments);
    }

    @Override
    public ViewPager getViewPager() {
        return binding.viewPager;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<ProductDetailsScreen.Component>getComponent(context).inject(this);
    }
}
