package com.softdesign.skillbrunchmainapp.ui.screens.product;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsPresenterVM;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogModel;
import com.softdesign.skillbrunchmainapp.ui.screens.product_details.ProductDetailsScreen;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductViewModel;

import flow.Flow;
import mortar.MortarScope;

/**
 * @author Sergey Vorobyev
 */
class ProductPresenterImpl extends AbsPresenterVM<ProductViewImpl, CatalogModel, ProductViewModel>
        implements ProductPresenter<ProductViewImpl> {

    private ProductRealm product;

    ProductPresenterImpl(ProductRealm product) {
        this.product = product;
    }

    @Override
    public void onPlusButtonClick() {
        model.incrementProductCount(product);
    }

    @Override
    public void onMinusButtonClick() {
        if (product.getCount() > 1) {
            model.decrementProductCount(product);
        }
    }

    @Override
    public void onFavoriteClick() {
        model.changeFavoriteStatus(product);
    }

    @Override
    public void onDetailsClick() {
        Flow.get(getView()).set(new ProductDetailsScreen(product));
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        if (product.isManaged()) {
            product.addChangeListener(element -> updateTotalPrice());
        }
        initViewModel();
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((ProductScreen.Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    private void initViewModel() {
        updateTotalPrice();
        viewModel.setDescription(product.getDescription());
        viewModel.setName(product.getProductName());
        viewModel.setImage(product.getImageUrl());
        viewModel.setFavorite(product.isFavorite());
    }

    private void updateTotalPrice() {
        viewModel.setPrice(String.valueOf(product.getCount() * product.getPrice()));
        viewModel.setCount(String.valueOf(product.getCount()));
    }
}
