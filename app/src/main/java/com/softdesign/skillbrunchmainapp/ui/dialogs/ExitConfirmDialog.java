package com.softdesign.skillbrunchmainapp.ui.dialogs;

import android.os.Bundle;

import com.softdesign.skillbrunchmainapp.R;

/**
 * @author Sergey Vorobyev
 */
public class ExitConfirmDialog extends BaseConfirmDialog {

    public static ExitConfirmDialog newInstance() {
        ExitConfirmDialog dialog = new ExitConfirmDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_DIALOG_MASSAGE, R.string.catalog_dialog_exit_confirm_mesaage);
        dialog.setArguments(bundle);
        return dialog;
    }
}
