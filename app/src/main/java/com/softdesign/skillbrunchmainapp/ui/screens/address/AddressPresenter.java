package com.softdesign.skillbrunchmainapp.ui.screens.address;

import android.view.View;

import com.softdesign.skillbrunchmainapp.mvp.BasePresenter;

/**
 * @author Sergey Vorobyev.
 */
interface AddressPresenter<V extends View> extends BasePresenter<V> {

    void onSaveButtonClick();
}
