package com.softdesign.skillbrunchmainapp.ui.screens.notifications;

import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;

/**
 * @author Sergey Vorobyev
 */
interface NotificationsView {

    void addNotification(NotificationRealm notification);
}
