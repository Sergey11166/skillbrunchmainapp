package com.softdesign.skillbrunchmainapp.ui.screens.product_details;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.scopes.DaggerScope;
import com.softdesign.skillbrunchmainapp.flow.AbsScreen;
import com.softdesign.skillbrunchmainapp.flow.Screen;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogModel;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductViewModel;

import dagger.Provides;
import flow.TreeKey;

/**
 * @author Sergey Vorobyev
 */
@Screen(R.layout.screen_product_details)
public class ProductDetailsScreen extends AbsScreen<CatalogScreen.Component> implements TreeKey {

    private ProductRealm productRealm;

    public ProductDetailsScreen(ProductRealm productRealm) {
        this.productRealm = productRealm;
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerProductDetailsScreen_Component.builder()
                .component(parentComponent)
                .module(new Module(productRealm))
                .build();
    }

    @Override
    public Object getParentKey() {
        return new CatalogScreen();
    }

    @DaggerScope(ProductDetailsScreen.class)
    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    public interface Component {
        void inject(ProductDetailsViewImpl view);

        void inject(ProductDetailsPresenterImpl presenter);

        CatalogModel getCatalogModel();

        RootPresenter getRootPresenter();
    }

    @dagger.Module
    public static class Module {

        private ProductRealm product;

        public Module(ProductRealm product) {
            this.product = product;
        }

        @Provides
        @DaggerScope(ProductDetailsScreen.class)
        ProductDetailsPresenter providePresenter() {
            return new ProductDetailsPresenterImpl(product);
        }

        @Provides
        @DaggerScope(ProductDetailsScreen.class)
        ProductViewModel provideViewModel() {
            return new ProductViewModel();
        }
    }
}
