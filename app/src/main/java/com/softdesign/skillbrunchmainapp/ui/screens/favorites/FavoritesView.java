package com.softdesign.skillbrunchmainapp.ui.screens.favorites;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;

/**
 * @author Sergey Vorobyev.
 */
public interface FavoritesView {

    void addFavoriteProduct(ProductRealm product);
}
