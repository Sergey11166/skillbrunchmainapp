package com.softdesign.skillbrunchmainapp.ui.view_models;

import android.databinding.Bindable;

import com.softdesign.skillbrunchmainapp.BR;

/**
 * @author Sergey Vorobyev.
 */
@SuppressWarnings("unused")
public class AccountViewModel extends AbsViewModel {

    private String username;
    private int usernameVisibility;
    private String avatarUrl;
    private String phone;
    private boolean isPushOrderStatus;
    private boolean isPushOffers;
    private boolean isEditMode;

    @Bindable
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
        notifyPropertyChanged(BR.username);
    }

    @Bindable
    public int getUsernameVisibility() {
        return usernameVisibility;
    }

    public void setUsernameVisibility(int usernameVisibility) {
        this.usernameVisibility = usernameVisibility;
        notifyPropertyChanged(BR.usernameVisibility);
    }

    @Bindable
    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
        notifyPropertyChanged(BR.avatarUrl);
    }

    @Bindable
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        notifyPropertyChanged(BR.phone);
    }

    @Bindable
    public boolean isPushOrderStatus() {
        return isPushOrderStatus;
    }

    public void setPushOrderStatus(boolean pushOrderStatus) {
        isPushOrderStatus = pushOrderStatus;
        notifyPropertyChanged(BR.pushOrderStatus);
    }

    @Bindable
    public boolean isPushOffers() {
        return isPushOffers;
    }

    public void setPushOffers(boolean pushOffers) {
        isPushOffers = pushOffers;
        notifyPropertyChanged(BR.pushOffers);
    }

    @Bindable
    public boolean isEditMode() {
        return isEditMode;
    }

    public void setEditMode(boolean editMode) {
        isEditMode = editMode;
        notifyPropertyChanged(BR.editMode);
    }
}
