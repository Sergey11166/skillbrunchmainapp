package com.softdesign.skillbrunchmainapp.ui.view_models;

import android.databinding.Bindable;

import com.softdesign.skillbrunchmainapp.BR;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class ProductCommentViewModel extends AbsViewModel {

    private float rating;
    private boolean isCommentValid;
    private String comment;

    @Bindable
    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
        notifyPropertyChanged(BR.rating);
    }

    @Bindable
    public boolean isCommentValid() {
        return isCommentValid;
    }

    public void setCommentValid(boolean commentValid) {
        isCommentValid = commentValid;
        notifyPropertyChanged(BR.commentValid);
    }

    @Bindable
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
        notifyPropertyChanged(BR.comment);
    }
}
