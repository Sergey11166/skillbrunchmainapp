package com.softdesign.skillbrunchmainapp.ui.screens.auth;

import com.softdesign.skillbrunchmainapp.mvp.BaseView;

/**
 * @author Sergey Vorobyev.
 */
interface AuthView extends BaseView {

    void showMessageLoggedIn();

    void showMessageLoginError();

    void alphaAnimateHideShowInputs(boolean isShow);

    void slideAnimateHideShowInputs(boolean isShow);

    void animateHideShowCatalogButton(boolean isShow);

    void animateHideShowSocialButtons(boolean isShow);

    void animateHideShowProgress(boolean isShow);
}
