package com.softdesign.skillbrunchmainapp.ui.screens.catalog;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductCommentRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;

import rx.Observable;

/**
 * @author Sergey Vorobyev.
 */
public interface CatalogModel {

    Observable<ProductRealm> getProducts();

    void incrementProductCount(ProductRealm product);

    void decrementProductCount(ProductRealm product);

    void changeFavoriteStatus(ProductRealm product);

    boolean isAuthenticated();

    int getCartCount();

    void addToCart(ProductRealm product);

    void addProductComment(ProductRealm product, ProductCommentRealm comment);
}
