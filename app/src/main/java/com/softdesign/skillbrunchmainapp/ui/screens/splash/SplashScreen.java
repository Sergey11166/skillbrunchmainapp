package com.softdesign.skillbrunchmainapp.ui.screens.splash;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.di.scopes.DaggerScope;
import com.softdesign.skillbrunchmainapp.flow.AbsScreen;
import com.softdesign.skillbrunchmainapp.flow.Screen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;

import dagger.Provides;

/**
 * @author Sergey Vorobyev
 */
@Screen(R.layout.screen_splash)
public class SplashScreen extends AbsScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerSplashScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    @DaggerScope(SplashScreen.class)
    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    public interface Component {
        void inject(SplashViewImpl view);

        void inject(SplashPresenterImpl presenter);
    }

    @dagger.Module
    public static class Module {
        @Provides
        @DaggerScope(SplashScreen.class)
        SplashModel provideModel() {
            return new SplashModelImpl();
        }

        @Provides
        @DaggerScope(SplashScreen.class)
        SplashPresenter providePresenter() {
            return new SplashPresenterImpl();
        }
    }
}
