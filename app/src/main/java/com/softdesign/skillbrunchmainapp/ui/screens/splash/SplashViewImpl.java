package com.softdesign.skillbrunchmainapp.ui.screens.splash;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsView;

/**
 * @author Sergey Vorobyev
 */
public class SplashViewImpl extends AbsView<SplashPresenter> implements SplashView {

    public SplashViewImpl(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<SplashScreen.Component>getComponent(context).inject(this);
    }

    @Override
    public boolean onViewBackPressed() {
        return true;
    }
}
