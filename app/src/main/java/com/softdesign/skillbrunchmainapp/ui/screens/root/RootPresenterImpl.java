package com.softdesign.skillbrunchmainapp.ui.screens.root;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.softdesign.skillbrunchmainapp.data.storage.dto.ActivityResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.dto.PermissionsResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.ui.view_models.RootViewModel;
import com.softdesign.skillbrunchmainapp.utils.L;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.io.File;

import rx.subjects.PublishSubject;

/**
 * @author Sergey Vorobyev.
 */
public class RootPresenterImpl implements RootPresenter {

    private RootView view;
    private RootModel model;
    private RootViewModel viewModel;
    private boolean isFirstStart = true;

    private PublishSubject<ActivityResultDto> activityResultSubject;
    private PublishSubject<PermissionsResultDto> requestPermissionsResultSubject;

    public RootPresenterImpl(RootModel model, RootViewModel viewModel,
                             PublishSubject<ActivityResultDto> activityResultSubject,
                             PublishSubject<PermissionsResultDto> requestPermissionsResultSubject) {
        this.model = model;
        this.viewModel = viewModel;
        this.activityResultSubject = activityResultSubject;
        this.requestPermissionsResultSubject = requestPermissionsResultSubject;
    }

    @Override
    public void takeView(@NonNull RootView rootView) {
        view = rootView;
        view.bindViewModel(viewModel);
        onViewBound(isFirstStart);
        isFirstStart = false;
    }

    @Override
    public void dropView() {
        view = null;
    }

    @Override
    public void setCartCount(int count) {
        viewModel.setCartCount(String.valueOf(count));
    }

    @Nullable
    @Override
    public RootView getView() {
        return view;
    }

    private void onViewBound(boolean isFirstBound) {
        if (isFirstBound) {
            model.getUserInfo().subscribe(this::onUserInfoLoaded, L::e);
        }
    }

    private void onUserInfoLoaded(UserInfoRealm userInfo) {
        userInfo.addChangeListener(this::updateUserInfo);
        updateUserInfo(userInfo);
    }

    @Override
    public void startLoginVk() {
        if (getView() != null) {
            getView().startLoginVk();
        }
    }

    @Override
    public void startLoginFb() {
        if (getView() != null) {
            getView().startLoginFb();
        }
    }

    @Override
    public void startLoginTwitter(TwitterAuthClient client) {
        if (getView() != null) {
            getView().startLoginTwitter(client);
        }
    }

    @Override
    public void startCameraForResult(File file, int requestCode) {
        if (getView() != null) {
            getView().startCameraForResult(file, requestCode);
        }
    }

    @Override
    public void startGalleryForResult(int requestCode) {
        if (getView() != null) {
            getView().startGalleryForResult(requestCode);
        }
    }

    @Override
    public void startSettingForResult(int requestCode) {
        if (getView() != null) {
            getView().startSettingForResult(requestCode);
        }
    }

    @Override
    public boolean checkAndRequestPermission(@NonNull String[] permissions, int requestCode) {
        if (getView() != null) {
            boolean isAllGranted = getView().isAllPermissionsGranted(permissions);
            if (!isAllGranted) {
                getView().requestPermissions(permissions, requestCode);
            }
            return isAllGranted;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        activityResultSubject.onNext(new ActivityResultDto(requestCode, resultCode, data));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        requestPermissionsResultSubject
                .onNext(new PermissionsResultDto(requestCode, permissions, grantResults));
    }

    @Override
    public void onStartFromNotification(NotificationRealm notification) {
        model.saveNotification(notification);
    }

    @Override
    public ActionBarBuilder newActionBarBuilder() {
        return new ActionBarBuilder(getView());
    }

    private void updateUserInfo(UserInfoRealm userInfo) {
        viewModel.setUsername(userInfo.getUsername());
        viewModel.setAvatarUri(userInfo.getAvatarUrl());
    }
}
