package com.softdesign.skillbrunchmainapp.ui.screens.catalog;

import android.view.View;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.mvp.BasePresenter;

/**
 * @author Sergey Vorobyev.
 */
public interface CatalogPresenter<V extends View> extends BasePresenter<V> {

    void onBuyButtonClick(ProductRealm productId);
}
