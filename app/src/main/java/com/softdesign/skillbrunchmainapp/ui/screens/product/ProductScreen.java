package com.softdesign.skillbrunchmainapp.ui.screens.product;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.scopes.DaggerScope;
import com.softdesign.skillbrunchmainapp.flow.AbsScreen;
import com.softdesign.skillbrunchmainapp.flow.Screen;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogScreen;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductViewModel;

import dagger.Provides;

/**
 * @author Sergey Vorobyev.
 */

@Screen(R.layout.screen_product)
public class ProductScreen extends AbsScreen<CatalogScreen.Component> {

    private ProductRealm productRealm;

    public ProductScreen(ProductRealm productRealm) {
        this.productRealm = productRealm;
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerProductScreen_Component.builder()
                .component(parentComponent)
                .module(new Module(productRealm))
                .build();
    }

    @DaggerScope(ProductScreen.class)
    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    public interface Component {
        void inject(ProductPresenterImpl presenter);

        void inject(ProductViewImpl view);
    }

    @dagger.Module
    public static class Module {

        private ProductRealm product;

        public Module(ProductRealm product) {
            this.product = product;
        }

        @Provides
        @DaggerScope(ProductScreen.class)
        ProductPresenter providePresenter() {
            return new ProductPresenterImpl(product);
        }

        @Provides
        @DaggerScope(ProductScreen.class)
        ProductViewModel provideViewModel() {
            return new ProductViewModel();
        }
    }
}
