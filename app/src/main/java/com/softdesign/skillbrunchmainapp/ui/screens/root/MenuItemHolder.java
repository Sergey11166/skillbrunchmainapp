package com.softdesign.skillbrunchmainapp.ui.screens.root;

import android.view.View.OnClickListener;

/**
 * @author Sergey Vorobyev
 */
public class MenuItemHolder {

    private final int iconResId;
    private final CharSequence itemTitle;
    private final OnClickListener listener;
    private final boolean hasCustomView;

    public MenuItemHolder(int iconResId, boolean hasCustomView, CharSequence itemTitle,
                          OnClickListener listener) {
        this.iconResId = iconResId;
        this.hasCustomView = hasCustomView;
        this.itemTitle = itemTitle;
        this.listener = listener;
    }

    int getIconResId() {
        return iconResId;
    }

    boolean hasCustomView() {
        return hasCustomView;
    }

    CharSequence getTitle() {
        return itemTitle;
    }

    OnClickListener getListener() {
        return listener;
    }
}
