package com.softdesign.skillbrunchmainapp.ui.screens.product;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.LinearLayout;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.databinding.ScreenProductBinding;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsView;
import com.softdesign.skillbrunchmainapp.ui.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductViewModel;
import com.softdesign.skillbrunchmainapp.utils.UIUtils;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.ChangeImageTransform;
import com.transitionseverywhere.Explode;
import com.transitionseverywhere.SidePropagation;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

import java.util.ArrayList;
import java.util.List;

import static android.animation.ObjectAnimator.ofArgb;
import static android.support.v4.content.ContextCompat.getColor;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.widget.ImageView.ScaleType.CENTER_CROP;
import static android.widget.ImageView.ScaleType.FIT_CENTER;

/**
 * @author Sergey Vorobyev.
 */
public class ProductViewImpl extends AbsView<ProductPresenter> implements ProductView {

    private static final int ANIMATION_DURATION = 300;

    private ScreenProductBinding binding;

    private AnimatorSet resultSet;
    private List<View> childList;
    private boolean isZoomed;
    private int density;

    public ProductViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
        density = (int) UIUtils.getDensity(getContext());
    }

    @Override
    public void bindViewModel(@NonNull AbsViewModel viewModel) {
        binding.setProductModel((ProductViewModel) viewModel);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            binding = DataBindingUtil.bind(this);
            binding.plusBtn.setOnClickListener(v -> presenter.onPlusButtonClick());
            binding.minusBtn.setOnClickListener(v -> presenter.onMinusButtonClick());
            binding.favorite.setOnClickListener(v -> presenter.onFavoriteClick());
            binding.details.setOnClickListener(v -> presenter.onDetailsClick());
            binding.productImage.setOnClickListener(v -> startZoomTransition());
        }
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<ProductScreen.Component>getComponent(context).inject(this);
    }

    @Override
    public boolean onViewBackPressed() {
        if (isZoomed) {
            startZoomTransition();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void animateAddToCart() {
        final int cx = (binding.productWrap.getLeft() + binding.productWrap.getRight()) / 2;
        final int cy = (binding.productWrap.getTop() + binding.productWrap.getBottom()) / 2;
        final int radius = Math.max(binding.productWrap.getWidth(), binding.productWrap.getHeight());

        final Animator hideCircleAnim;
        final Animator showCircleAnim;
        Animator hideColorAnim = null;
        Animator showColorAnim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            hideCircleAnim = ViewAnimationUtils.createCircularReveal(binding.productWrap, cx, cy, radius, 0);
            hideCircleAnim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    binding.productWrap.setVisibility(INVISIBLE);
                }
            });

            showCircleAnim = ViewAnimationUtils.createCircularReveal(binding.productWrap, cx, cy, 0, radius);
            showCircleAnim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    binding.productWrap.setVisibility(VISIBLE);
                }
            });

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ColorDrawable foreground = (ColorDrawable) binding.productWrap.getForeground();
                hideColorAnim = ofArgb(foreground, "color", getColor(getContext(), R.color.colorAccent));
                showColorAnim = ofArgb(foreground, "color", getColor(getContext(), R.color.transparent));
            }

        } else {
            hideCircleAnim = ObjectAnimator.ofFloat(binding.productWrap, "alpha", 0);
            showCircleAnim = ObjectAnimator.ofFloat(binding.productWrap, "alpha", 1);
        }

        AnimatorSet hideSet = new AnimatorSet();
        AnimatorSet showSet = new AnimatorSet();

        addAnimatorTogetherInSet(hideSet, hideCircleAnim, hideColorAnim);
        addAnimatorTogetherInSet(showSet, showCircleAnim, showColorAnim);

        hideSet.setDuration(ANIMATION_DURATION);
        showSet.setDuration(ANIMATION_DURATION);

        hideSet.setInterpolator(new FastOutSlowInInterpolator());
        hideSet.setInterpolator(new FastOutSlowInInterpolator());

        showSet.setStartDelay(ANIMATION_DURATION);

        if ((resultSet != null && !resultSet.isStarted()) || resultSet == null) {
            resultSet = new AnimatorSet();
            resultSet.playSequentially(hideSet, showSet);
            resultSet.start();
        }
    }

    private void addAnimatorTogetherInSet(AnimatorSet set, Animator... animators) {
        List<Animator> animatorList = new ArrayList<>(animators.length);
        for (Animator animator : animators) {
            if (animator != null) {
                animatorList.add(animator);
            }
        }
        set.playTogether(animatorList);
    }

    private void startZoomTransition() {

        // TODO: 03.03.2017 fixme for landscape orientation
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            return;

        TransitionSet set = new TransitionSet();
        Transition explode = new Explode();

        final Rect rect = new Rect(binding.productImage.getLeft(), binding.productImage.getTop(),
                binding.productImage.getRight(), binding.productImage.getBottom());

        explode.setEpicenterCallback(new Transition.EpicenterCallback() {
            @Override
            public Rect onGetEpicenter(Transition transition) {
                return rect;
            }
        });

        explode.setPropagation(new SidePropagation());

        Transition changeBounds = new ChangeBounds();
        Transition imageImageTransform = new ChangeImageTransform();

        set.addTransition(explode)
                .addTransition(changeBounds)
                .addTransition(imageImageTransform)
                .setDuration(ANIMATION_DURATION)
                .setInterpolator(new FastOutSlowInInterpolator());

        TransitionManager.beginDelayedTransition(binding.productCard, set);

        if (childList == null) {
            childList = UIUtils.getChildrenExcludeView(binding.productWrap, R.id.product_image);
        }

        LinearLayout.LayoutParams imageParams;
        if (!isZoomed) {
            imageParams = new LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT);
            binding.productImage.setScaleType(FIT_CENTER);
        } else {
            imageParams = new LinearLayout.LayoutParams(MATCH_PARENT, 220 * density);
            binding.productImage.setScaleType(CENTER_CROP);
        }
        binding.productImage.setLayoutParams(imageParams);

        if (!isZoomed) {
            for (View view : childList) {
                view.setVisibility(GONE);
            }
        } else {
            for (View view : childList) {
                view.setVisibility(VISIBLE);
            }
        }
        isZoomed = !isZoomed;
    }
}
