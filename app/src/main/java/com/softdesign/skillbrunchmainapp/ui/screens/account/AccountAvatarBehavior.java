package com.softdesign.skillbrunchmainapp.ui.screens.account;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;

import com.softdesign.skillbrunchmainapp.R;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class AccountAvatarBehavior extends CoordinatorLayout.Behavior<View> {

    private int avatarSize;

    public AccountAvatarBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        avatarSize = context.getResources().getDimensionPixelOffset(R.dimen.account_avatar_size);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof AppBarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        AppBarLayout appBarLayout = (AppBarLayout) dependency;

        float maxAppbarHeight = dependency.getHeight();

        float currentAppBarHeight = appBarLayout.getBottom();
        float expandedPercentageFactor = currentAppBarHeight / maxAppbarHeight;

        float translation = dependency.getBottom() - avatarSize / 2;

        child.setTranslationY(translation);
        child.setScaleX(expandedPercentageFactor);
        child.setScaleY(expandedPercentageFactor);

        return super.onDependentViewChanged(parent, child, dependency);
    }
}
