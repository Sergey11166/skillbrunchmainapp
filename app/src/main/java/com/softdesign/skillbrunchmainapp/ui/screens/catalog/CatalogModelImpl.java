package com.softdesign.skillbrunchmainapp.ui.screens.catalog;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductCommentRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.jobs.SendMessageJob;
import com.softdesign.skillbrunchmainapp.mvp.AbsModel;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.softdesign.skillbrunchmainapp.BuildConfig.FLAVOR;

/**
 * @author Sergey Vorobyev.
 */
class CatalogModelImpl extends AbsModel implements CatalogModel {

    @Override
    public Observable<ProductRealm> getProducts() {
        Observable<ProductRealm> disk = fromDisk();
        Observable<ProductRealm> network = fromNetwork();
        return Observable
                .mergeDelayError(disk, network)
                .distinct(ProductRealm::getId);
    }

    @Override
    public void incrementProductCount(ProductRealm product) {
        dataManager.incrementProductCountInRealm(product);
    }

    @Override
    public void decrementProductCount(ProductRealm product) {
        dataManager.decrementProductCountInRealm(product);
    }

    @Override
    public void changeFavoriteStatus(ProductRealm product) {
        dataManager.changeFavoriteStatusInRealm(product);
    }

    private Observable<ProductRealm> fromNetwork() {
        return dataManager.getProductsFromServer()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<ProductRealm> fromDisk() {
        return dataManager.getProductsFromRealm();
    }

    @Override
    public boolean isAuthenticated() {
        return dataManager.isAuthenticate();
    }

    @Override
    public int getCartCount() {
        return dataManager.getCartCount();
    }

    @Override
    public void addToCart(ProductRealm product) {
        dataManager.addToCart(product);
    }

    @Override
    public void addProductComment(ProductRealm product, ProductCommentRealm comment) {
        switch (FLAVOR) {
            case "devBase":
            case "prodBase":
                SendMessageJob job = new SendMessageJob(comment, product.getId());
                jobManager.addJobInBackground(job);
                break;
            case "devRealmMp":
            case "prodRealmMp":
                dataManager.saveProductCommentToRealm(product, comment);
        }
    }
}
