package com.softdesign.skillbrunchmainapp.ui.screens.root;

import android.support.annotation.IdRes;
import android.support.annotation.Nullable;

import com.softdesign.skillbrunchmainapp.mvp.BaseView;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.io.File;

/**
 * @author Sergey Vorobyev.
 */
public interface RootView extends BaseView {

    void showMessage(String message);

    @Nullable
    BaseView getCurrentScreen();

    void showProgress();

    void hideProgress();

    void setCheckedNavigationItem(@IdRes int item);

    boolean isAllPermissionsGranted(String[] permissions);

    void requestPermissions(String[] permissions, int requestCode);

    void startCameraForResult(File file, int requestCode);

    void startGalleryForResult(int requestCode);

    void startSettingForResult(int requestCode);

    void startLoginVk();

    void startLoginFb();

    void startLoginTwitter(TwitterAuthClient client);
}
