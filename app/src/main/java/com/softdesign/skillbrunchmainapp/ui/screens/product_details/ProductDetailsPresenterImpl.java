package com.softdesign.skillbrunchmainapp.ui.screens.product_details;

import android.os.Bundle;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsPresenterVM;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogModel;
import com.softdesign.skillbrunchmainapp.ui.screens.product_comment.ProductCommentScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.MenuItemHolder;
import com.softdesign.skillbrunchmainapp.ui.view_models.ProductViewModel;

import flow.Flow;
import io.realm.RealmChangeListener;
import mortar.MortarScope;

/**
 * @author Sergey Vorobyev
 */
class ProductDetailsPresenterImpl
        extends AbsPresenterVM<ProductDetailsViewImpl, CatalogModel, ProductViewModel>
        implements ProductDetailsPresenter<ProductDetailsViewImpl> {

    private RealmChangeListener<ProductRealm> productChangeListener;
    private ProductRealm product;

    ProductDetailsPresenterImpl(ProductRealm productRealm) {
        this.product = productRealm;
    }

    @Override
    public void onPlusButtonClick() {
        model.incrementProductCount(product);
    }

    @Override
    public void onMinusButtonClick() {
        if (product.getCount() > 1) {
            model.decrementProductCount(product);
        }
    }

    @Override
    public void onFavoriteClick() {
        model.changeFavoriteStatus(product);
    }

    @Override
    public void onAddCommentClick() {
        Flow.get(getView()).set(new ProductCommentScreen(product));
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        if (product.isManaged()) {
            productChangeListener = element -> {
                updateTotalPrice();
                if (getView() != null) {
                    getView().showComments(element.getComments());
                }
            };
            product.addChangeListener(productChangeListener);
        }
        initViewModel();
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        getView().showComments(product.getComments());
    }

    @Override
    protected void onExitScope() {
        super.onExitScope();
        subscriptions.unsubscribe();
        if (product.isManaged()) {
            product.removeChangeListener(productChangeListener);
        }
    }

    @Override
    protected void initActionBar() {
        rootPresenter.newActionBarBuilder()
                .setTitle(product.getProductName())
                .setBackArrow(true)
                .addAction(new MenuItemHolder(0, true, "cart count", item -> {
                    if (getRootView() != null) getRootView().showMessage("Go to the cart");
                }))
                .setTabs(getView().getViewPager())
                .build();
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((ProductDetailsScreen.Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    private void initViewModel() {
        updateTotalPrice();
        viewModel.setName(product.getProductName());
        viewModel.setDescription(product.getDescription());
        viewModel.setImage(product.getImageUrl());
        viewModel.setFavorite(product.isFavorite());
        viewModel.setRating(product.getRating());
    }

    private void updateTotalPrice() {
        viewModel.setPrice(String.valueOf(product.getCount() * product.getPrice()));
        viewModel.setCount(String.valueOf(product.getCount()));
    }
}
