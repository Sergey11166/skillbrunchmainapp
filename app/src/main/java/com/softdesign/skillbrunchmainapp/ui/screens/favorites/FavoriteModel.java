package com.softdesign.skillbrunchmainapp.ui.screens.favorites;

import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;

import rx.Observable;

/**
 * @author Sergey Vorobyev.
 */

public interface FavoriteModel {

    Observable<ProductRealm> getFavoriteProducts();

    void changeFavoriteStatus(ProductRealm product);

    void addToCart(ProductRealm product);

    boolean isAuthenticated();
}
