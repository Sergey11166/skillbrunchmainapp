package com.softdesign.skillbrunchmainapp.ui.dialogs;

import android.os.Bundle;

import com.softdesign.skillbrunchmainapp.R;

/**
 * @author Sergey Vorobyev
 */
public class RemoveConfirmDialog extends BaseConfirmDialog {

    public static RemoveConfirmDialog newInstance() {
        RemoveConfirmDialog dialog = new RemoveConfirmDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_DIALOG_MASSAGE, R.string.dialog_message_confirm_remove);
        dialog.setArguments(bundle);
        return dialog;
    }
}
