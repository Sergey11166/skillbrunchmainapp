package com.softdesign.skillbrunchmainapp.ui.screens.splash;

import android.view.View;

import com.softdesign.skillbrunchmainapp.mvp.BasePresenter;

/**
 * @author Sergey Vorobyev
 */
interface SplashPresenter<V extends View> extends BasePresenter<V> {
}
