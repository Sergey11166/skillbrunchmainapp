package com.softdesign.skillbrunchmainapp.ui.screens.splash;

import rx.Observable;

/**
 * @author Sergey Vorobyev
 */
interface SplashModel {

    Observable<?> loadProducts();

    void startUpdateProductsWithTimer();
}
