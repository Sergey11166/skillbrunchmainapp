package com.softdesign.skillbrunchmainapp.ui.view_models;

import android.databinding.Bindable;

import com.softdesign.skillbrunchmainapp.BR;

/**
 * @author Sergey Vorobyev.
 */
@SuppressWarnings("unused")
public class AddressViewModel extends AbsViewModel {

    private String place;
    private String fullAddress;
    private String comment;
    private int commentVisibility;

    @Bindable
    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
        notifyPropertyChanged(BR.place);
    }

    @Bindable
    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
        notifyPropertyChanged(BR.fullAddress);
    }

    @Bindable
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
        notifyPropertyChanged(BR.comment);
    }

    @Bindable
    public int getCommentVisibility() {
        return commentVisibility;
    }

    public void setCommentVisibility(int commentVisibility) {
        this.commentVisibility = commentVisibility;
        notifyPropertyChanged(BR.commentVisibility);
    }
}
