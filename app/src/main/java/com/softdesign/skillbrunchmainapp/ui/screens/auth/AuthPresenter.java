package com.softdesign.skillbrunchmainapp.ui.screens.auth;

import android.view.View;

import com.softdesign.skillbrunchmainapp.mvp.BasePresenter;

/**
 * @author Sergey Vorobyev.
 */
interface AuthPresenter<V extends View> extends BasePresenter<V> {

    void onEmailChanged();

    void onPasswordChanged();

    void onLoginButtonClick();

    void onShowCatalogButtonClick();

    void onFbButtonClick();

    void onVkButtonClick();

    void onTwitterButtonClick();

    boolean onBackPressed();
}
