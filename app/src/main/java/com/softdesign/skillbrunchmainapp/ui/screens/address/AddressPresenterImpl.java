package com.softdesign.skillbrunchmainapp.ui.screens.address;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.AddressRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsPresenterVM;
import com.softdesign.skillbrunchmainapp.ui.screens.account.AccountModel;
import com.softdesign.skillbrunchmainapp.ui.view_models.AddressViewModel;

import flow.Flow;
import mortar.MortarScope;

/**
 * @author Sergey Vorobyev
 */
class AddressPresenterImpl extends AbsPresenterVM<AddressViewImpl, AccountModel, AddressViewModel>
        implements AddressPresenter<AddressViewImpl> {

    private UserInfoRealm userInfo;
    private int editPosition = -1;

    AddressPresenterImpl(UserInfoRealm userInfo, int editPosition) {
        if (editPosition < -1) {
            throw new IllegalArgumentException("editedPosition must be more or equals -1");
        }
        this.userInfo = userInfo;
        this.editPosition = editPosition;
    }

    @Override
    public void onSaveButtonClick() {
        if (editPosition == -1) {
            AddressRealm newAddress = new AddressRealm(viewModel.getPlace(), viewModel.getFullAddress(),
                    viewModel.getComment());
            model.insertAddress(userInfo, newAddress);
        } else {
            model.updateAddress(userInfo, editPosition, new AddressRealm(viewModel));
        }
        Flow.get(getView()).goBack();
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        if (editPosition != -1) {
            AddressRealm address = userInfo.getAddresses().get(editPosition);
            viewModel.setPlace(address.getPlace());
            viewModel.setFullAddress(address.getFullAddress());
            viewModel.setComment(address.getComment());
        }
    }

    @Override
    protected void initActionBar() {
        rootPresenter.newActionBarBuilder()
                .setTitle(editPosition != -1 ?
                        R.string.address_toolbar_title_edit :
                        R.string.address_toolbar_title_new)
                .setBackArrow(true)
                .build();
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((AddressScreen.Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }
}
