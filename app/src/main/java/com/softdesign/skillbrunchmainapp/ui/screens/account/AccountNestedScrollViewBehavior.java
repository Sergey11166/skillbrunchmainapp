package com.softdesign.skillbrunchmainapp.ui.screens.account;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.softdesign.skillbrunchmainapp.R;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public class AccountNestedScrollViewBehavior extends AppBarLayout.ScrollingViewBehavior {

    private int avatarSize;

    public AccountNestedScrollViewBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        avatarSize = context.getResources().getDimensionPixelOffset(R.dimen.account_avatar_size);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof RelativeLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        float translation = dependency.getTranslationY() + avatarSize / 2 +
                avatarSize * dependency.getScaleX() / 2;
        child.setTranslationY(translation);
        return super.onDependentViewChanged(parent, child, dependency);
    }
}
