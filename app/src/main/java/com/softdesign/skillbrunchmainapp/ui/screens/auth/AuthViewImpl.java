package com.softdesign.skillbrunchmainapp.ui.screens.auth;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.CycleInterpolator;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.databinding.ScreenAuthBinding;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsView;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootView;
import com.softdesign.skillbrunchmainapp.ui.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.ui.view_models.AuthViewModel;
import com.softdesign.skillbrunchmainapp.utils.SimpleTextWatcher;
import com.softdesign.skillbrunchmainapp.utils.UIUtils;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.animation.AnimatorInflater.loadAnimator;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.LOLLIPOP;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static com.transitionseverywhere.TransitionSet.ORDERING_SEQUENTIAL;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * @author Sergey Vorobyev.
 */
public class AuthViewImpl extends AbsView<AuthPresenter> implements AuthView, View.OnClickListener {

    private static final int ANIMATION_DURATION = 300;

    private Transition bounds;
    private Transition fade;

    private ScreenAuthBinding binding;

    private int density;
    private Subscription animObs;
    private Animator scaleAnimator;

    public AuthViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
        density = (int) UIUtils.getDensity(context);
        bounds = new ChangeBounds();
        fade = new Fade();
    }

    @Override
    public void showMessageLoggedIn() {
        RootView rootView = getRootMvpView();
        if (rootView != null) {
            rootView.showMessage(getContext().getString(R.string.auth_snack_logged_in));
        }
    }

    @Override
    public void showMessageLoginError() {
        RootView rootView = getRootMvpView();
        if (rootView != null) {
            rootView.showMessage(getContext().getString(R.string.error_login));
        }
    }

    @Override
    public void bindViewModel(@NonNull AbsViewModel viewModel) {
        binding.setAuthModel((AuthViewModel) viewModel);
    }

    @Override
    public boolean onViewBackPressed() {
        return presenter.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:
                presenter.onLoginButtonClick();
                break;
            case R.id.show_catalog_btn:
                presenter.onShowCatalogButtonClick();
                break;
            case R.id.fb_btn:
                animateOnClickSocialBtn(binding.fbBtn);
                presenter.onFbButtonClick();
                break;
            case R.id.vk_btn:
                animateOnClickSocialBtn(binding.vkBtn);
                presenter.onVkButtonClick();
                break;
            case R.id.twitter_btn:
                animateOnClickSocialBtn(binding.twitterBtn);
                presenter.onTwitterButtonClick();
                break;
        }
    }

    @Override
    public void slideAnimateHideShowInputs(boolean isShow) {

        CardView.LayoutParams params = (LayoutParams) binding.authCard.getLayoutParams();
        int startHeight = params.height;
        if (isShow && startHeight == MATCH_PARENT) {
            startHeight = binding.showCatalogBtn.getHeight();
        } else if (!isShow && startHeight == binding.showCatalogBtn.getHeight()) {
            startHeight = MATCH_PARENT;
        }
        params.height = startHeight;
        binding.authCard.setLayoutParams(params);

        beginDelayedTransition(isShow);
        params.height = isShow ? MATCH_PARENT : binding.showCatalogBtn.getHeight();
        binding.authCard.setLayoutParams(params);
        binding.authCard.setCardElevation(isShow ? 4 * density : 0);
        if (isShow && !binding.loginEmailEt.hasFocus() && !binding.loginPasswordEt.hasFocus()) {
            binding.loginEmailEt.requestFocus();
        }
    }

    @Override
    public void animateHideShowCatalogButton(boolean isShow) {
        animateHideShowView(binding.showCatalogBtn, isShow);
    }

    @Override
    public void alphaAnimateHideShowInputs(boolean isShow) {
        animateHideShowView(binding.authCard, isShow);
    }

    @Override
    public void animateHideShowSocialButtons(boolean isShow) {
        animateHideShowView(binding.socialWrap, isShow);
    }

    @Override
    public void animateHideShowProgress(boolean isShow) {
        animateHideShowView(binding.progress, isShow);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            binding = DataBindingUtil.bind(this);
            initWidgets();
            animateLogo();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (animObs != null) {
            animObs.unsubscribe();
        }
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<AuthScreen.Component>getComponent(context).inject(this);
    }

    private void initWidgets() {
        binding.fbBtn.setOnClickListener(this);
        binding.vkBtn.setOnClickListener(this);
        binding.twitterBtn.setOnClickListener(this);
        binding.loginBtn.setOnClickListener(this);
        binding.showCatalogBtn.setOnClickListener(this);
        binding.loginEmailEt.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                presenter.onEmailChanged();
            }
        });
        binding.loginPasswordEt.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                presenter.onPasswordChanged();
            }
        });
    }

    private void animateHideShowView(View view, boolean isShow) {
        view.setAlpha(isShow ? 0 : 1);
        view.animate()
                .setDuration(ANIMATION_DURATION)
                .alpha(isShow ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        view.setVisibility(VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (!isShow) {
                            view.setAlpha(1);
                            view.setVisibility(INVISIBLE);
                        }
                    }
                });
    }

    private void animateOnClickSocialBtn(View button) {
        button.animate()
                .translationX(10)
                .setDuration(ANIMATION_DURATION)
                .setInterpolator(new CycleInterpolator(5));
    }

    private void beginDelayedTransition(boolean isShow) {
        TransitionSet set = new TransitionSet();

        set.addTransition(isShow ? fade : bounds)
                .addTransition(isShow ? bounds : fade)
                .setDuration(ANIMATION_DURATION)
                .setInterpolator(new FastOutSlowInInterpolator())
                .setOrdering(ORDERING_SEQUENTIAL);

        TransitionManager.beginDelayedTransition(binding.panelWrapper, set);
    }

    private void animateLogo() {
        if (SDK_INT >= LOLLIPOP) {
            AnimatedVectorDrawable avd = (AnimatedVectorDrawable) binding.logoLayout.logoImg.getDrawable();
            scaleAnimator = loadAnimator(getContext(), R.animator.logo_scale_animator);
            scaleAnimator.setTarget(binding.logoLayout.logoImg);

            avd.start();
            scaleAnimator.start();

            animObs = Observable.interval(6000, MILLISECONDS)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aLong -> {
                        avd.start();
                        scaleAnimator.start();
                    });
        }
    }
}
