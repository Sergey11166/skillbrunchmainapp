package com.softdesign.skillbrunchmainapp.ui.screens.catalog;

import android.content.Context;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ActivityResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.di.scopes.DaggerScope;
import com.softdesign.skillbrunchmainapp.flow.AbsScreen;
import com.softdesign.skillbrunchmainapp.flow.Screen;
import com.softdesign.skillbrunchmainapp.ui.screens.product.ProductScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootPresenter;

import dagger.Provides;
import mortar.MortarScope;
import rx.subjects.PublishSubject;

import static com.softdesign.skillbrunchmainapp.di.DaggerService.getComponent;
import static java.lang.String.format;
import static java.util.Locale.ENGLISH;

/**
 * @author Sergey Vorobyev.
 */
@Screen(R.layout.screen_catalog)
public class CatalogScreen extends AbsScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerCatalogScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    @DaggerScope(CatalogScreen.class)
    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    public interface Component {
        void inject(CatalogPresenterImpl presenter);

        void inject(CatalogViewImpl view);

        RootPresenter getRootPresenter();

        CatalogModel getCatalogModel();

        PublishSubject<ActivityResultDto> getOnActivityResultSubject();
    }

    @dagger.Module
    public static class Module {
        @Provides
        @DaggerScope(CatalogScreen.class)
        CatalogModel provideModel() {
            return new CatalogModelImpl();
        }

        @Provides
        @DaggerScope(CatalogScreen.class)
        CatalogPresenter providePresenter() {
            return new CatalogPresenterImpl();
        }
    }

    public static class Factory {
        static Context createProductContext(ProductRealm product, Context parentContext) {
            MortarScope parentScope = MortarScope.getScope(parentContext);
            ProductScreen screen = new ProductScreen(product);
            String scopeName = format(ENGLISH, "%s_%s", screen.getScopeName(), product.getId());

            MortarScope childScope = parentScope.findChild(scopeName);
            if (childScope == null) {
                childScope = parentScope.buildChild()
                        .withService(DaggerService.SERVICE_NAME,
                                screen.createScreenComponent(getComponent(parentContext)))
                        .build(scopeName);
            }
            return childScope.createContext(parentContext);
        }
    }
}
