package com.softdesign.skillbrunchmainapp.ui.screens.splash;

import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogScreen;
import com.softdesign.skillbrunchmainapp.utils.L;

import flow.Flow;
import mortar.MortarScope;
import rx.Subscriber;

import static flow.Direction.REPLACE;

/**
 * @author Sergey Vorobyev
 */

class SplashPresenterImpl extends AbsPresenter<SplashViewImpl, SplashModel>
        implements SplashPresenter<SplashViewImpl> {

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        L.d("start load products");
        model.loadProducts().subscribe(new Subscriber<Object>() {
            @Override
            public void onCompleted() {
                L.d("load products success");
                model.startUpdateProductsWithTimer();
                Flow.get(getView()).replaceTop(new CatalogScreen(), REPLACE);
            }

            @Override
            public void onError(Throwable e) {
                L.e("load products error", e);
            }

            @Override
            public void onNext(Object o) {
                // do nothing
            }
        });
    }

    @Override
    protected void initActionBar() {
        rootPresenter.newActionBarBuilder()
                .setBackArrow(true)
                .setVisibility(false)
                .build();
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((SplashScreen.Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }
}
