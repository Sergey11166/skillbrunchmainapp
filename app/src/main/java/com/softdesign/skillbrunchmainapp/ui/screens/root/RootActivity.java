package com.softdesign.skillbrunchmainapp.ui.screens.root;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.facebook.login.LoginManager;
import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.dto.ActivityResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.dto.PermissionsResultDto;
import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;
import com.softdesign.skillbrunchmainapp.databinding.ActivityRootBinding;
import com.softdesign.skillbrunchmainapp.databinding.DrawerHeaderBinding;
import com.softdesign.skillbrunchmainapp.databinding.ToolbarMenuItemCartBinding;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.di.components.AppComponent;
import com.softdesign.skillbrunchmainapp.di.modules.RootModule;
import com.softdesign.skillbrunchmainapp.di.scopes.DaggerScope;
import com.softdesign.skillbrunchmainapp.flow.AbsScreen;
import com.softdesign.skillbrunchmainapp.flow.TreeKeyDispatcher;
import com.softdesign.skillbrunchmainapp.mvp.BaseView;
import com.softdesign.skillbrunchmainapp.ui.activities.BaseActivity;
import com.softdesign.skillbrunchmainapp.ui.dialogs.ExitConfirmDialog;
import com.softdesign.skillbrunchmainapp.ui.screens.account.AccountScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.catalog.CatalogScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.notifications.NotificationsScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.splash.SplashScreen;
import com.softdesign.skillbrunchmainapp.ui.view_models.AbsViewModel;
import com.softdesign.skillbrunchmainapp.ui.view_models.RootViewModel;
import com.softdesign.skillbrunchmainapp.utils.L;
import com.softdesign.skillbrunchmainapp.utils.NavUtils;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import flow.Direction;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import rx.subjects.PublishSubject;

import static android.support.v4.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
import static android.support.v4.widget.DrawerLayout.LOCK_MODE_UNLOCKED;
import static android.view.MenuItem.SHOW_AS_ACTION_ALWAYS;
import static android.view.MenuItem.SHOW_AS_ACTION_IF_ROOM;
import static java.util.Collections.singletonList;
import static mortar.MortarScope.findChild;

/**
 * @author Sergey Vorobyev
 */
public class RootActivity extends BaseActivity
        implements RootView, OnNavigationItemSelectedListener, ActionBarView {

    @Inject
    RootPresenter presenter;

    private ActivityRootBinding binding;
    private DrawerHeaderBinding drawerHeaderBinding;
    private ToolbarMenuItemCartBinding menuItemBinding;

    private ActionBar actionBar;
    private ActionBarDrawerToggle toggle;
    private List<MenuItemHolder> menuItems;
    private ExitConfirmDialog exitConfirmDialog;

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new SplashScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(@NonNull String name) {
        MortarScope rootActivityScope = findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ?
                rootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        initBinding();
        initToolbar();
        handlePush();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initDrawer();
        DaggerService.<RootComponent>getComponent(this).inject(this);
        presenter.takeView(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (!isDrawerOpened()) {
                openDrawer();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        presenter.dropView();
        super.onDestroy();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        AbsScreen key = null;
        switch (id) {
            case R.id.nav_account:
                key = new AccountScreen();
                break;
            case R.id.nav_catalog:
                key = new CatalogScreen();
                break;
            case R.id.nav_favorites:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notifications:
                key = new NotificationsScreen();
                break;
        }

        if (key != null) {
            Flow.get(this).replaceTop(key, Direction.REPLACE);
        }
        closeDrawer();
        return false;
    }

    @Override
    public void onBackPressed() {
        if (isDrawerOpened()) {
            closeDrawer();
        } else if (getCurrentScreen() != null && !getCurrentScreen().onViewBackPressed() && !Flow.get(this).goBack()) {
            showExitConfirmDialog();
        }
    }

    private void initToolbar() {
        setSupportActionBar(binding.toolbar);
        actionBar = getSupportActionBar();
        toggle = new ActionBarDrawerToggle(this, binding.drawer,
                binding.toolbar, R.string.drawer_open, R.string.drawer_close);
        binding.drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    public void openDrawer() {
        binding.drawer.openDrawer(GravityCompat.START);
    }

    public void closeDrawer() {
        binding.drawer.closeDrawer(GravityCompat.START);
    }

    public NavigationView getNavigationView() {
        return binding.navView;
    }

    public void showExitConfirmDialog() {
        if (exitConfirmDialog == null) initExitConfirmDialog();
        exitConfirmDialog.show(getSupportFragmentManager(), ExitConfirmDialog.class.getName());
    }

    public boolean isDrawerOpened() {
        return binding.drawer.isDrawerOpen(GravityCompat.START);
    }

    @Nullable
    @Override
    public BaseView getCurrentScreen() {
        return (BaseView) binding.container.getChildAt(0);
    }

    @Override
    public void setCheckedNavigationItem(@MenuRes int item) {
        getNavigationView().setCheckedItem(item);
    }

    @Override
    public boolean isAllPermissionsGranted(String[] permissions) {
        boolean allGranted = true;

        for (String permission : permissions) {
            int selfPermission = ContextCompat.checkSelfPermission(this, permission);
            if (selfPermission != PackageManager.PERMISSION_GRANTED) {
                allGranted = false;
                break;
            }
        }
        return allGranted;
    }

    @Override
    public void startCameraForResult(File file, int requestCode) {
        NavUtils.goToCameraApp(this, file, requestCode);
    }

    @Override
    public void startGalleryForResult(int requestCode) {
        NavUtils.goToGalleryApp(this, requestCode);
    }

    @Override
    public void startSettingForResult(int requestCode) {
        NavUtils.goToSettingsApp(this, requestCode);
    }

    @Override
    public void startLoginVk() {
        VKSdk.login(this, VKScope.EMAIL, VKScope.PHOTOS);
    }

    @Override
    public void startLoginFb() {
        LoginManager.getInstance().logInWithReadPermissions(this, singletonList("public_profile"));
    }

    @Override
    public void startLoginTwitter(TwitterAuthClient client) {
        client.authorize(this, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                L.d("logged in as " + result.data.getUserName());
            }

            @Override
            public void failure(TwitterException e) {
                L.e(e);
                showMessage(e.getMessage());
            }
        });
    }

    @Override
    public void bindViewModel(@NonNull AbsViewModel viewModel) {
        RootViewModel rootViewModel = (RootViewModel) viewModel;
        binding.setRootModel(rootViewModel);
        menuItemBinding.setRootModel(rootViewModel);
        drawerHeaderBinding.setRootModel(rootViewModel);
    }

    @Override
    public void showMessage(String message) {
        showSnackBar(message);
    }

    @Override
    public boolean onViewBackPressed() {
        return false;
    }

    private void initDrawer() {
        binding.navView.setNavigationItemSelectedListener(this);
        binding.navView.setCheckedItem(R.id.nav_catalog);
    }

    private void initBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_root);
        menuItemBinding = ToolbarMenuItemCartBinding.inflate(getLayoutInflater(), null, false);
        drawerHeaderBinding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.drawer_header, binding.navView, false);
        binding.navView.addHeaderView(drawerHeaderBinding.getRoot());
    }

    private void initExitConfirmDialog() {
        exitConfirmDialog = ExitConfirmDialog.newInstance();
        exitConfirmDialog.setOnPositiveButtonClickListener((d, w) -> finish());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void setActionBarTitle(CharSequence title) {
        actionBar.setTitle(title);
    }

    @Override
    public void setActionBarTitle(int resId) {
        actionBar.setTitle(resId);
    }

    @Override
    public void setActionBarVisibility(boolean isVisible) {
        if (isVisible) {
            actionBar.show();
        } else {
            actionBar.hide();
        }
        binding.drawer.setDrawerLockMode(!isVisible ? LOCK_MODE_LOCKED_CLOSED : LOCK_MODE_UNLOCKED);
        toggle.syncState();
    }

    @Override
    public void setBackArrow(boolean enable) {
        if (toggle != null && actionBar != null) {
            binding.drawer.setDrawerLockMode(enable ? LOCK_MODE_LOCKED_CLOSED : LOCK_MODE_UNLOCKED);
            toggle.setDrawerIndicatorEnabled(!enable);
            actionBar.setDisplayHomeAsUpEnabled(enable);
            toggle.setToolbarNavigationClickListener(enable ? v -> onBackPressed() : null);
            toggle.syncState();
        }
    }

    @Override
    public void setMenuItems(List<MenuItemHolder> items) {
        menuItems = items;
        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menuItems != null && !menuItems.isEmpty()) {
            for (MenuItemHolder itemHolder : menuItems) {
                if (!itemHolder.hasCustomView()) {
                    MenuItem menuItem = menu.add(itemHolder.getTitle());
                    menuItem.setShowAsActionFlags(SHOW_AS_ACTION_IF_ROOM)
                            .setIcon(itemHolder.getIconResId())
                            .setOnMenuItemClickListener(item -> {
                                itemHolder.getListener().onClick(getRootView());
                                return true;
                            });
                } else {
                    getMenuInflater().inflate(R.menu.menu_toolbar, menu);
                    menu.findItem(R.id.cart)
                            .setShowAsActionFlags(SHOW_AS_ACTION_ALWAYS)
                            .setActionView(menuItemBinding.getRoot())
                            .getActionView().setOnClickListener(itemHolder.getListener());
                }
            }
        } else {
            menu.clear();
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTabLayout(ViewPager viewPager) {
        TabLayout tabLayout = new TabLayout(this);
        tabLayout.setupWithViewPager(viewPager);
        binding.appbar.addView(tabLayout);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    @Override
    public void removeTabLayout() {
        View tabLayout = binding.appbar.getChildAt(1);
        if (tabLayout != null && tabLayout instanceof TabLayout) {
            binding.appbar.removeView(tabLayout);
        }
    }

    private void handlePush() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String id = bundle.getString("google.message_id");
            if (TextUtils.isEmpty(id)) {
                return;
            }
            Date date = new Date(bundle.getLong("google.sent_time"));
            String title = bundle.getString("title");
            String content = bundle.getString("content");
            String meta = bundle.getString("meta");
            String type = bundle.getString("type");

            NotificationRealm notification = new NotificationRealm(id, type, meta, title, content, date);
            presenter.onStartFromNotification(notification);
        }
    }

    @DaggerScope(RootActivity.class)
    @dagger.Component(dependencies = AppComponent.class, modules = RootModule.class)
    public interface RootComponent {
        void inject(RootActivity activity);

        void inject(RootPresenterImpl presenter);

        RootPresenter getRootPresenter();

        PublishSubject<ActivityResultDto> getOnActivityResultSubject();

        PublishSubject<PermissionsResultDto> getOnPermissionsResultSubject();
    }
}
