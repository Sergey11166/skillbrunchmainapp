package com.softdesign.skillbrunchmainapp.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.softdesign.skillbrunchmainapp.R;

/**
 * @author Sergey Vorobyev.
 */
public class ChangeImageDialog extends BaseConfirmDialog {

    private OnClickListener onItemClickListener;

    public static ChangeImageDialog newInstance(OnClickListener onItemClickListener) {
        ChangeImageDialog dialog = new ChangeImageDialog();
        dialog.onItemClickListener = onItemClickListener;
        dialog.setRetainInstance(true);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String[] items = getResources().getStringArray(R.array.load_photo_dialog_items);
        builder.setItems(items, onItemClickListener);

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }
}
