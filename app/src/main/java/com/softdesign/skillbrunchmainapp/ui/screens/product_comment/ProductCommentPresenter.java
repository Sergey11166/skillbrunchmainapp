package com.softdesign.skillbrunchmainapp.ui.screens.product_comment;

import android.view.View;

import com.softdesign.skillbrunchmainapp.mvp.BasePresenter;

/**
 * @author Sergey Vorobyev
 */
interface ProductCommentPresenter<V extends View> extends BasePresenter<V> {

    void onSaveButtonClick();
}
