package com.softdesign.skillbrunchmainapp.ui.screens.account;

import com.softdesign.skillbrunchmainapp.data.storage.realm.AddressRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;

import rx.Observable;

/**
 * @author Sergey Vorobyev.
 */
public interface AccountModel {

    Observable<UserInfoRealm> getUserInfo();

    void uploadAvatar(String avatarUri);

    void updateUserInfo(UserInfoRealm to, UserInfoRealm from, boolean isAvatarChanged);

    void removeAddress(UserInfoRealm userInfo, AddressRealm address);

    void insertAddress(UserInfoRealm userInfo, AddressRealm address);

    void updateAddress(UserInfoRealm userInfo, int addressPosition, AddressRealm address);
}