package com.softdesign.skillbrunchmainapp.ui.view_models;

import android.databinding.Bindable;

import com.softdesign.skillbrunchmainapp.BR;

/**
 * @author Sergey Vorobyev
 */
public class NotificationViewModel extends AbsViewModel {

    private String title;
    private String content;
    private String icon;
    private String date;
    private String actionButtonText;

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
        notifyPropertyChanged(BR.content);
    }

    @Bindable
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
        notifyPropertyChanged(BR.icon);
    }

    @Bindable
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
        notifyPropertyChanged(BR.date);
    }

    @Bindable
    public String getActionButtonText() {
        return actionButtonText;
    }

    public void setActionButtonText(String actionButtonText) {
        this.actionButtonText = actionButtonText;
        notifyPropertyChanged(BR.actionButtonText);
    }
}
