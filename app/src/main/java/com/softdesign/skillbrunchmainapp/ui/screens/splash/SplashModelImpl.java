package com.softdesign.skillbrunchmainapp.ui.screens.splash;

import com.softdesign.skillbrunchmainapp.mvp.AbsModel;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Sergey Vorobyev
 */
class SplashModelImpl extends AbsModel implements SplashModel {

    @Override
    public Observable loadProducts() {
        return dataManager.getProductsFromServer()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void startUpdateProductsWithTimer() {
        dataManager.startUpdateProductsWithTimer();
    }
}
