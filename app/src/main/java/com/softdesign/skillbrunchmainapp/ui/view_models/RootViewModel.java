package com.softdesign.skillbrunchmainapp.ui.view_models;

import android.databinding.Bindable;

import com.softdesign.skillbrunchmainapp.BR;

/**
 * @author Sergey Vorobyev.
 */
@SuppressWarnings("unused")
public class RootViewModel extends AbsViewModel {

    private String avatarUri;
    private String username;
    private String cartCount;

    @Bindable
    public String getAvatarUri() {
        return avatarUri;
    }

    public void setAvatarUri(String avatarUri) {
        this.avatarUri = avatarUri;
        notifyPropertyChanged(BR.avatarUri);
    }

    @Bindable
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
        notifyPropertyChanged(BR.username);
    }

    @Bindable
    public String getCartCount() {
        return cartCount;
    }

    public void setCartCount(String cartCount) {
        this.cartCount = cartCount;
        notifyPropertyChanged(BR.cartCount);
    }
}
