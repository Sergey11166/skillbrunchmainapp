package com.softdesign.skillbrunchmainapp.ui.screens.account;

import com.softdesign.skillbrunchmainapp.data.storage.realm.AddressRealm;
import com.softdesign.skillbrunchmainapp.data.storage.realm.UserInfoRealm;
import com.softdesign.skillbrunchmainapp.jobs.UploadAvatarJob;
import com.softdesign.skillbrunchmainapp.mvp.AbsModel;

import rx.Observable;

/**
 * @author Sergey Vorobyev.
 */
class AccountModelImpl extends AbsModel implements AccountModel {

    @Override
    public Observable<UserInfoRealm> getUserInfo() {
        return dataManager.getUserInfoFromRealm();
    }

    @Override
    public void uploadAvatar(String uri) {
        jobManager.addJobInBackground(new UploadAvatarJob(uri));
    }

    @Override
    public void updateUserInfo(UserInfoRealm to, UserInfoRealm from, boolean isAvatarChanged) {
        dataManager.updateUserInfoInRealm(to, from, isAvatarChanged);
        if (isAvatarChanged) {
            uploadAvatar(from.getAvatarUrl());
        }
    }

    @Override
    public void removeAddress(UserInfoRealm userInfo, AddressRealm address) {
        dataManager.removeAddressFromRealm(userInfo, address);
    }

    @Override
    public void insertAddress(UserInfoRealm userInfo, AddressRealm address) {
        dataManager.insertAddressToRealm(userInfo, address);
    }

    @Override
    public void updateAddress(UserInfoRealm userInfo, int addressPosition, AddressRealm address) {
        dataManager.updateAddressInRealm(userInfo, addressPosition, address);
    }
}
