package com.softdesign.skillbrunchmainapp.ui.screens.notifications;

import android.os.Bundle;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.NotificationRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.notifications.NotificationsScreen.Component;
import com.softdesign.skillbrunchmainapp.ui.screens.root.MenuItemHolder;
import com.softdesign.skillbrunchmainapp.utils.L;

import mortar.MortarScope;
import rx.Subscriber;
import rx.Subscription;

/**
 * @author Sergey Vorobyev
 */
public class NotificationsPresenterImpl
        extends AbsPresenter<NotificationsViewImpl, NotificationsModel>
        implements NotificationsPresenter<NotificationsViewImpl> {

    private Subscription notificationSubs;

    @Override
    public void clickOnCancelButton(int position) {

    }

    @Override
    public void clickOnActionButton(int position) {

    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        notificationSubs = model.getNotifications().subscribe(new NotificationsSubscriber());
    }

    @Override
    public void dropView(NotificationsViewImpl view) {
        if (!notificationSubs.isUnsubscribed()) {
            notificationSubs.unsubscribe();
        }
        super.dropView(view);
    }

    @Override
    protected void initActionBar() {
        rootPresenter.newActionBarBuilder()
                .setTitle(R.string.menu_drawer_notifications)
                .setBackArrow(false)
                .addAction(new MenuItemHolder(0, true, "notification", view -> {
                    if (getRootView() != null) {
                        getRootView().showMessage("Go to the cart");
                    }
                }))
                .build();
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    private class NotificationsSubscriber extends Subscriber<NotificationRealm> {

        @Override
        public void onCompleted() {
            L.d("Notifications observable is completed");
        }

        @Override
        public void onError(Throwable e) {
            L.e(e);
        }

        @Override
        public void onNext(NotificationRealm notificationRealm) {
            if (getView() != null) {
                getView().addNotification(notificationRealm);
            }
        }
    }
}
