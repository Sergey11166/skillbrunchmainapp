package com.softdesign.skillbrunchmainapp.ui.screens.catalog;

import android.os.Bundle;

import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.storage.realm.ProductRealm;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.mvp.AbsPresenter;
import com.softdesign.skillbrunchmainapp.ui.screens.auth.AuthScreen;
import com.softdesign.skillbrunchmainapp.ui.screens.root.MenuItemHolder;
import com.softdesign.skillbrunchmainapp.utils.L;

import flow.Flow;
import mortar.MortarScope;
import rx.Subscriber;
import rx.Subscription;

/**
 * @author Sergey Vorobyev
 */
class CatalogPresenterImpl extends AbsPresenter<CatalogViewImpl, CatalogModel>
        implements CatalogPresenter<CatalogViewImpl> {

    private Subscription productsSubs;
    private int lastPagerPosition;

    @Override
    public void onBuyButtonClick(ProductRealm product) {
        if (!model.isAuthenticated() && getView() != null) {
            Flow.get(getView()).set(new AuthScreen());
        } else {
            model.addToCart(product);
            rootPresenter.setCartCount(model.getCartCount());
            getView().getCurrentProductView().animateAddToCart();
        }
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        rootPresenter.setCartCount(model.getCartCount());
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        subscribeOnProductsObs();
    }

    @Override
    public void dropView(CatalogViewImpl view) {
        lastPagerPosition = getView().getCurrentPagerPosition();
        productsSubs.unsubscribe();
        super.dropView(view);
    }

    @Override
    protected void initActionBar() {
        rootPresenter.newActionBarBuilder()
                .setTitle(R.string.menu_drawer_catalog)
                .setBackArrow(false)
                .addAction(new MenuItemHolder(0, true, "cart", view -> {
                    if (getRootView() != null) {
                        getRootView().showMessage("Go to the cart");
                    }
                }))
                .build();
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((CatalogScreen.Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    private void subscribeOnProductsObs() {
        if (getRootView() != null) {
            getRootView().showProgress();
        }
        productsSubs = model.getProducts().subscribe(new ProductsSubscriber());
    }

    private class ProductsSubscriber extends Subscriber<ProductRealm> {

        @Override
        public void onCompleted() {
            L.d("Product observable is completed");
        }

        @Override
        public void onError(Throwable e) {
            L.e(e);
            if (getRootView() != null) {
                getRootView().hideProgress();
                getRootView().showMessage(e.getMessage());
            }
        }

        @Override
        public void onNext(ProductRealm productRealm) {
            getView().addPage(productRealm);

            if (getRootView() != null) {
                getRootView().hideProgress();
            }

            if (getView().getPageCount() - 1 == lastPagerPosition) {
                getView().setPagerPosition(lastPagerPosition);
            }
        }
    }
}
