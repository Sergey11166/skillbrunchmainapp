package com.softdesign.skillbrunchmainapp.flow;

import com.softdesign.skillbrunchmainapp.mortar.ScreenScoper;
import com.softdesign.skillbrunchmainapp.utils.Constants;
import com.softdesign.skillbrunchmainapp.utils.L;

import flow.ClassKey;

/**
 * @author Sergey Vorobyev.
 */
public abstract class AbsScreen<T> extends ClassKey {

    private static final String TAG = Constants.LOG_TAG_PREFIX + "AbsScreen";

    public String getScopeName() {
        return getClass().getName();
    }

    public abstract Object createScreenComponent(T parentComponent);

    void unregisterScope() {
        L.d(TAG, "unregisterScope: " + getScopeName());
        ScreenScoper.destroyScreenScope(getScopeName());
    }
}
