package com.softdesign.skillbrunchmainapp;

import android.content.Intent;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.softdesign.skillbrunchmainapp.di.DaggerService;
import com.softdesign.skillbrunchmainapp.di.components.AppComponent;
import com.softdesign.skillbrunchmainapp.di.components.DaggerAppComponent;
import com.softdesign.skillbrunchmainapp.di.components.DaggerModelComponent;
import com.softdesign.skillbrunchmainapp.di.components.ModelComponent;
import com.softdesign.skillbrunchmainapp.di.modules.AppModule;
import com.softdesign.skillbrunchmainapp.di.modules.LocalModule;
import com.softdesign.skillbrunchmainapp.di.modules.ModelModule;
import com.softdesign.skillbrunchmainapp.di.modules.NetworkModule;
import com.softdesign.skillbrunchmainapp.di.modules.RootModule;
import com.softdesign.skillbrunchmainapp.mortar.ScreenScoper;
import com.softdesign.skillbrunchmainapp.services.PushService;
import com.softdesign.skillbrunchmainapp.ui.screens.root.DaggerRootActivity_RootComponent;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity.RootComponent;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.vk.sdk.VKSdk;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

/**
 * @author Sergey Vorobyev
 */
public class App extends MultiDexApplication {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "bgBMjfUSYsvAUWtzoSd5lzUh5";
    private static final String TWITTER_SECRET = "Pu3TqBOtbLDcpagD7vlkFnmFvSaTqTAjFTkBzKKDdNTo8hy0QK";

    private static App appInstance;
    private static AppComponent appComponent;
    private static ModelComponent modelComponent;
    private MortarScope rootScope;

    public static App get() {
        return appInstance;
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static ModelComponent getModelComponent() {
        return modelComponent;
    }

    private static ModelComponent createModelComponent() {
        return DaggerModelComponent.builder()
                .appComponent(getAppComponent())
                .modelModule(new ModelModule())
                .localModule(new LocalModule())
                .networkModule(new NetworkModule())
                .build();
    }

    @Override
    public Object getSystemService(String name) {
        return rootScope != null && rootScope.hasService(name) ?
                rootScope.getService(name) : super.getSystemService(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        appInstance = this;
        initLibraries();
        initDaggerComponents();
        startService(new Intent(this, PushService.class));
    }

    private void initLibraries() {
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        VKSdk.initialize(this);
        Realm.init(this);
        try {
            Picasso.setSingletonInstance(new Picasso.Builder(this)
                    .downloader(new OkHttp3Downloader(this))
                    .indicatorsEnabled(true)
                    .build());
        } catch (IllegalStateException e) {
            // ignore
            // to suppress java.lang.IllegalStateException: Singleton instance already exists
        }
    }

    private void initDaggerComponents() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        modelComponent = createModelComponent();

        RootComponent rootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(appComponent)
                .rootModule(new RootModule())
                .build();

        rootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, appComponent)
                .build("Root");

        MortarScope rootActivityScope = rootScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, rootComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                .build(RootActivity.class.getName());

        ScreenScoper.registerScope(rootScope);
        ScreenScoper.registerScope(rootActivityScope);
    }
}
