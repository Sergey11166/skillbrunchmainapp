package com.softdesign.skillbrunchmainapp;

/**
 * @author Sergey Vorobyev
 */
public class MockResponses {
    public static final String SUCCESS_LOGIN = "{" +
            "\"_id\" : \"58711631a242690011b1b26d\"," +
            "\"fullName\" : \"Вася\"," +
            "\"avatarUrl\" : \"http://anyUrl.ru/user/58711631a242690011b1b26d/avatar.jpg\"," +
            "\"token\" : \"weqfvw;edcnw'lkedm93847983yuhefoij321kml'kjvj30fewoidvn\"," +
            "\"phone\" : \"89179711111\"," +
            "\"address\" : " +
            "[" +
            "{" +
            "\"_id\" : \"58711631a242690011b1b26d\"," +
            "\"name\" : \"Работа\"," +
            "\"street\" : \"Автостроителей\"," +
            "\"house\" : \"24\"," +
            "\"apartment\" : \"1\"," +
            "\"floor\" : \"1\"," +
            "\"comment\" : \"loren\"" +
            "}" +
            "]" +
            "}";

    public static final String SUCCESS_UPLOAD_AVATAR = "{" +
            "\"avatarUrl\" : \"http://anyUrl.ru/user/58711631a242690011b1b26d/avatar.jpg\"" +
            "}";

    public static final String SUCCESS_UPLOAD_COMMENT = "{" +
            "\"_id\" : \"58712357a242690011b1b278\"," +
            "\"productId\" : \"584f93c0ed0d00001179ac89\"," +
            "\"userName\" : \"Сергей Воробьёв\"," +
            "\"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\"," +
            "\"raiting\": 3.1," +
            "\"commentDate\": \"2016-11-15T04:58:08.000Z\"," +
            "\"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \"," +
            "\"active\": true" +
            "}";

    public static final String SUCCESS_GET_PRODUCTS = "[\n" +
            "              {\n" +
            "                \"_id\": \"584f93c0ed0d00001179ac89\",\n" +
            "                \"productName\": \"test 1\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-01.jpg\",\n" +
            "                \"description\": \"description 1 description 1 description 1 description 1 description 1\",\n" +
            "                \"price\": 100,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": false,\n" +
            "                \"comments\": [\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  },\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b279\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  }\n" +
            "                ]\n" +
            "              },\n" +
            "              {\n" +
            "                \"_id\": \"584f93cfed0d00001179ac8a\",\n" +
            "                \"productName\": \"test 2\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-02.jpg\",\n" +
            "                \"description\": \"description 2 description 2 description 2 description 2 description 2\",\n" +
            "                \"price\": 200,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": true,\n" +
            "                \"comments\": [\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  },\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  },\n" +
            "                  {\n" +
            "                  \n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  }\n" +
            "                ]\n" +
            "              },\n" +
            "              {\n" +
            "                \"_id\": \"584f93e2ed0d00001179ac8b\",\n" +
            "                \"productName\": \"test 3\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-03.jpg\",\n" +
            "                \"description\": \"description 3 description 3 description 3 description 3 description 3\",\n" +
            "                \"price\": 300,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": true,\n" +
            "                \"comments\": [\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  }\n" +
            "                ]\n" +
            "              },\n" +
            "              {\n" +
            "                \"_id\": \"584f9409ed0d00001179ac8c\",\n" +
            "                \"productName\": \"test 4\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-04.jpg\",\n" +
            "                \"description\": \"description 4 description 4 description 4 description 4 description 4\",\n" +
            "                \"price\": 400,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": true,\n" +
            "                \"comments\": [\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  },\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  },\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": false\n" +
            "                  },\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  }\n" +
            "                ]\n" +
            "              },\n" +
            "              {\n" +
            "                \"_id\": \"584f9429ed0d00001179ac8d\",\n" +
            "                \"productName\": \"test 5\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-05.jpg\",\n" +
            "                \"description\": \"description 5 description 5 description 5 description 5 description 5\",\n" +
            "                \"price\": 100,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": true,\n" +
            "                \"comments\": [\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  },\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": false\n" +
            "                  }\n" +
            "                ]\n" +
            "              },\n" +
            "              {\n" +
            "                \"_id\": \"584f9437ed0d00001179ac8e\",\n" +
            "                \"productName\": \"test 6\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-06.jpg\",\n" +
            "                \"description\": \"description 6 description 6 description 6 description 6 description 6\",\n" +
            "                \"price\": 600,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": true,\n" +
            "                \"comments\": [\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  },\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": false\n" +
            "                  },\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  },\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  }\n" +
            "                ]\n" +
            "              },\n" +
            "              {\n" +
            "                \"_id\": \"584f9445ed0d00001179ac8f\",\n" +
            "                \"productName\": \"test 7\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-07.jpg\",\n" +
            "                \"description\": \"description 7 description 7 description 7 description 7 description 7\",\n" +
            "                \"price\": 600,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": true,\n" +
            "                \"comments\": []\n" +
            "              },\n" +
            "              {\n" +
            "                \"_id\": \"58712357a242690011b1b278\",\n" +
            "                \"productName\": \"test 8\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-08.jpg\",\n" +
            "                \"description\": \"description 8 description 8 description 8 description 8 description 8\",\n" +
            "                \"price\": 600,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": true,\n" +
            "                \"comments\": [\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": true\n" +
            "                  }\n" +
            "                ]\n" +
            "              },\n" +
            "              {\n" +
            "                \"_id\": \"584f944fed0d00001179ac90\",\n" +
            "                \"productName\": \"test 9\",\n" +
            "                \"imageUrl\": \"http://skill-branch.ru/img/app/product-08.jpg\",\n" +
            "                \"description\": \"description 8 description 8 description 8 description 8 description 8\",\n" +
            "                \"price\": 900,\n" +
            "                \"raiting\": 4.2,\n" +
            "                \"active\": false,\n" +
            "                \"comments\": [\n" +
            "                  {\n" +
            "                    \"_id\": \"58712357a242690011b1b278\",\n" +
            "                    \"avatar\": \"http://skill-branch.ru/img/app/avatar-1.png\",\n" +
            "                    \"raiting\": 3,\n" +
            "                    \"commentDate\": \"2016-11-15T04:58:08.000Z\",\n" +
            "                    \"comment\": \"Lorem ipsum dolor sit amet. Quas molestias excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Beatae vitae dicta sunt, explicabo ut enim. Excepturi sint, obcaecati cupiditate non numquam eius modi tempora incidunt. Id, quod maxime placeat, facere possimus. \",\n" +
            "                    \"active\": false\n" +
            "                  }\n" +
            "                ]\n" +
            "              }\n" +
            "            ]";

    public static final String SUCCESS_GET_VK_PROFILE = "{\n" +
            "  \"response\": [\n" +
            "    {\n" +
            "      \"uid\": 2247431,\n" +
            "      \"first_name\": \"Сергей\",\n" +
            "      \"last_name\": \"Воробьёв\",\n" +
            "      \"photo_200\": \"https://pp.userapi.com/c837221/v837221431/17632/zFJ-t57oWOc.jpg\",\n" +
            "      \"mobile_phone\": \"+79022589585\",\n" +
            "      \"home_phone\": \"\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    public static final String SUCCESS_GET_FB_PROFILE = "{\n" +
            "  \"picture\": {\n" +
            "    \"data\": {\n" +
            "      \"height\": 200,\n" +
            "      \"is_silhouette\": true,\n" +
            "      \"url\": \"https://scontent.xx.fbcdn.net/v/t1.0-1/c59.0.200.200/p200x200/10354686_10150004552801856_220367501106153455_n.jpg?oh=0dd039ae2c38043a23f9cbe444ff5167&oe=59840425\",\n" +
            "      \"width\": 200\n" +
            "    }\n" +
            "  },\n" +
            "  \"first_name\": \"Sergey\",\n" +
            "  \"last_name\": \"Vorobjev\",\n" +
            "  \"id\": \"1683965858285014\"\n" +
            "}";

    public static final String SUCCESS_FAVORITES = "[\n" +
            "  \"58711631a242690011b1b26d\",\n" +
            "  \"58711631a242690011b1b26e\",\n" +
            "  \"58711631a242690011b1b26f\"\n" +
            "]";

    public static final String SUCCESS_CART = "[\n" +
            "  {\n" +
            "    \"_id\": \"58711631a242690011b1b26e\",\n" +
            "    \"count\": 1\n" +
            "  },\n" +
            "  {\n" +
            "    \"_id\": \"58711631a242690011b1b26f\",\n" +
            "    \"count\": 2\n" +
            "  },\n" +
            "  {\n" +
            "    \"_id\": \"58711631a242690011b1b26d\",\n" +
            "    \"count\": 3\n" +
            "  }\n" +
            "]";
}
