package com.softdesign.skillbrunchmainapp.utils;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public interface Constants {

    String LOG_TAG_PREFIX = "Skill";

    String PATTERN_EMAIL = "^[\\w\\+\\.\\%\\-]{3,}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{1,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{1,25})+$";

    // onActivityResult request codes
    int REQUEST_CODE_CAMERA = 0;
    int REQUEST_CODE_GALLERY = 1;
    int REQUEST_CODE_SETTING_CAMERA = 4;
    int REQUEST_CODE_SETTING_GALLERY = 5;

    // Permissions requests
    int CAMERA_PERMISSION_REQUEST_CODE = 0;
    int GALLERY_PERMISSION_REQUEST_CODE = 1;

    // OkHttp client
    int MAX_CONNECT_TIMEOUT = 1000 * 5;
    int MAX_WRITE_TIMEOUT = 1000 * 20;
    int MAX_READ_TIMEOUT = 1000 * 20;

    // Http headers
    String AUTHORISATION = "Authorization";
    String LAST_MODIFIED_HEADER = "Last-Modified";
    String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";
    String DEFAULT_LAST_MODIFIED = "Thu, 01 Jan 1970 00:00:00 GMT";

    // Jobs
    int MAX_CONSUMER_COUNT = 3;
    int MIN_CONSUMER_COUNT = 1;
    int LOAD_FACTOR = 3;
    int CONSUMER_KEEP_ALIVE = 120;
    int INITIAL_BACK_OFF_IN_MS = 1000;
    int UPDATE_DATA_INTERVAL = 30;
    int RETRY_REQUEST_COUNT = 3;
    int RETRY_REQUEST_BASE_DELAY = 1000;

    // Social
    String VK_API_BASE_URL = "https://api.vk.com/method/users.get?fields=photo_200,contacts";
    String FB_API_BASE_URL = "https://graph.facebook.com/v2.9/me?fields=picture.width(200).height(200),first_name,last_name";

    // Notification
    String NOTIFICATION_ORDER_TYPE = "NOTIFICATION_ORDER_TYPE";
    String NOTIFICATION_OFFER_TYPE = "NOTIFICATION_OFFER_TYPE";
    int PUSH_REQUEST_CODE = 0;
    int NOTIFICATION_MANAGER_ID = 0;
}
