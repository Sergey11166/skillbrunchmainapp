package com.softdesign.skillbrunchmainapp.di.modules;

import android.content.Context;

import com.facebook.stetho.Stetho;
import com.softdesign.skillbrunchmainapp.data.managers.RealmManager;
import com.softdesign.skillbrunchmainapp.utils.L;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Sergey Vorobyev
 */
@Module
class FlavorLocalModule {

    @Provides
    @Singleton
    RealmManager provideRealmManager(Context context) {
        L.d("BASE", "provide RealmManager init: ");
        Stetho.initialize(Stetho.newInitializerBuilder(context)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(context))
                .enableWebKitInspector(RealmInspectorModulesProvider.builder(context).build())
                .build());

        return new RealmManager();
    }
}
