package com.softdesign.skillbrunchmainapp.di.modules;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.softdesign.skillbrunchmainapp.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.softdesign.skillbrunchmainapp.utils.Constants.CONSUMER_KEEP_ALIVE;
import static com.softdesign.skillbrunchmainapp.utils.Constants.LOAD_FACTOR;
import static com.softdesign.skillbrunchmainapp.utils.Constants.MAX_CONSUMER_COUNT;
import static com.softdesign.skillbrunchmainapp.utils.Constants.MIN_CONSUMER_COUNT;

/**
 * @author Sergey Vorobyev
 */
@Module
class FlavorModelModule {

    @Provides
    @Singleton
    JobManager provideJobManager() {
        Configuration cnf = new Configuration.Builder(App.get())
                .minConsumerCount(MIN_CONSUMER_COUNT)
                .maxConsumerCount(MAX_CONSUMER_COUNT)
                .loadFactor(LOAD_FACTOR)
                .consumerKeepAlive(CONSUMER_KEEP_ALIVE)
                .build();
        return new JobManager(cnf);
    }
}
