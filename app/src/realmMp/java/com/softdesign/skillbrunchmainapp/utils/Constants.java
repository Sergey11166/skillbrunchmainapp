package com.softdesign.skillbrunchmainapp.utils;

/**
 * @author Sergey Vorobyev
 */
@SuppressWarnings("unused")
public interface Constants {

    String LOG_TAG_PREFIX = "Skill";

    String PATTERN_EMAIL = "^[\\w\\+\\.\\%\\-]{3,}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{1,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{1,25})+$";

    // Activity requests
    int REQUEST_CODE_CAMERA = 0;
    int REQUEST_CODE_GALLERY = 1;
    int REQUEST_CODE_SETTING_CAMERA = 4;
    int REQUEST_CODE_SETTING_GALLERY = 5;

    // Permissions requests
    int CAMERA_PERMISSION_REQUEST_CODE = 0;
    int GALLERY_PERMISSION_REQUEST_CODE = 1;

    // OkHttp client settings
    int MAX_CONNECT_TIMEOUT = 1000 * 3;
    int MAX_WRITE_TIMEOUT = 1000 * 6;
    int MAX_READ_TIMEOUT = 1000 * 6;

    String LAST_MODIFIED_HEADER = "Last-Modified";
    String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";

    String REALM_USER = "sevoro.sv@gmail.com";
    String REALM_PASSWORD = "2848";
    String REALM_AUTH_URL = "http://5.141.84.162:9080/auth";
    String REALM_DB_URL = "realm://5.141.84.162:9080/~/default";
}
