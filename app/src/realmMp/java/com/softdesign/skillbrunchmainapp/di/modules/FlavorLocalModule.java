package com.softdesign.skillbrunchmainapp.di.modules;

import android.content.Context;

import com.facebook.stetho.Stetho;
import com.softdesign.skillbrunchmainapp.data.managers.RealmManager;
import com.softdesign.skillbrunchmainapp.utils.L;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.SyncConfiguration;
import io.realm.SyncCredentials;
import io.realm.SyncUser;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.softdesign.skillbrunchmainapp.utils.Constants.REALM_AUTH_URL;
import static com.softdesign.skillbrunchmainapp.utils.Constants.REALM_DB_URL;
import static com.softdesign.skillbrunchmainapp.utils.Constants.REALM_PASSWORD;
import static com.softdesign.skillbrunchmainapp.utils.Constants.REALM_USER;
import static io.realm.SyncCredentials.usernamePassword;

/**
 * @author Sergey Vorobyev
 */
@Module
public class FlavorLocalModule {

    private static String TAG = "REALM_MP";

    @Provides
    @Singleton
    RealmManager provideRealmManager(Context context) {
        L.d(TAG, "provide RealmManager init: ");
        Stetho.initializeWithDefaults(context);

        Observable.create((Observable.OnSubscribe<SyncUser>) subscriber -> {
            SyncCredentials credentials = usernamePassword(REALM_USER, REALM_PASSWORD, false);

            // sync create observer for login user
            if (!subscriber.isUnsubscribed()) {
                try {
                    subscriber.onNext(SyncUser.login(credentials, REALM_AUTH_URL));
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }

            // async create observer for login user
            /*if (!subscriber.isUnsubscribed()) {
                SyncUser.loginAsync(credentials, REALM_AUTH_URL, new SyncUser.Callback() {
                    @Override
                    public void onSuccess(SyncUser user) {
                        subscriber.onNext(user);
                        subscriber.onCompleted();
                    }

                    @Override
                    public void onError(ObjectServerError error) {
                        subscriber.onError(error);
                    }
                });
            }*/
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(syncUser -> {
                    SyncConfiguration config = new SyncConfiguration.Builder(syncUser, REALM_DB_URL).build();
                    Realm.setDefaultConfiguration(config);
                    L.d(TAG, "set sync config for realm: ");
                });
        return new RealmManager();
    }
}