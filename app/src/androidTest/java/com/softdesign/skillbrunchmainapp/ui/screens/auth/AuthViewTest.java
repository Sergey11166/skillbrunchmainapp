package com.softdesign.skillbrunchmainapp.ui.screens.auth;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.softdesign.skillbrunchmainapp.App;
import com.softdesign.skillbrunchmainapp.R;
import com.softdesign.skillbrunchmainapp.data.network.RestService;
import com.softdesign.skillbrunchmainapp.data.network.adapters.ProductCommentAddedJsonAdapter;
import com.softdesign.skillbrunchmainapp.data.network.adapters.ProductCommentJsonAdapter;
import com.softdesign.skillbrunchmainapp.ui.screens.root.RootActivity;
import com.squareup.moshi.Moshi;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressKey;
import static android.support.test.espresso.action.ViewActions.typeTextIntoFocusedView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasFocus;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.view.KeyEvent.KEYCODE_ENTER;
import static com.softdesign.skillbrunchmainapp.MockResponses.SUCCESS_GET_PRODUCTS;
import static com.softdesign.skillbrunchmainapp.MockResponses.SUCCESS_LOGIN;
import static com.softdesign.skillbrunchmainapp.utils.Constants.DEFAULT_LAST_MODIFIED;
import static com.softdesign.skillbrunchmainapp.utils.Constants.LAST_MODIFIED_HEADER;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

/**
 * @author Sergey Vorobyev
 */
public class AuthViewTest {

    private static final String TEST_VALID_EMAIL = "mail@mail.com";
    private static final String TEST_INVALID_EMAIL = "mail.com";
    private static final String TEST_VALID_PASSWORD = "12345678";
    private static final String TEST_INVALID_PASSWORD = "1234567";
    private static final String TEST_EMAIL_ERROR = "Wrong format";
    private static final String TEST_PASSWORD_ERROR = "Number symbols should't be less 8";

    @Rule
    public ActivityTestRule<RootActivity> activityTestRule =
            new ActivityTestRule<>(RootActivity.class, true, false);

    private ViewInteraction addToCartBtn;
    private ViewInteraction logo;
    private ViewInteraction appName;
    private ViewInteraction emailEt;
    private ViewInteraction emailWrap;
    private ViewInteraction passwordEt;
    private ViewInteraction passwordWrap;
    private ViewInteraction authCard;
    private ViewInteraction loginBtn;
    private ViewInteraction catalogBtn;
    private ViewInteraction vkBtn;
    private ViewInteraction fbBtn;
    private ViewInteraction twitterBtn;

    private MockWebServer mockWebServer = new MockWebServer();

    private static Matcher<View> withErrorTextInputLayout(String expectedError) {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                if (!(item instanceof TextInputLayout)) {
                    return false;
                }

                CharSequence error = ((TextInputLayout) item).getError();
                if (error != null) {
                    return error.toString().equals(expectedError);
                }

                return expectedError == null;
            }

            @Override
            public void describeTo(Description description) {

            }
        };
    }

    @Before
    public void setUp() {
        App.getModelComponent().getDataManager().deleteToken();
        App.getModelComponent().getDataManager().setServer(createMockServer().create(RestService.class));
        prepareDispatcherProducts();

        activityTestRule.launchActivity(new Intent());

        addToCartBtn = onView(withId(R.id.add_to_card_Btn));
        addToCartBtn.perform(click());

        logo = onView(withId(R.id.logo_layout));
        appName = onView(withId(R.id.app_name));
        emailEt = onView(withId(R.id.login_email_et));
        emailWrap = onView(withId(R.id.login_email_wrap));
        passwordEt = onView(withId(R.id.login_password_et));
        passwordWrap = onView(withId(R.id.login_password_wrap));
        authCard = onView(withId(R.id.auth_card));
        loginBtn = onView(withId(R.id.login_btn));
        catalogBtn = onView(withId(R.id.show_catalog_btn));
        twitterBtn = onView(withId(R.id.twitter_btn));
        vkBtn = onView(withId(R.id.vk_btn));
        fbBtn = onView(withId(R.id.fb_btn));
    }

    @After
    public void tearDown() throws Exception {
        mockWebServer.shutdown();
    }

    private Retrofit createMockServer() {
        return new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(MoshiConverterFactory.create(new Moshi.Builder()
                        .add(new ProductCommentJsonAdapter())
                        .add(new ProductCommentAddedJsonAdapter())
                        .build()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(new OkHttpClient.Builder().build())
                .build();
    }

    private void prepareDispatcherProducts() {
        final Dispatcher dispatcher = new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                MockResponse response = new MockResponse();
                switch (request.getPath()) {
                    case "/products":
                        return response
                                .setResponseCode(200)
                                .setHeader(LAST_MODIFIED_HEADER, DEFAULT_LAST_MODIFIED)
                                .setBody(SUCCESS_GET_PRODUCTS);
                    default:
                        return response.setResponseCode(404);
                }
            }
        };
        mockWebServer.setDispatcher(dispatcher);
    }

    private void prepareDispatcherLoginSuccess() {
        final Dispatcher dispatcher = new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                MockResponse response = new MockResponse();
                switch (request.getPath()) {
                    case "/login":
                        return response
                                .setResponseCode(200)
                                .setBody(SUCCESS_LOGIN);
                    default:
                        return response.setResponseCode(404);
                }
            }
        };
        mockWebServer.setDispatcher(dispatcher);
    }

    private void prepareDispatcherLoginError() {
        final Dispatcher dispatcher = new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                MockResponse response = new MockResponse();
                switch (request.getPath()) {
                    case "/login":
                        return response.setResponseCode(403);
                    default:
                        return response.setResponseCode(404);
                }
            }
        };
        mockWebServer.setDispatcher(dispatcher);
    }

    @Test
    public void fullActions() throws Exception {

        checkInitialViewState();

        clickOnLoginButtonThenCheckViews();

        pressBackThenCheckViews();

        inputEmailAndPasswordThenCheckErrors();

        loginError();

        loginSuccess();

        logout();

        goBackFromScreen();
    }

    private void checkInitialViewState() {
        logo.check(matches(isDisplayed()));
        appName.check(matches(isDisplayed()));
        appName.check(matches(withText(R.string.app_name)));
        authCard.check(matches(not(isDisplayed())));
        catalogBtn.check(matches(isDisplayed()));
        loginBtn.check(matches(isDisplayed()));
        loginBtn.check(matches(withText(R.string.auth_button_log_in)));
        twitterBtn.check(matches(not(isDisplayed())));
        vkBtn.check(matches(not(isDisplayed())));
        fbBtn.check(matches(not(isDisplayed())));
    }

    private void clickOnLoginButtonThenCheckViews() {
        loginBtn.perform(click());
        authCard.check(matches(isDisplayed()));
        catalogBtn.check(matches(not(isDisplayed())));
        emailEt.check(matches(hasFocus()));
        loginBtn.check(matches(not(isEnabled())));
        twitterBtn.check(matches(isDisplayed()));
        vkBtn.check(matches(isDisplayed()));
        fbBtn.check(matches(isDisplayed()));
    }

    private void pressBackThenCheckViews() {
        pressBack();
        checkInitialViewState();
    }

    private void inputEmailAndPasswordThenCheckErrors() {
        loginBtn.perform(click());
        emailEt.perform(click());
        emailEt.perform(typeTextIntoFocusedView(TEST_INVALID_EMAIL));
        emailEt.check(matches(withText(TEST_INVALID_EMAIL)));
        emailWrap.check(matches(withErrorTextInputLayout(TEST_EMAIL_ERROR)));
        loginBtn.check(matches(not(isEnabled())));

        emailEt.perform(clearText());
        emailEt.perform(typeTextIntoFocusedView(TEST_VALID_EMAIL));
        emailEt.check(matches(withText(TEST_VALID_EMAIL)));
        emailWrap.check(matches(withErrorTextInputLayout(null)));
        loginBtn.check(matches(not(isEnabled())));

        emailEt.perform(pressKey(KEYCODE_ENTER));
        passwordEt.check(matches(hasFocus()));
        passwordEt.perform(typeTextIntoFocusedView(TEST_INVALID_PASSWORD));
        passwordEt.check(matches(withText(TEST_INVALID_PASSWORD)));
        passwordWrap.check(matches(withErrorTextInputLayout(TEST_PASSWORD_ERROR)));
        loginBtn.check(matches(not(isEnabled())));

        passwordEt.perform(clearText());
        passwordEt.perform(typeTextIntoFocusedView(TEST_VALID_PASSWORD));
        passwordEt.check(matches(withText(TEST_VALID_PASSWORD)));
        pressBack();
        passwordWrap.check(matches(withErrorTextInputLayout(null)));
        loginBtn.check(matches(isEnabled()));
    }

    private void loginError() {
        prepareDispatcherLoginError();
        loginBtn.perform(click());

        authCard.check(matches(isDisplayed()));
        catalogBtn.check(matches(not(isDisplayed())));
        onView(allOf(withId(android.support.design.R.id.snackbar_text),
                withText(R.string.error_login))).check(matches(isDisplayed()));
    }

    private void loginSuccess() {
        prepareDispatcherLoginSuccess();

        emailEt.perform(click());
        emailEt.perform(clearText());
        emailEt.perform(typeTextIntoFocusedView(TEST_VALID_EMAIL));

        emailEt.perform(pressKey(KEYCODE_ENTER));
        passwordEt.perform(clearText());
        passwordEt.perform(typeTextIntoFocusedView(TEST_VALID_PASSWORD));
        pressBack();
        loginBtn.perform(click());
        loginBtn.check(matches(withText(R.string.auth_button_log_out)));
        authCard.check(matches(not(isDisplayed())));
        catalogBtn.check(matches(isDisplayed()));
        onView(allOf(withId(android.support.design.R.id.snackbar_text),
                withText(R.string.auth_snack_logged_in))).check(matches(isDisplayed()));
    }

    private void logout() {
        loginBtn.perform(click());
        loginBtn.check(matches(withText(R.string.auth_button_log_in)));
    }

    private void goBackFromScreen() {
        pressBack();
        addToCartBtn.check(matches(isDisplayed()));
    }
}