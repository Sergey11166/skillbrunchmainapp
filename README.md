## Учебное приложение с курсов по андроид разработке [описание курсов](http://www.skill-branch.ru/android) ##

## Используемые технологии: ##
- Java
- MVP
- DataBinding
- Dagger2
- Flow
- Mortar
- RealmDB
- RxJava
- JUnit
- Mockito
- Espresso